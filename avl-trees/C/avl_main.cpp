/*
##############################################################################
# 
#      ZbW      Abteilung Informatik           CH-9015 St.Gallen  
# 
#  DATE:	17.12.97
#  AUTHOR:	zbw
#  TITEL:	Binaere Baeume, Grundfunktionen
#
#  FILE:        avl_main.cpp
#  type:
#  OS:    	Solaris 1.1, dos
#  BUILD:       borland, make
#
#  DESCRIPTION: Aufbau eines bion. Baumes
#
##############################################################################
*/
/*
 * INCLUDES
 */
#include <iostream.h>
#include <stdlib.h>
#include "avlbaum.h"

const int MAXLINE = 20;
static char s[MAXLINE]; //Hilfs-String-Buffer

int main()
{
	CBaum aTree;
	do
	{
		cout << "Aufg. AVL - Baueme" << endl;
		cout << "-------------------" << endl << endl;
		cout << "1. Show (Inorder)\n";
		cout << "2. Insert\n";
		cout << "3. Suchen\n";
		//cout << "4. Loeschen\n";
		//cout << "5. Insert (iterativ)\n";
		cout << "6. Hoehe\n";
		cout << "7. durchschn. Pfadlaenge\n";
		cout << "0.  Ende" << endl << endl;
		cout << "Wahl > ";
		cin.getline(s,MAXLINE);

		switch(s[0])
		{
		 case '0': break;
		 case '1': aTree.treeprint();break;
		 case '2': cout << "Insert Wert > "; cin.getline(s,MAXLINE);
			   aTree.treeinsert(atol(s));break;
		 case '3': cout << "Suche Wert > "; cin.getline(s,MAXLINE);
			   if (aTree.treesearch(atol(s))!=NULL)
				cout << "ok" << endl;
			   else
				cout << "not ok" << endl;
			   break;

/*		 case '4': cout << "Loesche Wert > "; cin.getline(s,MAXLINE);
			   if (aTree.treedelete(atol(s)))
				cout << "ok" << endl;
			   else
				cout << "nicht vorhanden" << endl;
			   break;
		 case '5': cout << "Insert Wert > "; cin.getline(s,MAXLINE);
			   aTree.treeinsert(atol(s));break;
*/		 case '6': cout << "Der Baum hat die Hoehe "
				<< aTree.getHeight();break;
		 case '7': cout << "Der Baum hat eine mittlere Pfadlaenge ";
			   aTree.treepath();break;
		 default: cout << "Ung. Option\n";

		}
	} while(s[0] != '0');
	return 0;
}
