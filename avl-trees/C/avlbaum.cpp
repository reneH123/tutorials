/*
##############################################################################
#      ZbW      Abteilung Informatik           CH-9015 St.Gallen  
# 
#  DATE:	4.1.97
#  AUTHOR:	zbw
#  TITEL:	Bin�re Baume mit AVL 
#
#  FILE:	avlbaum.cpp
#  type:	C++-Source
#  OS:    	Solaris 2.5, DOS
#  BUILD:	make, Borland
#
#  DESCRIPTION:	Uebung zu binaeren Suchbaeumen mit AVL-Baum (insert)
##############################################################################
*/
/*
 * INCLUDES
 */
#include <iostream.h>
//#include "c:\data\zbw\progmeth\binbaum\binbaum.h"
#include "avlbaum.h"

#define DEBUG
/*****************************************************
**public functions Klasse CBaum
******************************************************/

CBaum::~CBaum()
{
	removetree(Wurzel);
}

void
CBaum::removetree(CKnoten *root)
{
    if(root)
    {
	removetree(root->ublinks);
	removetree(root->ubrechts);
    delete root;
    }
}

void
CBaum::treeinsert(int schluessel)
{
	bool higher;
	Wurzel=treeinsert(Wurzel,schluessel,&higher);
	setHeight();

}

void
CBaum::treeprint(void)
{
	treeprint(Wurzel);
}

void
CBaum::setHeight(void)
{
	treeheight(Wurzel,0);
}

int
CBaum::treepath(void)
{
	return treepath(Wurzel,0);
}

//noch zu implementieren !!
bool
CBaum::treedelete(int schluessel)
{
	CKnoten *hilf;
	bool found;
	//!! Wurzel=treedelete(Wurzel,schluessel, &found);
	if(found == true)
	{
		setHeight();
		setSize(getSize() - 1);
		return true;
	}
	else
		return false;

}

CKnoten *
CBaum::treesearch(int schluessel)
{
	return treesearch(Wurzel,schluessel);
}



/*****************************************************
**private functions
******************************************************/

/*
* treeinsert private function, AVL-Version
*/
CKnoten * 
CBaum::treeinsert(CKnoten *root, int schluessel, bool *hoeher)
{
	CKnoten *neu;
	if (!root)
	{	/* Element einf�gen */
		neu = new CKnoten ;
		neu->info = schluessel;
		neu->balance = Neutral;
		neu->ublinks = NULL;  neu->ubrechts = NULL;
		root = neu;
		*hoeher = true;
	}
	else
	{
		if (schluessel < root->info)
		{  	/* links einf�gen */
			root->ublinks = treeinsert(root->ublinks , schluessel, hoeher);
			if (*hoeher == true)
			{ 	/* linker Unterbaum ist gewachsen */
				switch (root->balance) {
					case LinksL :
						if (root->ublinks->balance == LinksL)
						{	// LL-Rotation
							root = rotateleft(root);
							root->ubrechts->balance = Neutral;
						}
						else
						{	/* LR-Rotation */
						    root->ublinks = rotateright(root->ublinks);
						    root = rotateleft(root);
						    checkbalance(root);
						}
						root->balance = Neutral;
						*hoeher = false;
						break;
					case RechtsL :
						root->balance = Neutral;
						*hoeher = false;
						break;
					case Neutral :
						root->balance = LinksL;
						break;
				}
			}
		}
		else if (schluessel > root->info)
		{	/* im rechten Unterbaum einf�gen */
			root->ubrechts = treeinsert(root->ubrechts , schluessel, hoeher);
			if (*hoeher == true)
			{ 	/* rechter Unterbaum ist gewachsen */
				switch (root->balance) {
					case RechtsL :
						if (root->ubrechts->balance == RechtsL)
						{	/* RR-Rotation */
							root = rotateright(root);
							root->ublinks->balance = Neutral;
						}
						else
						{	/* RL-Rotation */
							root->ubrechts= rotateleft(root->ubrechts);
							root = rotateright(root);
							checkbalance(root);
						}
						root->balance = Neutral;
						*hoeher = false;
						break;
					case LinksL :
						root->balance = Neutral;
						*hoeher = false;
						break;
					case Neutral :
						root->balance = RechtsL;
						break;
				}//switch
			}//if hoeher true
		}
	}
	return(root);
}

/*
* treesearch private function
*/
CKnoten*
CBaum::treesearch(CKnoten *root, int schluessel)
{
	if (!root)
		return NULL;

	if(schluessel == root->info)
		return root;

	if (schluessel < root->info)
		return treesearch(root->ublinks,schluessel);
	if(schluessel >root->info)
		root->ubrechts = treesearch(root->ubrechts,schluessel);
	return(root);
}
/*
* treeprint  private function
*/
void
CBaum::treeprint(CKnoten *root)
{
	if (root)
	{
		treeprint(root->ublinks);
		cout << root->info << endl;
		treeprint(root->ubrechts);
	}
}



/*
* treeheight
*/

void
CBaum::treeheight(CKnoten *root, int hoehe)
{
	if (root)
	{
		treeheight(root->ublinks, hoehe +1);
		if (hoehe > Height)
			Height = hoehe;
		treeheight(root->ubrechts, hoehe +1);
	}
}


/*
* treepath private : Summe aller Pfadlaengen
* pfad : Pfadlaenge des aktuellen Knotens
* h    : Summe der Pfadlaengen beider Unterbaeume
*/
int
CBaum::treepath(CKnoten *root, int pfad)
{
	int h = 0;
	if (root)
	{
		h = treepath(root->ublinks, pfad +1);
		h += treepath(root->ubrechts, pfad +1);
		h += pfad;

	}
	return h;
}

/*
* rotateright
*/
CKnoten *
CBaum::rotateright(CKnoten *knoten)
{
#ifdef DEBUG
cout << "RR \n";
#endif


}



/*
* rotateleft
*/
CKnoten *
CBaum::rotateleft(CKnoten  *knoten)
{

}

/*
* swap
*/
void 
CBaum::swap(CKnoten  **z1,CKnoten  **z2,CKnoten  **z3)
{
	CKnoten  *h;
	h = *z1;
	*z1 = *z2;
	*z2 = *z3;
	*z3 = h;
}


/*
* checkbalance
*/
void 
CBaum::checkbalance(CKnoten  *root)
{


}

