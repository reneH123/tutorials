/*
##############################################################################
# 
#      ZbW      Abteilung Informatik           CH-9015 St.Gallen  
# 
#  DATE:	4.1.98
#  AUTHOR:	zbw
#  TITEL:       Bin. Baeume
#
#  FILE:        avlbaum.h
#  type:	C++ Source (Header)
#  OS:    	Solaris 2.x, Win x
#  BUILD:       Borland, make
#
#  DESCRIPTION:  Header mit Klassendef. CBaum und CKnoten
#
##############################################################################
*/
#ifndef BIN_BAUM_H
#define BIN_BAUM_H

enum BalanceTyp {LinksL,Neutral,RechtsL};

class CKnoten
{
friend class CBaum;
public:
	CKnoten(){ublinks=NULL; ubrechts=NULL;}
private:
	int info;
	BalanceTyp balance;
	CKnoten  *ublinks, *ubrechts;
};

class CBaum
{
public:
	CBaum(){anzNodes=0;Wurzel=NULL;}
	~CBaum();
	void treeinsert(int schluessel);
	void treeprint(void);
	//int  treeheight(void);
	int  treepath(void);
	bool treedelete(int schluessel);
	CKnoten *treesearch(int schluessel);
	CKnoten *getTree(void) {return Wurzel;}
	void setSize(int n) 	{anzNodes=n;}
	int getSize(void) const {return anzNodes;}
	void setHeight(void);
	int getHeight(void) const {return Height;}
private:
	//ueberladene Funktionen fuer Rekursionen
	void removetree(CKnoten *root);
	CKnoten *treeinsert(CKnoten *root, int schluessel, bool *higher);
	void treeprint(CKnoten *root);
	void treeheight(CKnoten *root, int level);
	int treepath(CKnoten *root, int pfad);
	CKnoten *treedelete(CKnoten *root, int schluessel, bool *found);
	CKnoten *treesearch(CKnoten *root, int schluessel);
	CKnoten *rotateright(CKnoten  *knoten);
	CKnoten *rotateleft(CKnoten  *knoten);
	void swap(CKnoten  **z1,CKnoten  **z2,CKnoten  **z3);
	void checkbalance(CKnoten *root);

	//Elemente
	CKnoten *Wurzel;
	int anzNodes;
	int Height; //aktuelle Baumhoehe
};
#endif
