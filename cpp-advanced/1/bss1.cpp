#include <iostream>
#include <stdio.h>

using namespace std;

int global_aaa;       // .bss (gloabl)
int global_bbb;       // .bss (global)
int global_ccc=123;   // is in .data (because initialised global)
int global_ddd=0;     // .bss (global) (because initial value is zero)

static int global_eee;    // .bss  (static values always in bss!)
static int global_fff=7;  // .data  (again, because initialised global)
static int global_fff2=0;  // .bss  (again, because initial value is zero)

int foo(){
  static int global_ggg;      // .bss   (local)
  static int global_hhh=0xdead;  // .data  (local) (initialised global always in .data!)

  int local_iii=999;          // stack
  return local_iii+7394;
}

// rule I: global initialised data (except 0) always goes to .data (local goes to stack)
// rule II: global uninitialised (or zero-initialised) data always goes to .bss
// rule III: static variable same as global variable w.r.t. .bss/.data

int main(){
  printf("Hello world!\n");
  return 0;
}

/*
DATA:
    23: 0000000000201000     0 SECTION LOCAL  DEFAULT   23 
    50: 0000000000201000     0 NOTYPE  WEAK   DEFAULT   23 data_start
    64: 0000000000201000     0 NOTYPE  GLOBAL DEFAULT   23 __data_start
    55: 0000000000201008     0 OBJECT  GLOBAL HIDDEN    23 __dso_handle
    63: 0000000000201010     4 OBJECT  GLOBAL DEFAULT   23 global_ccc
    38: 0000000000201014     4 OBJECT  LOCAL  DEFAULT   23 _ZL10global_fff

BSS:
    66: 0000000000201018     0 NOTYPE  GLOBAL DEFAULT   24 __bss_start
    24: 0000000000201018     0 SECTION LOCAL  DEFAULT   24 
    30: 0000000000201018     1 OBJECT  LOCAL  DEFAULT   24 completed.7698
    49: 0000000000201018     0 NOTYPE  GLOBAL DEFAULT   23 _edata
    61: 0000000000201018     0 OBJECT  GLOBAL HIDDEN    23 __TMC_END__
    59: 000000000020101c     4 OBJECT  GLOBAL DEFAULT   24 global_aaa
    62: 0000000000201020     4 OBJECT  GLOBAL DEFAULT   24 global_bbb
    52: 0000000000201024     4 OBJECT  GLOBAL DEFAULT   24 global_ddd
    36: 0000000000201028     1 OBJECT  LOCAL  DEFAULT   24 _ZStL8__ioinit
    37: 000000000020102c     4 OBJECT  LOCAL  DEFAULT   24 _ZL10global_eee
    65: 0000000000201030     0 NOTYPE  GLOBAL DEFAULT   24 _end



.data                  2101248 .. +24 = [0x201000 .. 0x201017]
.bss                   2101272 .. +24 = [0x2010 18 .. 0x2010 2F]

 
*/
