	.file	"bss1.cpp"
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.globl	global_aaa
	.bss
	.align 4
	.type	global_aaa, @object
	.size	global_aaa, 4
global_aaa:
	.zero	4
	.globl	global_bbb
	.align 4
	.type	global_bbb, @object
	.size	global_bbb, 4
global_bbb:
	.zero	4
	.globl	global_ccc
	.data
	.align 4
	.type	global_ccc, @object
	.size	global_ccc, 4
global_ccc:
	.long	123
	.globl	global_ddd
	.bss
	.align 4
	.type	global_ddd, @object
	.size	global_ddd, 4
global_ddd:
	.zero	4
	.local	_ZL10global_eee
	.comm	_ZL10global_eee,4,4
	.data
	.align 4
	.type	_ZL10global_fff, @object
	.size	_ZL10global_fff, 4
_ZL10global_fff:
	.long	7
	.local	_ZZ3foovE10global_ggg
	.comm	_ZZ3foovE10global_ggg,4,4
	.align 4
	.type	_ZZ3foovE10global_hhh, @object
	.size	_ZZ3foovE10global_hhh, 4
_ZZ3foovE10global_hhh:
	.long	789
	.text
	.globl	_Z3foov
	.type	_Z3foov, @function
_Z3foov:
.LFB1493:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$999, -4(%rbp)
	movl	-4(%rbp), %eax
	addl	$7394, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1493:
	.size	_Z3foov, .-_Z3foov
	.section	.rodata
.LC0:
	.string	"Hello world!"
	.text
	.globl	main
	.type	main, @function
main:
.LFB1494:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	leaq	.LC0(%rip), %rdi
	call	puts@PLT
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1494:
	.size	main, .-main
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB1975:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L7
	cmpl	$65535, -8(%rbp)
	jne	.L7
	leaq	_ZStL8__ioinit(%rip), %rdi
	call	_ZNSt8ios_base4InitC1Ev@PLT
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rax
	movq	%rax, %rdi
	call	__cxa_atexit@PLT
.L7:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1975:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_global_aaa, @function
_GLOBAL__sub_I_global_aaa:
.LFB1976:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1976:
	.size	_GLOBAL__sub_I_global_aaa, .-_GLOBAL__sub_I_global_aaa
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_global_aaa
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
