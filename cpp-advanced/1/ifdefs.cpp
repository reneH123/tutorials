// g++ ifdefs.cpp -DMYOPTION
#if 1
  #define AAA
  #define BBB
#else
  #define CCC
#endif


// BUG: #ifdef (AAA)
//#ifdef AAA  // OK
//#if defined(AAA)  // OK, equivalent
//#ifndef BBB // OK
//#if defined(AAA)==true // OK
#if defined(AAA)==true && defined(BBB)==true
  //#error blub
  // nothing is also okay!
#endif

#if 1
#if defined(MYOPTION)
 #error INVALID-SWITCH_PASSED!!!  // pass -DMYOPTION to gcc!
#endif
#endif

// if-elif are excluding each other (this is different in a switch)

// comment binds stronger than preprocessor directive

//


#include <stdio.h>

int main(){
  printf("Hello world!\n");
  return 0;
}
