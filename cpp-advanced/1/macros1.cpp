// g++ macros1.cpp -E | less
#include <stdio.h>

// arguments are comma-separated for VA_ARGS in macro heads

#define LOOP_1(X) \
  printf("%s\n",#X);

#define LOOP(X,...) \
  printf("%s\n", #X); \
  LOOP_1(__VA_ARGS__)

// __VA_ARGS__ is equivalent to the real content behind '...'
// this VA_ARGS "unrolling" is called "loop-unrolling" here by the author (not to be mixed with same name optimisation!)



#line 123

int main(){
  LOOP(copy paste cut)
  LOOP(copy paste cut,AAA)
  LOOP(copy paste cut,AAA,BBB,CCC)  // // if in LOOP_1 "..." was missing, then the preprocessor(!) would complaint here since 3 arguments
  return 0;
}
