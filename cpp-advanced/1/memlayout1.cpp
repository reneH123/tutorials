// memory layout
#include <stdio.h>

struct sample_t {
 char first;
 char second;
 char third;
 short fourth;

 int fifth; // if added, then mem layout as below!
}; 
/*
 memory layout:

   | first(1Byte) second(1B) |    = 2 bytes
   | third(1B)    <blank>    |    = 2 bytes
   | forth(2B)               |    = 2 bytes

   |  2Bytes reserved (algn) |
   | fifth(4B)               |    = 2B+4B = 6bytes (if fifth is added)

So, with fifth: 3xWORD (3x4B=12 Bytes)
    w/o  fifth: 1xWORD + 1/2WORD = 6Bytes
*/

void print_size(struct sample_t* var) {
 printf("Size: %lu bytes\n", sizeof(*var));
}

void print_bytes(struct sample_t* var) {
 unsigned char* ptr = (unsigned char*)var;
 for (int i = 0; i < sizeof(*var); i++, ptr++) {
  printf("%d ", (unsigned int)*ptr);
 }
 printf("\n");
}

int main(int argc, char** argv) {
 printf("sizeof(unsigned char*)=%lu\n", sizeof(unsigned char*)); // 8 bytes (all pointers on the same architecture 64bit have size 8Bytes)
 struct sample_t var;
 var.first = 'A';
 var.second = 'B';
 var.third = 'C';
 var.fourth = 765;
 var.fifth = -1;
 print_size(&var);  // output: 6 bytes on 32 and 64-bit architectures (is the same!)
 print_bytes(&var);
 return 0;
}

//  Output:
//   6 bytes\n  65 66 67 0 253 2
//
//  bytes: 'A' 'B' 'C' 0x7F 0xfd 0x2
//                     127   253
//                     ^--- could also be zeroed! depends on architecture OR (undocumented bug!? or GCC standard)

// struct resides on stack; stack is not (always) zeroed in (not for structs) !!!
