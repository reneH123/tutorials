// POINTER ARITHMETICS + correct use of */&

#include <stdio.h>

int main(){
  printf("Hello world!\n");
  int val=123;
  int *p=0;
  char *c_ptr=(char*)&val;  // cast (char*) REQUIRED(!), because pointer types are not compatible (HERE: start address will be the same assumed!)
  p=&val;  // this derefence does NOT require a cast, because type coercion possible without any conflict
  printf("  sizeof(int*)=%lu, sizeof(char*)=%lu\n",sizeof(int*), sizeof(char*));

  // BEFORE: sizeof(p)=8, but int ptr at 3ea54434, char ptr at 3ea54434
  printf("sizeof(p)=%lu, but int ptr at %x, char ptr at %x\n", (unsigned long)sizeof(p), p, c_ptr);
  ++p; ++c_ptr;
  // AFTER: sizeof(p)=8, but int ptr at 3ea54438, char ptr at 3ea54435
  printf("sizeof(p)=%lu, but int ptr at %x, char ptr at %x\n", (unsigned long)sizeof(p), p, c_ptr);

  

  return 0;
}

// Pointer size is fixed and type-independent (architecture dependent!)
// BUT: step-size is type-dependent, e.g. ++ptr !!

