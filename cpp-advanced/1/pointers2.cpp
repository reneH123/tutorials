#if  0
// demonstrates an dangling pointer issue

#include <stdio.h>

int* create_an_integer(int val){
  int var = val;
  return &var;
}

int main(){
 printf("Hello world!\n");
 int* ptr = create_an_integer(100);
 printf("%d\n", *ptr);  // crash is caused here, since LINUX detects a SEGV software IRQ
 return 0;
}

#endif

#if 0
//demonstrates passing of pointers (changes inside and/or where exactly is changed what)
#include <stdio.h>

void func(int* a){
  int b=9;
  *a=5;
  a=&b;
}

int main(int argc, char** argv) {
  int x = 3;
  int* xptr = &x;
  printf("Value before call: %d\n", x);
  printf("Pointer before function call: %p\n", (void*)xptr);
  func(xptr);   // changes content the pointer is pointing to, but not the pointer address (even if specified inside, since pointer addr is on stack)!
  printf("Value after call: %d\n", x);
  printf("Pointer after function call: %p\n", (void*)xptr);
  return 0;
}

#endif

#if 1

// demonstrates the use of function pointers

#include <stdio.h>

int sum(int a, int b){
  return a+b;
}

int subtract(int a, int b){
  return a-b;
}

int main(){
  printf("Hello world!\n");
  int (*func_ptr)(int, int) = sum;  // both exactly the same: sum, &sum (however, & be better)

  int result = func_ptr(5,4);
  printf("Sum is: %d\n", result);

  func_ptr = subtract;
  result = func_ptr(5,4);
  printf("Difference is: %d\n", result);
  return 0;
}

#endif
