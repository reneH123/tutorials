#include <iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

/* Note: char* argv[] equivalent in assembler with cha** argv */

int main(int argc, char** argv){ // intentional flaw: if argc == 0, then segmentation fault!
  double r = atof(argv[1]);  // does not detect errors!
  double s = sin(r);
  cout << "Hello World! sin(" << r << ")=" << s << endl;
  return 0;
}
