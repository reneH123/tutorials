#include <iostream>  
using namespace std;  

void break1(){
  for(int i=1;i<=3;i++){        
    for(int j=1;j<=3;j++){        
      if(i==2&&j==2){
        break;  // breaks the inner loop; the outer loop continues with i==3, the inner loop with j=1
      }
      cout<<i<<" "<<j<<"\n";             
    }
  }
}

// result: 1 1  > 1 2 > 2 1 > 3 1 > 3 2



void continue1(){
  for(int i=1;i<=10;i++){      
    if(i==5){      
      continue;
    }
    cout<<i<<"\n";
  }
}



int main(){
  cout << "Test break1:" << endl;
  break1();
  cout << endl;
  cout << "Test continue1:" << endl;
  continue1();
  return 0;
}
