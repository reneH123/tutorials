#include <iostream>

using namespace std;

int main(){
  int age=-1;

  cout << "Enter your age";   // no 'endl' here may cause in incomplete output issued to stdout
  cin >> age;  // invalid input is set by default to "0"
  cout << "Your age is: " << age << endl;
}
