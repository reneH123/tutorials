#include <iostream>  

using namespace std;  

class Base
{  
   int a;  // until here class size is 4 Bytes
   int b;  // ... 8 Bytes
   char c; // ... 12 Bytes, although sizeof(char) is only 1 Bytes; this is called (unpacked) class field assignment
   char d; // ... 12 Bytes, since now c and d are aligned among 3rd (4 Bytes) word
} __attribute__((__aligned__(8)));  // now the total size is no more 12 Bytes, but 16 Bytes in fact (! because aligned at 8 bits); DEFAULT: 4-Byte alignment (would consume 12 Bytes)


int main()  
{  
 Base b;
 cout << "Size of class base is : " << sizeof(b) << endl;
 cout << "Sizes of char:" << sizeof(char) << " int: " << sizeof(int) << endl;
 cout << "Sizes : " << sizeof(int*) << endl;  // sizeof(int*) == sizeof(void*)
 return 0;
}
