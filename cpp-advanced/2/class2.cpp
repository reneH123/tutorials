#include <iostream>

using namespace std;

class Class {
public:
	Class(void) {
		cout << "ctor" << endl;
		this->value = 0;
	}
	~Class(void) {
		cout << "dtor" << endl;
	}
	int value;
};

int main() {
	Class *ptr = new Class;
	ptr->value = 111;
	cout << ptr->value++ << endl; // 111
	cout << ++(ptr->value) << endl; // 113
	delete ptr;

	return 0;
}

