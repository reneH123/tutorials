#include <iostream>

using namespace std;

class X{
public:
	int data;
public:
	X(int x): data(x){};
};

class Y{
	X x;
public:
	Y(int x);  // declaration of Y's ctor
};

Y::Y(int x): x(1){};  // definition of Y's ctor

int main() {
	Y y(5);

	return 0;
}

