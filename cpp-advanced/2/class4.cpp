#include <iostream>

using namespace std;

class X {
private:
	int xxx1, xxx2, xxx3;

private:
	int getXXX1() { // not visible anywhere, not even in (immediate) sub-class(es)
		return this->xxx1;
	}
protected:  // only visibly within (the immediate) sub-class(es)
	int getXXX2() {
		return this->xxx2;
	}
public:
	int getXXX3() { // visibility everywhere
		return this->xxx3;
	}
};

class Y: public X {
private:
	int yyy1, yyy2, yyy3;
private:
	int getYYY1() {
		return 0;//return this->getXXX1();   // NOK!  Every method MUST return a value!
	}
protected:  // not visible from outside, but visible within inheritance hierarchy
	int getYYY2() {
		return this->getXXX2();
	}
public:  // anywhere visible
	int getYYY3() {
		return this->getXXX3();
	}

};

class Y2: protected X {
private:
	int yyy1, yyy2, yyy3;
private:
	int getYYY1() {
		return 0;
	}
protected:
	int getYYY2() {
		return this->getXXX2();
	}
public:  // the only method visible from outside!
	int getYYY3() {
		return this->getXXX3();
	}

};

int foo(){
 // OK! must not return a value!
}

int main() {
	X x1;
	//x1.getXXX1();
	//x1.getXXX2();
	x1.getXXX3();

	Y y1;  // class Y : public X
	//y1.getXXX1();
	//y1.getXXX2();
	y1.getXXX3();
	//y1.getYYY1();
	//y1.getYYY2();
	y1.getYYY3();

	Y2 y2;  // class Y2 : protected X      => same as with public, except non-public only visible inside; only new public methods visible outside
	//y2.getXXX1();
	//y2.getXXX2();
	//y2.getXXX3(); // now not visible, since all imported from X into Y2 becomes private from outside!
	//y2.getYYY1();
	//y2.getYYY2();
	y2.getYYY3();

	//return 0;  // OK! must not return a value!
}

