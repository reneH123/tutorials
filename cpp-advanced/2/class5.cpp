#include <iostream>
#include <string>

using namespace std;

class X {
	string name;
public:
	X(string name) :
			name(name) {
		dosth(); // calls X::dosth
	}
	virtual void dosth() {
		cout << "Welcome to the super-class!" << endl;
	}

	void dofancystuff(){
		dosth();  // calls dosth() that is in the current Virtual-Method-Table, so, e.g. X1::dosth() or X2::dosth()
	}
};

class X1: public X {
public:
	X1(string name) :
			X(name) {
	}
	void dosth() {
		cout << "Welcome to the X1 ctor!" << endl;
	}
};

class X2: public X {
public:
	X2(string name) :
			X(name) {
	}
	void dosth() {
		cout << "Welcome to the X2 ctor!" << endl;
	}
};

int main() {
	X1 *x1 = new X1("alice");  // "Welcome to the super-class!"   (so, calls from the superclass's ctor, HENCE X::dosth !!)
	X2 *x2 = new X2("bob");    // "Welcome to the super-class!"

	cout << "Now do something fancy:" << endl;
	x1->dofancystuff();   // "Welcome to X1 ctor!"  (just, because it is VIRTUAL and not called from superclass ctor !)
	x2->dofancystuff();   // "Welcome to X2 ctor!"  (ditto)

}

