#include <iostream>
#include <string>

using namespace std;

class Pet {
protected:
	string name;
public:
	Pet(string name) :
			name(name) {
	}
	virtual ~Pet(){}
	virtual void MakeSound(void) {
		cout << name << " is silent: (" << endl;
	}
};

class Dog: public Pet {
public:
	Dog(string name) :
			Pet(name) {
	}
	void MakeSound(void) {
		cout << name << " says: Woof!" << endl;
	}
};

class GermanShepherd: public Dog {
public:
	GermanShepherd(string name) :
			Dog(name) {
	}
	void MakeSound(void) {
		cout << name << " says: Wuff!" << endl;
	}
	void Laufen(void) {
		cout << name << " runs(gs)!" << endl;
	}
};

class MastinEspanol: public Dog {
public:
	MastinEspanol(string name) :
			Dog(name) {
	}
	void MakeSound(void) {
		cout << name << " says: Guau!" << endl;
	}
	void Ejecutar(void) {
		cout << name << " runs (mes) !" << endl;
	}
}
;

void PlayWithPet(Pet *pet) {
	GermanShepherd *gs;
	MastinEspanol *mes;
	pet->MakeSound();
	if (gs = dynamic_cast<GermanShepherd*>(pet))  // HERE must be a dynamic_cast! If it was, however, static_cast, then there would be !=NULL (no COMPILER nor runtime error!) and Laufen would be executed with totally corrupt data!
		gs->Laufen();
	if (mes = dynamic_cast<MastinEspanol*>(pet))
		mes->Ejecutar();
	// difference static_cast and dynamic_cast:
	//  - static_cast: we are absolutely sure that at runtime it is a certain sub_class, so cast  (always returns !=NULL)
	//  - dynamic_cast: we are NOT sure if it is a certain subclass and we try, if it was not, then returns NULL  (at runtime it will be decided if it can effectively and properly be casted or not by using RTTI, and further meta-info)
}

int main() {
	Pet *pet = new Pet("creature");
	Dog *dog = new Dog("Dog");
	GermanShepherd *gs = new GermanShepherd("Hund");
	MastinEspanol *mes = new MastinEspanol("Perro");
	PlayWithPet(pet);
	PlayWithPet(dog);
	PlayWithPet(gs);
	PlayWithPet(mes);
	return 0;
}

/*
 creature is silent: (
 Dog says: Woof!
 Hund says: Wuff!
 Hund runs(gs)!
 Perro says: Guau!
 Perro runs (mes) !
 */

