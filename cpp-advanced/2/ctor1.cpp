#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

class AAA{
private:
	int _foo;
	int _bar;
public:
	explicit /*void*/ AAA(int z): _foo(foo()), _bar(bar()){ // left-to-right execution of arguments
		_foo++;
		_bar++;
		return; // although return type of this c-tor is void (verify by return a number, check error message!), it is not allowed to explicitly specify void as return type (COMPILER error)
	}
	AAA(int z, char c2, char c1='a', float f=0.3f){ // without c2 COMPILER error, since would be ambiguous to 1st c-tor
		this->_foo = 4;
		this->_bar = 3.f;
	}
	/*explicit*/ int foo(){
		cout << "FOO" << endl;
AAA::_foo=7;
(this->_foo)++;
		return 5;
	}
	int bar(){
		cout << "BAR" << endl;
		return 4;
	}
};




int main(int argc, char *argv[])
{
  AAA aaa(7);
  cout << "Bye!" << endl;
  return 0;
}

