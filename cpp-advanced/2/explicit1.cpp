#include <iostream>

// g++ -std=c++98 explicit1.cpp -o explicit1
// 

class Foo
{
public:
  // single parameter constructor, can be used as an implicit conversion
  Foo (int foo) : m_foo (123), myptr(NULL)   // execution from left-to-right (as mentioned)
  {
  }

  int GetFoo () { return m_foo; }

private:
  int m_foo;
  void* myptr;
};

void DoBar (Foo foo)
{
  int i = foo.GetFoo ();
}

int main ()
{
  DoBar (42); // existed already before 2011: instead of DoBar(new Foo(42));
}
