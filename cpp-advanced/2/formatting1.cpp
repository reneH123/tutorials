    #include <iostream>
    #include <iomanip>

using namespace std;

    int main() {
        float total = 132.3961;
        cout << fixed << setprecision(2) << total << endl;

	printf("%.7g",total); // uses stdio

	double f = 1.23;
	double g = 3.45;
	double h = 5.67;

	double res = (f/g)-h;

	cout << "Result 1 is: " << res << endl;
	res = f/g-h;

	cout << "Result 2 is: " << res << endl;
	res = f/(g-h); // equivalent to result 1

	cout << "Result 3 is: " << res << endl;

        // formatting ints
	int x = (3/2) +2;   // is 3
	cout << "Value of x is: " << x << endl;

    }
