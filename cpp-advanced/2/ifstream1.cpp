#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv){
  if (argc<=2){
    cerr << "Usage: " << argv[0] << " infile outfile " << endl;
    exit(1);  // is from stdlib, but already in iostream
  }
  
  // READING from file via streams:
  const char* fname1 = "temp123";
  ifstream infile(fname1);
  char buff[100];
  infile >> buff;
cout << "Reading from buffer: " << buff << endl; // reads only the 1st characters until whitespace and/or EOF

  // WRITING into file via streams:
  
  return 0;
}
