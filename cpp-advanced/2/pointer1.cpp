#include <iostream>  

using namespace std;  

int main()  
{  
  int aaa[]={11,12,13,14,15};  // array initialization ; STACK/BSS
  int *ptr;       // pointer declaration  
  ptr=aaa;    // assigning base address of array to the pointer ptr  
  ptr=ptr+1;   // incrementing the value of pointer  
  cout <<"value of second element of an array : "  << *ptr << endl;
  // *ptr is 11+1 == 12
  // *ptr is the same as ptr[1]  (because was incremented previously)
  // *ptr+1 is 3rd element in array
  ptr+=1;
  // .. and now it is the next
  cout << "Value third element of an array: " << *ptr+1 << endl;
  // reset hard to 1st
  // is dangerous, because may read sth from previous STACK frame:
  cout << "First element in array: " << *ptr-22 << endl;

  cout << "The address of ptr: " << ptr << endl; // the stack address changes every time (in .rela.ptr), e.g. 0x7ffecf733048
  return 0;  
}
