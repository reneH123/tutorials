#include <iostream>

using namespace std;  

int main()  
{  
    void *ptr; // int *ptr;   <-
    float f=10.3;  
    ptr = &f; // error: cannot convert ‘float*’ to ‘int*’ in assignment
    std::cout << "The value of *ptr is : " << (float*)ptr << std::endl;  
    return 0;  
}  
