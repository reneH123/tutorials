#include <iostream>

using namespace std;

int glob = 123;

int foo(){
  ++glob;
  cout << "Current foo is: " << glob << endl;
  return glob;
}

int bar(){
  glob=2*glob;
  cout << "Current bar is: " << glob << endl;
}

int main(){
  cout << "Hello world!" << endl;
  glob = bar() + bar() + foo();  //  ==  (bar()+bar()) + foo(), so it is left-associative!
  cout << "Glob: " << glob << endl;
  return 0;
}
