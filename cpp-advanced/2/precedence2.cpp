#include <stdio.h>

int glob=111;

int foo(int c){
  printf(" -- foo:: glob=%d, c=%d\n",glob,c);
  glob = (glob<<1) - 5;
}

int main(){
  printf("Hello world!\n");
  int a; int b;
  a=b=foo(5)+foo(6);    // is ((111<<1)-5) + ((217<<1)-5)  = 217+429 = 646;   a:646, b:646, glob:429
  printf(" a:%d, b:%d\n",a,b);  // foo(5) is executed first, then foo(6)  -> '+' is left-associative
  printf("glob: %d\n",glob);
  return 0;
}
