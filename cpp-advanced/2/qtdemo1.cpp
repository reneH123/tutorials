// g++ qtdemo1.cpp -o qtdemo1
#include <qapplication.h>
#include <qlabel.h>

int main(int argc, char* argv[])
{
  QApplication a(argc, argv);
  QLabel hello("Hello world!", 0);
  hello.resize(100, 30);
  a.setMainWidget(&hello);
  hello.show();
  return a.exec();
}

// TODO: use controls and event listeners for more details!
