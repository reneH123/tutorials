#include <iostream>

using namespace std;

int main(){
  int a=555;
  void *ptr = &a;  // ptr becomes factually a reference parameter
  printf("Hello world!\n");
  cout << "data: " << (int*)ptr << " ptr: "<< ptr << endl;

  return 0;
}
