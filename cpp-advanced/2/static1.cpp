#include <iostream>

using namespace std;

class Class {
public:
	static int StaticPart;  // this is only a (static variable) declaration, not a definition! Static vars MUST be defined outside of a class!
	int NonStaticPart=5;

	void print(void) {
		cout << "(StaticPart,NonStaticPart)=" << Class::StaticPart << "," << NonStaticPart << ")" << endl;
		// just:  StaticPart  allowed
		//  Class::StaticPart  allowed
		// this->StaticPart  allowed,    but ALL only if StaticPart is declared
	}
	static void incParts(){
		Class newObj;
		if (newObj.NonStaticPart==5)  // that would be okay, although static is calling non-static!
			StaticPart++;
	}
};

int Class::StaticPart = 123;  // this is a valid definition! Initialisation is optional

int main() {
	Class c1, c2;

	c1.print();  // 123,5
	c1.incParts();
	c2.print();  // 124,5

	return 0;
}

