#include <list>
#include <string>
#include <iostream>

using namespace std;

void dumplist(list<string> &l){
  list<string>::iterator it;
  for (it=l.begin(); it!=l.end(); ++it){
    cout << "Content: " << *it << endl;
  }
}

int main(){
  cout << "Hello world!" << endl;

  std::list<std::string> slist;
  slist.push_front("string 1");
  slist.push_front("string 2");
  slist.push_back("string 3");

  dumplist(slist);
  slist.clear();

  cout << "Bye!" << endl;
  return 0;
}
