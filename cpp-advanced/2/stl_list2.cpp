#include <deque>
#include <iostream>
#include <iterator>
#include <list>
#include <vector>

using namespace std;

template<class T>
void dumpCollection(const T& start, const T& end) {
	int cnt = 0;
	for (T it = start; it != end; it++) {
		cout << *it << " ";
		cnt++;
	}
	if (cnt > 0)
		--cnt;
	cout << " (isEmpty=" << (cnt == 0) << ",sz=" << cnt << ")";
	cout << endl;
}

int main() {
	int a[]={1,2,3,4,5};
	int b[]={6,7,8,9,10};
	int c[]={11,12,13,14,15};
	list<int> l1(a,a+5);  // 1 2 3 4 5  (isEmpty=0,sz=4)
	list<int> l2(b,b+5); // 6 7 8 9 10  (isEmpty=0,sz=4)
	list<int> l3(c,c+5); // 11 12 13 14 15  (isEmpty=0,sz=4)

	// equivalent to list concatenation:
	l2.splice(l2.end(),l3);  // l2: 6 7 8 9 10 11 12 13 14 15  (isEmpty=0,sz=9)

	// moving one element - '15'
	list<int>::iterator it = l2.begin();
	advance(it,9);
	l1.splice(l1.end(),l2,it);  // l1: 1 2 3 4 5 15  (isEmpty=0,sz=5)
	// in l1 at position 11.end() the (rest sublist) of l2 ( l2': 15 NULL) is added

	// moving range of elements
	it = l1.end();
	advance(it,-1);
	l1.splice(it,l2,l2.begin(),l2.end());  // l1: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15  (isEmpty=0,sz=14)
	//           ^--------------------^  the whole l2 list is inserted into l1 at position it

	//dumpCollection(l1.begin(),l1.end());

	return 0;
}

