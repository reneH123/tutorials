#include <deque>
#include <iostream>
#include <iterator>
#include <list>
#include <vector>

using namespace std;

template<class T>
void dumpCollection(const T& start, const T& end) {
	int cnt = 0;
	for (T it = start; it != end; it++) {
		cout << *it << " ";
		cnt++;
	}
	cout << " (isEmpty=" << (cnt == 0) << ",sz=" << cnt << ")";
	cout << endl;
}

bool removeOdd(int element){
	return (element%2);
}

int main() {
	int a[]={1,2,3,4,5};
	int b[]={6,7,8,9,10};

	list<int> l1(a,a+4);
	list<int> l2(b,b+4);
	list<int> l2r(l2.rbegin(),l2.rend());
	l1.merge(l2r);  // 1 2 3 4 9 8 7 6

	dumpCollection(l1.begin(),l1.end());

	l1.sort();

	dumpCollection(l1.begin(),l1.end());  // 1 2 3 4 6 7 8 9

	return 0;
}

