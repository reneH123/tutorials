#include <vector>
#include <list>
#include <string>
#include <iostream>

using namespace std;

void dumpvector(vector<string> &v){
  int counter = 0;
  vector<string>::iterator it;
  for (it=v.begin(); it!=v.end(); ++it){
    cout << "Content(" << counter++ << "): " << *it << endl;
  }
  cout << "Elements: " << v.size() << endl;
}

int main(){
  cout << "Creating some sample vector:" << endl;

  std::vector<std::string> svec(1, "aaa"); // init 10x "aaa"
  svec.push_back("string 1");
  svec.push_back("string 2");
  svec.push_back("string 3");
  svec.push_back("string 4");
  svec.push_back("string 5");
  dumpvector(svec);
  cout << endl;

  cout << "Now erase relatively to iterator (end)" << endl;
  vector<string>::iterator it = svec.end();
  // ++it;   // ++it at the end does is ignored!
  // --it;  would be the same as it=it-0; (no pointer reassignment)!
  it=it-2;  // select the element before the last, here "4"
  svec.erase(it);  // if svec is clear, then Segmentation Fault!
  dumpvector(svec);
  cout << endl;

  cout << "Now erase relatively to iterator (begin)" << endl;
  it = svec.begin();
  ++it;  // select 2nd element from start, here "1"
  svec.erase(it);  // if svec is clear, then Segmentation Fault!
  dumpvector(svec);
  cout << endl;

  // reinitialise vector (svec)
  cout << "Clearing vector" << endl;
  svec.clear();
  dumpvector(svec);
  
  cout << "Adding 5 elements" << endl;
  for (int i=0;i<5;i++){ // strings trimmed by 1 char; in total 4 removals
    svec.push_back("string " + i);  // returns "string", "tring", .."ng"
  }
  dumpvector(svec);
  cout << endl;

  // insert at front  (into svec2)
  cout << "Insertion at front" << endl;
  std::vector<std::string> svec2(1, "blub");
  svec2.insert(svec2.begin(), "hello");
  svec2.insert(svec2.end()-1, "world!");  // adding "world!" before the last vector element
  dumpvector(svec2);
  cout << endl;

  // insert several elements I
  cout << "Inserting elements I" << endl;
  svec2.insert(svec2.begin(), 2, "xxx");  // add twice xxx at front
  svec2.insert(svec2.end(), 2, "yyy");    // .. and yyy at rear
  dumpvector(svec2);
  cout << endl;


  cout << "Insert a whole container" << endl;
  cout << "  BEFORE svec: " << endl;
  dumpvector(svec);
  cout << "  BEFORE svec2: " << endl;
  dumpvector(svec2);
  cout << endl;

  //   add entire svec2 at the beginning of svec:
  svec.insert(svec.begin(),svec2.begin(),svec2.end());

  cout << "  AFTER svec: " << endl;
  dumpvector(svec);
  cout << "  AFTER svec2: " << endl;
  dumpvector(svec2);
  cout << endl;


  // erase selectively:  2nd xxx, blub and ring
  cout << "Erase selectively elements" << endl;
  svec.erase(svec.end()-3);    // ring
  svec.erase(svec.begin()+4);  // first elements starts with index "0"
  svec.erase(svec.begin()+1);  // the 2nd xxx
  dumpvector(svec);
  cout << endl;

  // copy a vector (or a segment of an vector)
  cout << "Copy a vector (segment)" << endl;
  vector<string> copyVec;
  copyVec.clear();
  //cout << "BEFORE copyVec:" << endl;
  //dumpvector(copyVec);
  cout << "BEFORE svec:" << endl;
  dumpvector(svec);
  cout << endl;
  list<string> copyList;
  copyList.clear();
  std::copy(svec.begin(),svec.end(), std::front_inserter(copyList));  // will not work if copyList.begin() is just added;  may also be a front_inserter (then it is reversed!)

  cout << "AFTER copyList:" << endl;
list<string>::iterator it2;
  for (it2 = copyList.begin(); it2!=copyList.end(); it2++){
    cout << "CONTENT(l)=" << *it2 << endl;
  }
  cout << "AFTER svec:" << endl;
  dumpvector(svec);
  cout << endl;


  cout << "Bye!" << endl;
  return 0;
}
