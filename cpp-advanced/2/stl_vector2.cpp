#include <vector>
#include <iostream>

using namespace std;

class A{
  int number; // private by default
public:
  A(int _number): number(_number) {
    cout << "A c-tor number:" << this->number << endl;
  }
};

int main(){
  vector<A> v1;
  v1.push_back(1);  // C++11; calls implicitly constructor(int _number)

  A *a2=new A(2);   // ditto, but this notation may be more familiar
  v1.push_back(*a2);
  return 0;
}
