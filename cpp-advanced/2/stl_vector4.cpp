#include <vector>
#include <iostream>

using namespace std;

class A{
  int number;
public:
  A(int _number): number(_number)
  {
    cout<<"Normal constructor"<<endl;
  }
  A()
  {
    cout<<"Default constructor"<<endl;
  }
  A(const A& source)
  {
    number=source.number;
    cout<<"Copy constructor" <<endl;
  }
  A& operator=(const A& source)
  {
    number=source.number;
    cout<<"Assignment operator"<<endl;
    return *this;
  }
  ~A()
  {
    cout<<"Destructor"<<endl;
  }
};

int main(){
  vector<A> v1;
  v1.push_back(1); // <- Normal-c-tor, Copy object, d-tor temporary object (this is automatically triggered by C+11's constant initializer!)
  cout << "First ready" << endl << endl;  // NC, CC, d-tor

  v1.push_back(2);  // since internal memory grows, now 2x CC from temp objects, and 2x d-tor is required
  cout << "Second ready" << endl << endl; // NC, CC, CC, d-tor, d-tor

  v1.push_back(3); // in analogy to previous
  cout << "Third ready" << endl << endl; // NC, CC,CC,CC,d-tor,d-tor,d-tor

  return 0;  // v1 contains 3x A, so 3x d-tor
}
