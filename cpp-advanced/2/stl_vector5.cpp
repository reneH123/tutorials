#include <deque>
#include <iostream>
#include <iterator>
#include <list>
#include <vector>

using namespace std;

template<class T>
void dumpCollection(const T& start, const T& end) {
	for (T it = start; it != end; it++) {
		cout << *it << " ";
	}
	cout << endl;
}

int main() {
	vector<int> v;  // only push_back/pop_back available   (=LIFO, stack)
	list<int> l;    // same as deque
	deque<int> d;   // push_back or push_front    (=FIFO)

	// init data structures:
	for (int i = 0; i < 10; i++) {
		v.push_back(i);
		l.push_back(i);
		d.push_back(i);
	}

	// 1) vector operations
	v.erase(v.begin() + 3);  // delete 4th (=="3") element in vector
	v.erase(v.end() - 2);    // delete "8"

	v.pop_back();   // a vector implements a LIFO (stack)
	v.pop_back();   // vector(stack) have no peek or so; because that can always be accessed via: v.end()-1 "==" v.rbegin()

	int lastV = *(v.end()-1);
	cout << "v.end()-1:" <<  lastV << endl;
	int lastV2 = *(v.rbegin());
	cout << "v.rbegin():" <<  lastV2 << endl;

#if 0
	if ((v.end()-1) == v.rbegin()){   // COMPILER error: 1st operand returns an normal_iterator, 2nd operand a reverse_iterator!
		cout << "BBB" << endl;
	}
#endif

	cout << "Dumping v:   (isEmpty=" << v.empty() << ",sz=" << v.size() << ")" << endl;
	dumpCollection(v.begin(), v.end());
#if 0
	for (auto it=v.begin();it != v.end(); it++) {
		cout << *it << " ";
	}
#endif

	// 2) list operations:
	l.pop_front();
	l.pop_back();

	cout << "Dumping l:   (isEmpty=" << l.empty() << ",sz=" << l.size() << ")" << endl;
	l.reverse();  // reverse list
	dumpCollection(l.begin(), l.end());

	d.pop_back();

	cout << "Dumping d:   (isEmpty=" << d.empty() << ",sz=" << d.size() << ")" << endl;
	dumpCollection(d.begin(), d.end());  // difference to list: has dequeue/enqueue

	cout << endl;

	return 0;
}

/*
 Dumping v:   (isEmpty=0,sz=6)
0 1 2 4 5 6
Dumping l:   (isEmpty=0,sz=8)
1 2 3 4 5 6 7 8
Dumping d:   (isEmpty=0,sz=9)
0 1 2 3 4 5 6 7 8
 */

