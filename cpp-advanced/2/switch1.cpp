#include <iostream>
#include <stdio.h>

using namespace std;

inline int foo(){
  return 5;
}

int main(int argc, char** argv){
  printf("Hello world!\n");
  cout << "ARGC: " << argc << endl;
  for (int i=0;i<argc;i++){
    cout << "Argument " << i << ": " << argv[i] << endl;
  }
  cout << endl;
  switch (argc){ // must be an integer or castable to int
    case 0: cout << "case 0" << endl;
    // case >1: cout << endl;  // error, since must be an ordinary type!
    //case 2.2: cout << endl;  // error, must be an integer!
    case -12: cout << endl;  // OK!
    case 'a': cout;
    // case (1+foo()): cout << endl;  // error: non-constexpr ! even error when foo is const
    // case 1+foo(): cout << endl; // error: also an error when inlined!
    default: cout << "Some other case" << endl;
  }

  return 0;
}
