#include <iostream>

#if 1
template<typename T> class Handle
{
  T* pointer;  // pointer to actual object
  int* pcount; // the number of Handle’s pointing to the same object
public:
  explicit Handle(T* pointer_):
    pointer(pointer_), pcount(new int(1)) {}

  explicit Handle(const Handle<T>& r) throw():
    pointer(r.pointer), pcount(r.pcount)
  {
    ++(*pcount);
  }

  ~Handle() throw()
  {
    if (--(*pcount) == 0)
    {
      delete pointer;
      delete pcount;
    }
  }

  T* operator->()
  {
    return pointer;
  }

  T& operator*()
  {
    return *pointer;
  }

  Handle& operator= (const Handle& rhs) throw()
  {
    if (pointer == rhs.pointer)
      return *this;
    if (--(*pcount) == 0) 
    {
      delete pointer;
      delete pcount;
    }
    pointer = rhs.pointer;
    pcount = rhs.pcount;
    ++(*pcount);
    return *this;
  }
};
#endif

int main(){
  // TODO!
  return 0;
}
