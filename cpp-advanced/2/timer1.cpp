#include <iostream>

//#include <sys/times.h> /* tms */
#include <unistd.h>
#include <time.h> // ...


using namespace std;

int main(){
#if 0 
/* for clock ticks per sec */
tms t1, t2;
times(&t1);
/* perform operations... */
sleep(1);  // ...

times(&t2);
tms diff;
// user time:
diff.tms_utime = t2.tms_utime - t1.tms_utime;
// system time:
diff.tms_stime = t2.tms_stime - t1.tms_stime;
// user time, children processes:
diff.tms_cutime = t2.tms_cutime - t1.tms_cutime;
// system time, children processes:
diff.tms_cstime = t2.tms_cstime - t1.tms_cstime;
double ticks = sysconf(_SC_CLK_TCK);
double cpu_time;
cpu_time = double(diff.tms_utime + diff.tms_stime)/ticks;
#endif
clock_t start = clock();
sleep(1); // do sth...
clock_t stop = clock();

cout << "Start: " << start << endl;
cout << "Stop: " << stop << endl;

//std::cout << "CPU time elapse: " << cpu_time << "diff.tms_utime: " << diff.tms_utime << " diff.tms_stime: " << diff.tms_stime << "Ticks: " << ticks << std::endl;
}
