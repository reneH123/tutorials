#include <iostream>
#include <valarray>

using namespace std;

const int& DIMENSION = 7;

void init(valarray<int>& arr){
  for (int i=0; i<DIMENSION; i++){
    arr[i] = (int)random()%1000;
  }
}

void dump(const valarray<int>& arr){
  cout << "ValArray has " << arr.size() << " element(s): (";
  for (int i=0; i<DIMENSION-1; i++){
    cout << " " << arr[i];
  }
  cout << " )" << endl;
}



int main(){
  valarray<int> u1(DIMENSION), u2(DIMENSION), v(2); // all zero initially

  srand(time(NULL));

  init(u1);
  init(u2);

  dump(u1);
  dump(u2);

  v=u1+u2;  // or e.g. v=u1*u1+u2*u2;  works, with correct precedence of operators; valarray ideally to be used as vector class, etc.
  dump(v);  // v is widened to DIMENSION

  return 0;
}
