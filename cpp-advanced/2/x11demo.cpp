// g++ x11demo.cpp -lX11 -o x11demo

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>


#define STRING  "Hello, world"
#define BORDER   1
#define FONT    "fixed"

XWMHints xwmh = {
  (InputHint|StateHint),   // flags
  False,                   // input
  NormalState,             // initial_state
  0,                       // icon pixmap
  0,                       // icon window
  0, 0,                    // icon location
  0,                       // icon mask
  0                        // window group
};


main(int argc, char **argv)
{
Display *dpy;
Window win;
GC gc;
XFontStruct *fontstruct;
unsigned long fth, pad;
unsigned long fg, bg, bd;
unsigned long bw;
XGCValues
gcv;
XEvent
event;
XSizeHints xsh;
char *geomSpec;
XSetWindowAttributes xswa;


if ((dpy = XOpenDisplay(NULL)) == NULL) {
fprintf(stderr, "%s: can’t open %s\en", argv[0], XDisplayName(NULL));
  exit(1);
}

if ((fontstruct = XLoadQueryFont(dpy, FONT)) == NULL) {
  fprintf(stderr, "%s: display %s doesn’t know font %s\en", argv[0], DisplayString(dpy), FONT);
  exit(1);
}

fth = fontstruct->max_bounds.ascent +
fontstruct->max_bounds.descent;
bd = WhitePixel(dpy, DefaultScreen(dpy));
bg = BlackPixel(dpy, DefaultScreen(dpy));
fg = WhitePixel(dpy, DefaultScreen(dpy));
pad = BORDER;
bw = 1;
xsh.flags = (PPosition | PSize);
xsh.height = fth + pad * 2;
xsh.width = XTextWidth(fontstruct, STRING,
strlen(STRING)) + pad * 2;
xsh.x = (DisplayWidth(dpy,DefaultScreen(dpy))-xsh.width)/2;
xsh.y = (DisplayHeight(dpy,DefaultScreen(dpy))-xsh.height)/2;
win = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy),
xsh.x, xsh.y, xsh.width, xsh.height,
bw, bd, bg);

XSetStandardProperties(dpy, win, STRING, STRING, None,
argv, argc, &xsh);
XSetWMHints(dpy, win, &xwmh);
xswa.colormap = DefaultColormap(dpy, DefaultScreen(dpy));
xswa.bit_gravity = CenterGravity;
XChangeWindowAttributes(dpy, win,
(CWColormap | CWBitGravity), &xswa);
gcv.font = fontstruct->fid;
gcv.foreground = fg;
gcv.background = bg;
gc = XCreateGC(dpy, win,
(GCFont | GCForeground | GCBackground), &gcv);
XSelectInput(dpy, win, ExposureMask);
XMapWindow(dpy, win);

/*
* Loop forever, examining each event.
*/
while (1) {
XNextEvent(dpy, &event);
if (event.type == Expose && event.xexpose.count == 0) {
XWindowAttributes xwa;
int
x, y;
while (XCheckTypedEvent(dpy, Expose, &event));
if (XGetWindowAttributes(dpy, win, &xwa) == 0)
break;
x = (xwa.width - XTextWidth(fontstruct, STRING,
strlen(STRING))) / 2;
y = (xwa.height + fontstruct->max_bounds.ascent
- fontstruct->max_bounds.descent) / 2;
XClearWindow(dpy, win);
XDrawString(dpy, win, gc, x, y, STRING, strlen(STRING));
}
}
exit(1);

}


