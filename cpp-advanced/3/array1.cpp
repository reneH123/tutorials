//#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(){
  printf("Hello world!\n");

  // case 1: constant array
  {
    int arrrayOfInts[5] = {0,1111,222,3,4}; 
    // array size may be dropped in declaration (but required, e.g. when looping)
    // the generated code will be 100% identical!

    for (int i=0;i<5;i++){   // if upper bound is, e.g. by accident, bigger, then stack overflow, and crash/SEGV is not 100% !!
      printf("arr[%d]=%d\n",i,arrrayOfInts[i]);
      // HERE: upper limit 500 - still no crash!
      //  if upper limit approx. 5000+ - SEGV!
    }
  }


  // case 2: constant matrix
  int const n_rows = 3;
  int const n_cols = 7;
  int const m[n_rows][n_cols] =
  {
    { 1, 2, 3, 4, 5, 6, 7 },
    { 8, 9, 10, 11, 12, 13, 14 },
    { 15, 16, 17, 18, 19, 20, 21 }
  };

  {
    for (int i=0;i<n_rows;i++){
      for (int j=0;j<n_cols;j++){
        printf("%d\t",m[i][j]);
      }
      printf("\n");
    }
  }

  return EXIT_SUCCESS;
}
