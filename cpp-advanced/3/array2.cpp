#include <iostream>
#include <stdio.h>
#include <algorithm>  // for std::sort

using namespace std;

// cmpints is in fact a binary predicate
//  types of p1 and p2 are given and depend on concrete types, e.g. here int
static int cmpints(const void *p1, const void *p2){
 int x1 = *(const int*)p1;   // equivalent here to: *(int const*)p1;
 int x2 = *(const int*)p2;
 return x1>x2;
}

#if 0
int main(){
#else
  auto main() -> int{   // C++11 etc. jargon
#endif
  printf("How many elements do you want to sort:\n");
  int x;
  cin >> x;
  cout << "X was: " << x << endl;
  #if 0
    int arr[x];  // ERROR: because dynamically allocating won't (always, only if the compiler is clever to allocate the right amount correctly beforehand) work if allocated to the stack!
    delete[] arr; // invalid, since in the "wrong" memory segment selected, hence SEGV !
  #else
  int* arr = new int[x];  // use this instead (because must be on the heap!)
  #endif

  { // init array with some data:  (otherwise arr will contain random value, because all non-0 initialised!)
    for (int i=0;i<x;i++)
      arr[i]=111-i;
  }

  { 
    cout << "Before sorting: " << endl;
    for (int i=0;i<x;i++)
      printf("%d ",arr[i]);
    printf("\n");
  }

  /*std::*/sort(arr, arr+x);  // pointer to first and last element
  // OR: use standard QuickSort from stdlib.h:
//  qsort(&arr[0], x, sizeof(int), cmpints);  // base address, total elements, size of any element, compare predicate

  {
    cout << "After sorting: " << endl;
    for (int i=0;i<x;i++)
      printf("%d ",arr[i]);
    printf("\n");
  }

  // dynamically deallocating:
printf("arr ptr add: 0x%x\n", &arr);
printf("arr[x-1]=%d\n",arr[x-1]);
  delete[] arr;

  cout << "Bye!" << endl;

  return EXIT_SUCCESS;
}
