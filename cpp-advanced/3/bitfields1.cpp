#include <iostream>
#include <stdio.h>

// bit field specifier: ":1"
struct mybitfield{
  unsigned aaa :1;
  unsigned bbb :1;
  unsigned ccc :1;
  unsigned ddd :1;

  unsigned eee :1;
  unsigned fff :1;
  unsigned ggg :1;
  unsigned hhh :1;  // aaa..hhh consumes 1 Byte in total

  unsigned iii :24;  // bitfield of 24 bits  => until here 1+3=4 Bytes

  unsigned jjj: 7;  // if jjj was bitfield big (e.g. >10k), then SEGV!
};

using namespace std;

int main(){
  printf("Hello world!\n");
  printf("  mybitfield sz=%d\n", (int)sizeof(mybitfield));
  mybitfield mbf;
  mbf.hhh=9;   // 1 bit;  on little endian arch: the LSB-bit is stored
  mbf.jjj=131;  // 131 = 3  modulo 128 (because 2^7); Compiler warning here!
  cout << "HHH:" << mbf.hhh << endl;
  cout << "JJJ:" << mbf.jjj << endl;  // "3"

  return EXIT_SUCCESS;
}
