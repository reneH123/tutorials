#include <iostream>
#include <stdio.h>

using namespace std;

class Bar{
 public:
  int ID=555;
}; // some class

typedef struct Foo{
public:
  Bar& getBar(){}  // deref does not change anything wrt non-init!
} Foo_t;


int main(){
  cout << "Hello world!" << endl;

  struct Foo foo;
  cout << "sizeof(Foo)=" << sizeof(Foo_t) << ", sizeof(Bar)=" << sizeof(Bar) << endl; // 1 Byte if Foo is empty w.r.t. sizeof(Foo) and sizeof(Foo_t) !
  auto f = foo.getBar();
  cout << "f.ID is " << f.ID << endl;  // output is not 555 ! It's some random stuff, because not initialised

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
