#include <iostream>
#include <stdio.h>

using namespace std;

class Bar{
 public:
  int ID=555;
}; // some class

typedef struct Foo{
private:
  Bar mybar;
public:
  Bar& getBar(){
    // original: return mybar;
    // deduplicated version: call the invariant getBar instead:
    // return getBar();   ERROR, although compiles!
    // return const_cast<Bar&>(getBar());  ERROR, although compiles too!
    cout << "We are in 1st getBar"<< endl;
    return const_cast<Bar&>(const_cast<const Foo*>(this)->getBar());  // OK!
    // Explanation: 1st: cast to a constant Foo instance in order to delegate to overloaded getBar() method
    //              2nd: return a (non-constant) Bar&
  }
  const Bar& getBar() const{
    // 1st const: returning Bar may not be used as lval
    // 2nd const: this getBar() may not alter the internal state of this struct/class
    cout << "We are in 2nd getBar" << endl;
    return mybar;
  }
} Foo_t;


int main(){
  cout << "Hello world!" << endl;

  struct Foo foo;
  cout << "sizeof(Foo)=" << sizeof(Foo_t) << ", sizeof(Bar)=" << sizeof(Bar) << endl; // 1 Byte if Foo is empty w.r.t. sizeof(Foo) and sizeof(Foo_t) !
  auto f = foo.getBar();
  cout << "f.ID is " << f.ID << endl;  // output is 555, because initialised !
  
  const struct Foo foo2;
  auto f2 = foo2.getBar();  // explicitly calls the 2nd getBar
  // the auto call is equivalent to: const Bar& bar2 = foo2.getBar();

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
