#include <iostream>
#include <stdio.h>


using namespace std;

int main(){
  cout << "Hello world!" << endl;

{ // OK!
  int a = 11;
  int* b = &a;
  cout << "a=" << a << ", b=" << b << ", *b=" << *b << endl;
}

{ // OK!
  const int a = 11;
  int* b = const_cast<int*>(&a);
  cout << "a=" << a << ", b=" << b << ", *b=" << *b << endl;
}

#if 0
{
  const int a = 11;
  int b = &a;   // COMPILATION ERROR!
}
#endif

{ // OK!
  const int a = 11;
  const int &b = a;   // is C99-compatible
  cout << "a=" << a << ", b=" << b << ", &b=" << &b << endl;
}

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
