//g++ -std=c++98 const4.cpp && ./a.out
//g++ -std=c99 const4.cpp && ./a.out
#include <iostream>
#include <stdio.h>


using namespace std;

int main(){
  cout << "Hello world!" << endl;

  int a=0;
  int b=2;

  // all pointers to a:
  const int* pA=&a; // pointer may alter, its content may not! content is ro
  int* const pB=&a; // pointer may not alter, its content may! ADR is ro
  const int* const pC=&a;

  const int& pD=a;  // must be (non-null) initialised; C99 and C++98 compatible! "safest" variant

  //*pA=b; // ERROR, content is readonly
  pA=&b;
  cout << "pA=" << pA << ", *pA=" << *pA << endl;

  //pB=&a;  // ERROR: pointer is readonly
  cout << "pB=" << pB << ", *pB=" << *pB << endl;
  *pB=b;
  cout << "pB=" << pB << ", *pB=" << *pB << endl;

  //pC=&a;  // ERROR: pointer is readonly
  //*pC=a;  // ERROR: content is readonly, too!

  //pD=a;   // ERROR: pointer and content are readonly!

  cout << "Bye!" << endl;
  return 0;
}
