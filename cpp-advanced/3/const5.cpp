#include <iostream>
#include <string>

using namespace std;

class AAA{
private:
	const int aaa;
public:
	AAA(int _aaa): aaa(_aaa)   // OK!
    {
		//this->aaa = _aaa;  // NOK! allowed neither here nor anywhere else inside AAA!
	}
	const int getAAA(){
		return this->aaa;
	}
};

int main() {
	AAA *aaa1 = new AAA(555);
	cout << "AAA was: " << aaa1->getAAA() << endl;
	delete aaa1;
	return 0;
}

