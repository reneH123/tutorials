#include <iostream>
#include <string>

using namespace std;

class AAA{
private:
	int aaa;
public:
	AAA(int _aaa): aaa(_aaa)   // OK!
    {
		//this->aaa = _aaa;  // NOK! allowed neither here nor anywhere else inside AAA!
	}
	int getAAA(){
		return this->aaa;
	}
	int setAAA(int aaa){
		this->aaa = aaa;
	}

};

int main() {
	const AAA *aaa1 = new AAA(555);

	int aaa2 = const_cast<AAA*>(aaa1)->getAAA();
	const_cast<AAA*>(aaa1)->setAAA(444);

	//aaa1->setAAA(444); // COMPILER error (because const!)
	// aaa1->getAAA();  // even get() causes a COMPILER error !!

	cout << "AAA: " << const_cast<AAA*>(aaa1)->getAAA() << endl;

	delete aaa1;
	return 0;
}

