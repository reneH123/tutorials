#include <iostream>
#include <string>
#include <exception>

using namespace std;

int main() {
	float a,b;
	try{
		throw new int(999); //string("hi!");
	}
	 catch (string s){
		 cout << "String exception!" << endl;
	 }
	 catch (int i){
		 cout << "AAA-0: " << i << endl;
	 }
	 catch (int* i){
		 cout << "AAA-1: "<< *i << endl;  // hits it
	 }
	 catch (int& i){
		 cout << "AAA-2: " << i << endl;
	 }
	 catch (exception e){
		 cout << "AAA-3: " << e.what() << endl;
	 }
	cout << a/b << endl;
	return 0;
}

