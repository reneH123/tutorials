#include <iostream>
#include <string>

using namespace std;

class Class {
public:
	Class(void) {
		cout << "ctor" << endl;
	}
	~Class(void) {
		cout << "dtor" << endl;
	}
	void Hello() {
		cout << "Hi!" << endl;
	}
};

void dosth(int i) {
	if (i == 0)
		throw string("exception-1");
	Class obj;  // ctor is called just here!
	if (i == 1)
		throw string("exception-2");   // call all dtors before control is passed to exception-handler (catch-case above)
	obj.Hello();
	if (i == 2)
		throw string("exception-3");
}

int main() {
	for (int i = 0; i < 3; i++) {
		try {
			cout << "----" << endl;
			dosth(i);
		} catch (string &e) {
			cout << e << endl;
		}
	}
	return 0;
}


/*
 ----
exception-1
----
ctor
dtor
exception-2
----
ctor
Hi!
dtor
exception-3
 */

