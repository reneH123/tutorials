#include <iostream>
#include <string>

using namespace std;

class Class {
public:
	string msg;
	Class(string txt) :
			msg(txt) {
	}
};

void lastchance(void){
	cout << "That was your last chance!" << endl;
}

void function(void) throw ()  // no exception specified
{
	throw string("some message");
}

int main() {
	// registers a function to be called in case of an unexpected exception:
	set_unexpected(lastchance);  // requires a function pointer
	try {
		function();
	} catch (string &e) {
		cout << "Caught exception!" << endl;
	}
}

