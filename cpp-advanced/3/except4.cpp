#include <exception>
#include <iostream>

using namespace std;

void unexp(void) {
	cout << "Unexpected exception arrived!" << endl;
	throw 3.155;  // if commented out, then program would terminate immediately with a system eh-message (only!)
	// by throwing here an (empty) exception, the eh-handling above is kicked on, and bad_exception catch is triggered
	// It is always bad_exception, regardless which type /empty is returned here!!
}

void function(void) throw (int, bad_exception) {
	throw 3.14;  // throws a double exception
}

int main() {
	set_unexpected(unexp);
	try {
		function();
	} catch (double f) { // although double exception are treated here, it would be a violation towards function's eh-list, therefore bad_exception!
		cout << "Got double" << endl; // not visited! Add double in functions eh-list in order to visit!
	} catch (bad_exception &bad) {
		cout << "It's so bad.. " << endl;
	}

	cout << "Finished" << endl;
	return 0;
}

/*
 Unexpected exception arrived!
 It's so bad..
 Finished
 */

