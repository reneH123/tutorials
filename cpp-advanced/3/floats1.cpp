#include <iostream>

using namespace std;

int main(){
  double a=0.1;  // if a,b,c were 10x bigger, then would be EQUAL (as well as most other cases)!
  double b=0.2;
  double c=0.3;

  printf("Hello world!\n");

  if (a+b==c){  // in approx. 99.99% of all other cases this would be it !!
    cout << "EQUAL" << endl;
  }
  else{
    cout << "NOT EQUAL" << endl;
    cout << "  Because there is a difference of epsilon: " << (double)(c-(a+b)) << endl;
  }

  printf("Bye!\n");
  return EXIT_SUCCESS;
}
