#include <iostream>
#include <stdio.h>

using namespace std;

class Accessor{
public:
  void private_accessor();
};


class PrivateHolder{
public:
  PrivateHolder(int val): private_value(val){}
private:
  int private_value;
  friend void friend_function();             // case 1) common function; specifies which function/method, etc. may access private fields
  //friend void friend_function2();  // if commented out, then f_f2 will not be able to access private_value !
  friend void Accessor::private_accessor();  // case 2) other class'es methods
  friend class Accessor;                     // case 3) ALL methods/c-tors/d-tors may fully access PrivateHolder's state
};

// Comment: 'friend' allows full (public) access to protected/private class members which otherwise would not be allowed/restricted; it is implemented as an exclusion list for visibility restrictions

// Comment: 'friend' breaks actually encapsulation, it does not "enhance" it really, because it makes it visible to the outside without real OO-technique (e.g. getter/setter); its internal state may completely be broken/invalidated without any transition soley by its class methods (=transitions become totally unbound and control inverts!)


void Accessor::private_accessor(){
    PrivateHolder ph(30);
    cout << ph.private_value << endl;    
  }

void friend_function(){
  PrivateHolder ph(10);
  cout << ph.private_value << endl;
}

#if 0
void friend_function2(){
  PrivateHolder ph(20);
  cout << ph.private_value << endl;  // COMPILER ERROR as long as f_f2 is not registered inside class PrivateHolder as friend function !
}
#endif


int main(){
  cout << "Hello world!" << endl;

  friend_function();

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
