#include <iostream>
#include <string>

using namespace std;

class Class{
	friend class Friend;  // is a declaration only (if no definition follows, NO COMPILER error/warning until actually used!)
private:
	int field;
	void print(void){
		cout << "The secret number: " << field << endl;
	}
};

class Friend{
public:
	void dosth(Class &c){
		c.field = 100;
		c.print();
	}
};


int main() {
	Class o;
	Friend f;
	f.dosth(o);
	return 0;
}

/* The secret number: 100 */

