#include <iostream>

using namespace std;

typedef struct{
  char *name;
  char *firstname;
  int age;
} t_person;


void f(t_person* p){}

template<class T>
void g(T* p){}

void h(nullptr_t p){}

int main(){

  // boolean values:
  cout << "Hi! true: " << (int)true << " false: " << (int)false << endl;
  int i=5;
  if (i!=false){  // IS NOT EQUIVALENT TO: (i==true) !!
    cout << "IF-case" << endl;
  }
  else{
    cout << "ELSE-case" << endl;
  }

  //  pointer constant:  nullptr
  t_person *p1 = new t_person();
  p1->age=69;
  p1=nullptr; // use for C++11 and newer; instead of p1=NULL;
  //p1->age=96; // causes SEGV
  // success

  // nullptr compatible types:
  f(nullptr);  // OK: compatible with any type (except generic type!)
  // ERROR: g(nullptr);  // type mismatch: T* and nullptr
  // ERROR: g(NULL);  // T* and NULL also not allowed !
  g(p1);  // OK!
  h(nullptr); // OK!

  // boolean constants:
  bool b=true;
  cout << "b is " << b << endl;
  !b;  // does not invert b, it is a valid statement and its value doesn't change
  cout << "b is " << b << endl;

  return EXIT_SUCCESS;
}

// 0 .. false
// 1 .. true (all !=0; but: be careful how testing; != instead of ==)

// RULE: nullptr is not compatible with a generic type (from template class) !
