#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

int main(){
  cout << "Hello world!" << endl;

  vector<float> v(10); // initialises automatically all to zeroes

  // supported from C++11 onwards:
#if 0
  for (auto el: v){  // only to be used as rval
    el=123;          // lval-usage accepted, but dropped!
    v[el]=123;       // even this is IGNORED (however, not an error) !
  }
#elif 0          // OK-case
  for (auto&& el: v){   // this one works! (C++11 jargon!)
    el=444;
    //v[el]=555;    // this won't work (since internally hold as a const-expression for each element)
  }
#elif 0
  vector<float>::iterator it;          // OK-case
  for (it=v.begin();it!=v.end();++it)
    *it=123;
#else
  // OK-case too
  for (int i=0;i<10;i++)
    v[i]=i+111;
#endif

  // output: only if elements to be read(!); C++11-specific(!)
  for (auto el: v){
    cout << el << endl;   // very problematic: it is implicitly copy-constructing all the time!!!
  }

  // scope coverings:
  int a=333;
  for (a=5;a<10;++a){
    cout << "a is: " << a << endl;
  }
  cout << "After loop, a is: " << a << endl;  // if there was not a==333, then a would be 10

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
