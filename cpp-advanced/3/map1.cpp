#include <iostream>
#include <stdio.h>
#include <map>

using namespace std;

int main(){
  cout << "Hello world!" << endl;

  map<char,int> mymap;  // in map::std::
  mymap['a']=55;        // notation as in Python, JavaScritp and some other languages
  mymap['b']=66;
  mymap['c']=77;
  map<char,int>::iterator it;

  cout << "Iterate forwards:" << endl;
  for (it = mymap.begin(); it!=mymap.end(); ++it){  // idiomatic iteration
    cout << "(" << it->first << "," << it->second << ")" << endl;	
  }
  cout << "The value behind 'c' is: " << mymap['c'] << endl; // index by key


  cout << "Iterate backwards:" << endl;
  for (it = mymap.end(), --it; ; --it){  // idiomatic iteration
    cout << "(" << it->first << "," << it->second << ")" << endl;	
    if (it==mymap.begin()){
      break;
    }
  }

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
