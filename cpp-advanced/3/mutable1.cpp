#include <iostream>
#include <stdio.h>

using namespace std;

int glob_x=444;
static int glob_y=333;

auto mylambda1 = [](int, float) {
};

#if 0
auto mylambda2 = [glob_x](int aaa, float bbb) {  // issues a WARNING on glob_x (since global/static)
  int zzz;
  return 'c'+aaa+bbb-2*glob_y;  // OK, although glob_y is not in the lambda-capture
};
#endif



int main(){
  cout << "Hello world!" << endl;
int a=0;

#if 0
auto bad_counter = [a] {   // produces compiler error, because 'a' is (const and) not mutable; by default this is in ALL lambdas !
  // Explanation: by design ALL lambdas pass variables by value (only!) -- that's why and how functional programming languages have originally been designed in order to be free of side-effects
  return a++;
};
#endif

auto good_counter = [a]() mutable {
  return ++a;
};

auto good_counter2 = [a](int, float) mutable {
  return a=a+10;  // 'a' must be in the lambda-capture, otherwise COMPILER error
//  return 5;
};

  cout << "a=" << a << endl;  // 0
  a=good_counter();
  cout << "a=" << a << endl;  // 1
  a=good_counter();
  cout << "a=" << a << endl;  // 2
  a=good_counter();
  cout << "a=" << a << endl << endl;  // 3

  int a2=0;
  cout << "a2=" << a2 << endl;  // 0
  a2=good_counter2(3,4);   // passed arguments: int float   (curried notation was not introduced into C++11; what a disgrace, not a real surprise after all!)
  cout << "a2=" << a2 << endl;  // 1
  a2=good_counter2(0,0);
  cout << "a2=" << a2 << endl;  // 1

int loc_x=444;

// there may only be bound variables in the lambda-capture !
//  and those variables may (by default) only be read (if writing to, then use 'mutable' as shown above)
auto mylambda2 = [loc_x](int aaa, float bbb) {  // no WARNING on loc_x, since in its context and local
  int zzz;
  return 'c'+aaa+bbb-2*glob_y;  // OK, although glob_y is not in the lambda-capture
};

  cout << "Bye!" << endl;
  return 0;
}
