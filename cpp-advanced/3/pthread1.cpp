// g++ pthread1.cpp -lpthread -D_REENTRANT -o pthread1

#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>  // sleep


using namespace std;

#define NUM_THREADS 4
int FILES=17;
int CORES=4;


void* thread_function(void *arg){
  int thread_id;
  thread_id = *((int*)arg);
  int is_last = 0;
  int old_width=-1;
  
  int width=FILES/CORES;         // CORES!=0
  if (FILES%CORES){
    ++width;
    if (thread_id == CORES-1){   // last thread contains the rest
      is_last = 1;
      old_width = width;
      width=FILES-(old_width*(CORES-1));
    }
  }
  int start_idx = (is_last)?thread_id*old_width:thread_id*width;
  int stop_idx=start_idx+width-1;
  // there are 'width' files to be processed in this thread #'thread_id'
  // starting with file 'start_idx' and ending at 'stop_idx' (in FILES)


  printf("Hello from thread-id#%d, width=%d, start-idx=%d, stop-idx=%d\n", thread_id, width, start_idx, stop_idx);
  sleep(10);
  pthread_exit(NULL);
}

int main(){
  cout << "In total " << FILES << " files " << endl;
  cout << "In total " << CORES << " cores " << endl << endl;
/*
organise workers:
#FILES=17 distributed among #CORES=4
   THREAD#1: 5    0..4
   THREAD#2: 5    5..9
   THREAD#3: 5    10..14
   THREAD#4: 2    15..16
       <<width>>  <<start-idx>>
*/  


  pthread_t threads[NUM_THREADS];
  int tmp;
  int args[NUM_THREADS] = {0,1,2,3};
  pthread_attr_t attr;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  pthread_attr_setscope(&attr,PTHREAD_SCOPE_SYSTEM);

  // creating threads
  for(int i=0;i<NUM_THREADS;i++){
    tmp=pthread_create(&threads[i],&attr,thread_function,(void*)&args[i]);
    if (tmp){
      cout << "Creating thread " << i << " failed!" << endl;
      return 1;
    }
  }

  // joining threads
  for(int i=0;i<NUM_THREADS;i++){
    tmp=pthread_join(threads[i],NULL);
    if (tmp){
      cout << "Joining thread" << i << " failed!" << endl;
      return 1;
    }
  }

  cout << "Bye!" << endl;
  return EXIT_SUCCESS;
}
