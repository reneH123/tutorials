#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROWS 5
#define COLS 10

#if 1
  #define CASE1
  #undef  CASE2
#else
  #define CASE2
  #undef  CASE1
#endif

#ifdef CASE1
void dumpMatrix(const int** const p)
#else
void dumpMatrix(int p[ROWS][COLS])
#endif
{
    for(int i=0;i<ROWS;i++){
      for(int j=0;j<COLS;j++){
        printf("%d ",p[i][j]);  fflush(0);
      }
      printf("\n");
    }
}

int main(void) {
#ifdef CASE1
    // CASE 1: array of pointers
    // []-operator binds stronger than *
    int *p1[ROWS];  // array of X elements with base type: int*
    //   the ROWS-array is in stack; the dynamically allocated ints will be in heap
 
    for(int i=0;i<ROWS;i++){
      p1[i]=(int*)malloc(COLS*sizeof(int));
      for(int j=0;j<COLS;j++){
        p1[i][j]=888;
      }
    }

    int row1[] = {61,62,63,64,65,66,67,68,69};
    int row2[] = {3,4,2,5,1};

    free(p1[0]); // free manually before, otherwise leaks!
    p1[0] = (int*)&row1;
    free(p1[1]); // ditto
    p1[1] = (int*)&row2;  // remaining 5 cols remain uninitialised
    //p1[ROWS+100] = NULL;  // no CRASH, but still invalid write!
    free(p1[ROWS-1]); // ditto
    p1[ROWS-1] = (int*)&row1;
row1[1]=99;  // matrix dump will show update of row1 in several matrix cells
    // iterate over p:
    dumpMatrix((const int** const)p1);  // 1st const may be, 2nd const must be
    //dumpMatrix((const int**)p1);  // OK when dumpMatrix expects const int** const p

printf("sz(p1)=%lu\n",sizeof(p1)); // returns 40 Bytes 
/*  // will have the same effect as this:
    for(int i=0;i<ROWS;i++){
      for(int j=0;j<COLS;j++){
        printf("%d ", p1[i][j]);
      }
      printf("\n");
    }*/

    for(int i=0;i<ROWS;i++){
      printf("Freeing at i=%d\n p1[i]=(%d,%p)\n",i, *p1[i],p1[i]);
      if (i!=0 && i!=1 && i!=ROWS-1)  // jump row1 and row2 assigned arrays (stack references only)
        free(p1[i]);
    }

#else
typedef int MYCOLS[COLS];
    //  CASE 2:  pointer of arrays
//    int (*p2)[COLS];  // pointer to array each of which contains 5 ints
    MYCOLS *p2;
    p2 = (MYCOLS*)malloc(ROWS*sizeof(MYCOLS)); // allocate ROWS rows

    //p2=NULL;      // valid, since p2 is a pointer!
    //p2[0] = NULL; // invalid, because rhs is not a pointer (it is an array)


    // init matrix element-wise  OK!
    for(int i=0;i<ROWS;i++){
      for(int j=0;j<COLS;j++){
        p2[i][j]=i*ROWS+j;
      }
    }

    MYCOLS col1 = {3,4,2,5,1};
    int col2[COLS] = {9,9,9,9,9};
    p2[4][4]=777;

    //for(int k=0;k<COLS;k++)    always works, safe vector copy!
    //  p2[4][k]=col1[k];
    //p2[4] = &col1;  // COMPILER error, because: array type lhs is not assignable!

    // OR: copy element-wise:
    //for(int k=0;k<COLS;k++)
    //  p2[4][k]=col1[k];

    // OR: use memcpy (not strcpy! because memcpy does not interpret Bytes, e.g. value "0" would halt copying, and only valid characters, cmp with ASCII-table!):
    // strncpy(&p2[4][0],&col1[0],COLS*sizeof(int));   // NOK!
    memcpy(&p2[4][0],&col1[0],COLS*sizeof(int)); // OK!

    (p2[4][0])+=19; // OK!  1st cell in 4th row is now 22
    int *myp = (int*)&p2[4];
    printf("myp=(%d,%p), p2[4][0]=(%d,%p)\n",*myp,myp,p2[4][0],&p2[4][0]);
    myp++;
    printf("myp=(%d,%p), p2[4][0]=(%d,%p)\n",*myp,myp,p2[4][0],&p2[4][0]);

    printf("col1[0]=(%d,%p), col1[1]=(%d,%p)\n", col1[0], &col1[0], col1[1], &col1[1]);  // width 4 Bytes, OK!
    printf("p2[4][0]=(%d,%p), p2[4][1]=(%d,%p)\n", p2[4][0], &p2[4][0], p2[4][1], &p2[4][1]);  // width also 4B, OK!


printf("sz(p2)=%lu\n", sizeof(p2));  // returns 8 Bytes (on a 64bit machine)
    dumpMatrix(p2);  // OK when dumpMatrix consumes int p[ROWS][COLS]
/*  // will have the same effect as this:
    for(int i=0;i<ROWS;i++){
      for(int j=0;j<COLS;j++){
        printf("%d ", p2[i][j]);
      }
      printf("\n");
    }
*/

    free(p2);
#endif
    return 0;
}

