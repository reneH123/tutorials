       #include <stdio.h>
       #include <stdlib.h>
       #include <unistd.h>
// is POSIX01 and C89 compatible

       void
       bye(void)
       {
           printf("That was all, folks\n");
       }

       int
       main(void)
       {
           long a;
           int i;

           a = sysconf(_SC_ATEXIT_MAX);
           printf("ATEXIT_MAX = %ld\n", a);
puts("AAA-0");
           i = atexit(bye);
puts("AAA-1");
           if (i != 0) {
puts("AAA-2");
               fprintf(stderr, "cannot set exit function\n");
puts("AAA-3");
               exit(EXIT_FAILURE);
puts("AAA-4");
           }
puts("AAA-5");
           exit(EXIT_SUCCESS);
puts("AAA-6");
       }  // returns AAA-0, 1, 5 and calls bye()


