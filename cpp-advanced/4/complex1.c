#include <stdio.h>
#include <complex.h>

int main(void){
  double _Complex c1,c2; // could specify any other precission, e.g. float
  c1=1-2i;
  c2=-3+5i;
  double _Complex c3=c1+c2;
  double _Complex c4=c1*c2;
  printf("c1+c2=%.1f + %.1fi, cabs=%.3f\n",creal(c3),cimag(c3),cabs(c3));
  printf("c1*c2=%.1f + %.1fi, cabs=%.3f\n",creal(c4),cimag(c4),cabs(c4));
  return 0;
}
