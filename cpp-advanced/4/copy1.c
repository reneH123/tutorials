#include <stdio.h>
#include <stdlib.h>

#define BUFSZ 128

int main(int argc, char *argv[]) {
  if (argc!=3){
    printf("cp SRC TARGET\n\nCopies one file into another!");
    exit(2);
  }
  FILE *f_src, *f_dst;
  if ((f_src=fopen(argv[1],"rb"))==NULL){
    printf("ERROR: could not open input file '%s'!\n",argv[1]);
    exit(1);
  }
  f_dst=fopen(argv[2],"wb");

#if 0
  char c;
  while ((c=fgetc(f_src))!=EOF)   // BUG: -1 may still be valid (binary) data
    if (fputc(c,f_dst)==EOF)      //   this problem frequently occurs in books about C!
      break;
#else
  // here is my corrected version:
  char buff[BUFSZ];  // buffer including ending '\0'
  size_t read_bytes;

  while(!feof(f_src)){
    read_bytes=fread(buff,1,BUFSZ,f_src);
    if (read_bytes<BUFSZ){
      fwrite(buff,1,read_bytes,f_dst);  // in general would be wise to check written bytes, but here we are overwriting destination, so no extra check makes really sense
      break;
    }
    fwrite(buff,1,BUFSZ,f_dst);
  }
#endif
  fclose(f_src);
  fclose(f_dst);

  return 0;
}
