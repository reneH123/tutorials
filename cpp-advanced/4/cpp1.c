#include <stdio.h>

#if 0
# 10 test1.h     // causes a preprocessor fail  (cpp)
#endif

# 10 "nonexisting.file"  // OK
# 11 "hello world aa bb ccc"

int main(void) {
  printf("Hello world!\n");
  return 0;
}
