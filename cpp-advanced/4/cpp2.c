#include <stdio.h>

#include "myhead1.h"
#include "myhead2.h"

int main(void) {
  printf("Hello world %d!\n", g(2,3));
  return 0;
}


///////////////  MYHEAD1.H:

//#ifndef MYHEAD1_H_
//#define MYHEAD1_H_
int f(){
  return 5;
}
//#endif


///////////////  MYHEAD2.H:
#ifndef MYHEAD2_H_
#define MYHEAD2_H_

#if 0
int g(int,int);
#else

#include "myhead1.h"
int g(int x,int y){
  return x*x+2*y-x+f();
}
#endif

#endif

///////////////  MYHEAD2.C:
#if 0
#include "myhead2.h"

int g(int x,int y){
  return x*x+2*y-x+f();
}

#endif
