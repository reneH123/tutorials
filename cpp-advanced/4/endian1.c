#include <stdio.h>

int main(void) {
  short x=0x0a0b;
  printf("0x%x\n", *(char*)&x);  // little endian outputs 0xb

  char arr[2]={0xa, 0xb};  // these bytes are consecutively regardless endianness
  printf("arr[0]=0x%x\n",arr[0]);  // always dumps 0xa !
  return 0;
}
