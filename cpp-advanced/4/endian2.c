#include <stdio.h>

union {
  float x;
  int i;
} fni;

int main(void) {
  fni.i=0x0a0b0c0d;
  printf("0x%x\n", *(((char*)&fni.x)+1));  // little endian outputs 0xd, ..+1 outputs 0xc, etc.

  return 0;
}
