#include <stdio.h>
#include <string.h>
#include <errno.h>

int main(void) {
  FILE *f = fopen("TEST.txt","r");
  if(f==NULL) {
      printf ("Error opening file unexist.ent: %s\n",strerror(errno));
  }
  return 0;
}


/*
 * string.h must be included, otherwise strange COMPILER warning + SEGV! (?!?BUG in "gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04) " !!?)
 */
