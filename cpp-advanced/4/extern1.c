#include <stdio.h>

extern int var1;
int var1;  // maybe anywhere inside this or other source files (GCC Translation Units)

int main(void) {
  var1++;
  return 0;
}
