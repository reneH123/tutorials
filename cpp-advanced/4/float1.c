#include <stdio.h>

int foo(int n){
  printf("n=%d,",n);  fflush(0);
  return 10*n;
}

int main(){
  {
    // upper/lower-casing
    printf("%c \n", 'A'+' ');  // returns 'a'
  }

  float x=0;
#if 0
  int z2=5/0; // compiler warning
#endif
  float res=-5/x; // no compiler warning, res is 'inf'
  // if 'float' is replaced by 'int' then a runtime exception is thrown in both C and C++
  printf("%f\n", res);

  // operator precedence:
  {
    int x=2*3%5;
    printf("x=%d\n",x);
  }
  // special chars:
  {
    char c1=' ';
    printf("Empty char: %d\n",c1);
  }

  // brackets in if are MANDATORY:
  {
    if ((5))
      printf("abc\n");
  }
 
  // printf argument evaluation ordering:
  {
    printf("%d, %d, %d\n", foo(1), foo(2), foo(3));  // last argument is evaluated 1st  => so, var-args are implemented internally as LIFOs/stacks !
  }

  // difference between %f (with rounding float) and %g (without rounding float):
  {
    printf("%7i\n",12345);    // 7 characters wide (with prefixed spaces)
    printf("%+6d\n",-12345);  // sign is always trailed
    printf("%2.4f\n",0.1234567);
  }

  // scanf 
#if 0
  {
#if 0
    int maxFishes;
    scanf("%d", maxFishes);  // SEGV, because requires a writable address, here however it is not a pointer, but a stack location
#endif
    int loc2=555;
    int *maxFishes=&loc2;  // must refer to an valid address(!), otherwise SEGV
printf("BEFORE Address of maxFishes: 0x%x\n", maxFishes);
    scanf("%d", maxFishes); 
printf("AFTER Address of maxFishes: 0x%x\n", maxFishes);  // address does not change (e.g. guaranteed no internal realloc()!)
    scanf("%d",345);  // syntactically valid, but SEGV!
  }
#endif

  printf("%d\n", 1/2%2);

  return 0;
}
