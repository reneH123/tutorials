#include <stdio.h>

//int f(void);   // if was in, then the call would be invalid!

int f(){
  printf("Hello world!\n");
}

int main(void) {
  f(1,2,3); // is totally legal! If no arguments shall be passed, declare int f(void) ...
  //  those arguments are not directly accessible inside f, but through registers according to valid ABI (!)
  return 0;
}
