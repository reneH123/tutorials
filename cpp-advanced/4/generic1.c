#include <stdio.h>

void dumpint(int i)     { printf("I%d\n",i); }
void dumpchar(char i)   { printf("C%c\n",i); }
void dumpfloat(float i) { printf("F%f\n",i); }

#define dump(x) _Generic( (x),char: dumpchar, int: dumpint, default: dumpfloat)(x)

// this example demonstrates the use of Generic Polymorphism (simulated polymorphism, however! Type checking fixes type, not during runtime)

int main(void){
  char c='y';
  dump((float)'x' );
  // <=> _Generic( ((float)'x'),char: dumpchar, int: dumpint, default: dumpfloat)((float)'x');
  return 0;
}
