#include <stdio.h>

int g();

#if 0
  int foo(int n){  // OK!
    return 5*n;
  }
#endif

int main(void) {
  int foo(int n){  // only visible in main
    return 5*n;
  }
  printf("Hello world %d!\n", foo(2));
  return 0;
}


int g(){
  return foo(5)*foo(3);  // LINKER error: undefined symbol 'foo'
}
