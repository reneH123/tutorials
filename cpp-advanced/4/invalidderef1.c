#include <stdio.h>

int main(void) {
    char buf[]= "hello world!";
    //char *p=buf;
    void *p=buf;  // just warning
    char *p2=p;
    *p2='X';   // OK! but:  *p='X' would cause COMPILER error (during type checking)
    printf("buf='%s'\n", buf);

    //printf("c='%c'\n", *p);  // COMPILER error: void* variable may NEVER be dereferenced!
    return 0;
}
