#include <stdio.h>
#include <stdlib.h>

int main(void) {
  char *p=(char*)malloc(10);


printf("p=0x%x\n",(char*)p);
    //free(p+1); p=NULL;  // immediate CRASH: p+1 is an "invalid pointer" to free(.) (not listed in Boehm's free and consumed lists!
    //free(p-1); // ditto; no partial or sub- heap releases!
    free(p); // consumes void*, but is not explicitly set to NULL on success, must be done manually if important!
printf("p=0x%x\n",(char*)p);

    return 0;
}
