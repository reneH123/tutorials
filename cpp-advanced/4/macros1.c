#include <stdio.h>

int main(void) {
  puts("Date: ""NOT "  "AVAILABLE");  // OK! this is a valid const char*

  puts("Date: "__DATE__ " in file: "__FILE__);  // .. and so is this!
  printf("DATE: "__DATE__); // ditto
  return 0;
}
