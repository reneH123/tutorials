#include <stdio.h>

int main(void) {
  puts("Date: ""NOT "  "AVAILABLE");  // OK! this is a valid const char*

  puts("Date: "__DATE__ " in file: "__FILE__);  // .. and so is this!
  printf("DATE: "__DATE__"\n"); // ditto
#if 0
#ifdef 0  // invalid!
  printf("STDC: %d\n",__STDC__);
#endif
#endif

#ifdef __STDC__
  printf("STDC is defined and has value: %d\n",__STDC__);
#endif
  return 0;
}
