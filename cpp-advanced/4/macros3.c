#include <stdio.h>

#define EXPAND1(X) /*abc*/#X  // arguments is put into quotes; both comments are not expanded!

int main(void) {
  puts(EXPAND1(AAA));
  return 0;
}
