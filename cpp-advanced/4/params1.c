#include <stdio.h>

// const int*  .. values may not change
// int * const .. pointer may not change
// const int* const .. values and pointer may not change

void print(const int* const a) {
    int i;
    for(i = 0; i < 5; i++)
        printf("%d ", a[i]);
    printf("\n");
}

void mul2(int * const arrptr) {
    int i;
    for(i = 0; i < 5; i++)
       arrptr[i] *= 2;
}

int main(void) {
    int arr[5] = { 1, 2, 3, 4, 5 };

    print(arr);
    mul2(arr);
    print(arr);
    return 0;
}
