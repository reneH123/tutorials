#include <stdio.h>

int mystrlen(const char * const s) {
  char *ptr = (char*)&s[0];
#if 0
  int counter = 0;
  while(*ptr++ != '\0') {
      counter++;
  }
  printf("DEBUG: ptr:%p, s:%p\n",ptr,s);
  return counter;
#else
  while (*ptr++);  // is cooler)
  return ptr-s-1;  // and still safe here!
#endif
}

int main(void) {
  const char* s1="abcdef";  // also works for "";
  int len=mystrlen((char*)s1);
  printf("s1='%s', len=%d\n",s1,len);
  return 0;
}
