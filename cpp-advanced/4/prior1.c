#include <stdio.h>

#if 0
int main(void) {
  int len=5;
  printf("BEFORE: len: %d\n", len);
  float f=(len++)*(len++)*(len++);
  printf("AFTER: len: %d, f:%f\n", len,f); // f== 5*6*7; len==8
  return 0;
}
#endif

#if 0
int main(void) {
  int len=5;
  printf("BEFORE: len: %d\n", len);
  float f=(len++)*(len++);
  printf("AFTER: len: %d, f:%f\n", len,f); // f== 5*6; len==7
  return 0;
}
#endif


int main(void) {
  int len=5;
  printf("BEFORE: len: %d\n", len);
  float f=(len--)*(len--) + (len++)*(len++);
  // 1st operand: 20, len==3
  // 2nd operand: (if separated:) 30, len==7
  //                 (if linked:) 12, len==5 => in total: 32, len==5
  printf("AFTER: len: %d, f:%f\n", len,f); // f==32 ; len==5
  return 0;
}
