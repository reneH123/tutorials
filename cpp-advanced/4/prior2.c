#include <stdio.h>

#if 0
int main(void) {
  int var1=100, var2=200, var;
  float v1=-1.0, v2=1.0, v;
  var = (((var1)>(var2)) ? (var1) : (var2));
  v = (((v1)>(v2))?(v1):(v2));

  printf("var: %d, v:%f\n", var,v);  // var==200, v==1.0
  return 0;
}
#endif

int main(void) {
  int var1=100, var2=200, var;
  float v1=-1.0, v2=1.0, v;
  var = var1>var2 ?var1:var2;  // nothing changes in operator precedences
  v = v1>v2?v1:v2;   // ditto, because: > higher priority than ?:

  printf("var: %d, v:%f\n", var,v);  // still var==200, v==1.0
  return 0;
}
