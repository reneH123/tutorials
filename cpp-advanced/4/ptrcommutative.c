#include <stdio.h>
#include <string.h>

// call: a.out aaa bbb =>  c1='o', c2='o'
int main(int argc, char** argv){
  char buf[] = "hello world!";
  int x=1;
  int y=argc;
  char c1=buf[4];
  char c2=(x+y)[buf];

  printf("c1='%c', c2='%c'\n",c1,c2);
  return 0;
}
