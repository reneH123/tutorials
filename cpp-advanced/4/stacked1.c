#include <stdio.h>


int main(int argc, char *argv[]) {
  printf("argc:%d\n",argc);
  int var1=11;
  if (argc==3){
    int var1=var1+3;  // it is the inner var1, (which is zeroed by default under Linux, but not necessarily under MacOS), so ..
    printf("Innervar1:%d\n",var1);  // == 3
  }
  printf("var1:%d\n",var1);
  return 0;
}
