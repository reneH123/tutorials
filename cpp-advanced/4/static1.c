#include <stdio.h>

void foo(){
  static int i=0;  // this initial value is only when program is started; re-initialisation NEVER takes place again for static variables!
  printf("BEFORE i=%d\n",i);
  i++;
  printf("AFTER i=%d\n",i);
}


int main(int argc, char *argv[]) {
  foo(); // BEFORE: i=0; AFTER: i=1
  foo(); // BEFORE: i=1; AFTER: i=2
  return 0;
}
