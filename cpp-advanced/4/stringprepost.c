#include <stdio.h>
#include <string.h>

// call: a.out aaa bbb =>  c1='o', c2='o'
int main(int argc, char** argv){
  char string[] = "ABC";
  char *p;
  char c;


  p=&string[1]; //&(string[0]);
  c=p[0];

printf("BEFORE c='%c', p=0x%x, string='%s'\n",c,p,string);

#if 0
  c=*p++;  // identical to c=*(p++);  '++' binds stronger than '*' !
// is identical to
//  c=*p;
//  p++;
#endif


//  c=(*p)++;
// is identical to:
  c=*p;
  (*p)++; // variable 'string' is also changed

printf("AFTER c='%c', p=0x%x, string='%s'\n",c,p,string);

  return 0;
}
