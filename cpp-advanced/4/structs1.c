#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 0
  struct myStruct{};  // OK! (no error, no warning)
#else
  struct myStruct{ int aaa; struct myInnerStruct{};}; // COMPILER warning because of empty
  
  //struct myInnerStruct{};  // COMPILER error: redefinition

int aaa;
#endif

struct STR {
 int field;
 char c[7];
} Structure;  // 12 Bytes
//int STR;


int main(void) {
    // inner structs may regularly be referred to (no namespace/prefix required):
    printf("sz(myInnerStruct)=%lu\n",sizeof(struct myInnerStruct)); // sz==0

    // however, fields are in logical scopes:
    aaa = 5; // the global variable is set

    // prototypical struct declarations (as with prototype function declarations):
    printf("sz(AnonymousStruct)=%lu\n", sizeof(struct {int aaa, bbb; short ccc; char ddd;}));  // 12 Bytes
    // Remark: ISO-C does not require a valid defined structure to be referred to!

    printf("sz(STR)=%lu, sz(struct STR)=%lu\n", sizeof(STR), sizeof(struct STR));
    // in -std=c99 this is strict and is safe, however,
    //  in after c11 "struct STR" may be written just as "STR", then a potential mismatch with global variable STR may happen!

    return 0;
}

