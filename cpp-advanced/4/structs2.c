#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct DATA{
  int year : 11; // 11bits to be used => fits into 2Bytes short internally (max.2048)
  int month;
short yyy;  // 2Bytes slack between padded yyy and day  (padding is architecture and platform-dependent!); in unpacked mode
  int day;
}  __attribute((packed))__;

struct PERSON{
  char name[3];
  char fname[4];
  struct DATA dob;
};  // again packing does not work!: __attribute((packed))__;

int main(void) {
  // PART I: struct and memory layout
  {
   struct DATA data = {2020,10,65535,18};  // OK  if it wasn't initialised at all then not zeroed!  If at least partially initialised, then ALL other fields ARE zeroed !
   //DATA data = {2020,10,18};             // NOK
   //struct DATA data = {{2020},10,18};    // OK, but compiler warning: superficial {}
   size_t sz = sizeof(struct DATA);
   printf("sz(struct DATA)=%lu\n", sz); 
 
   printf("hex dump of struct DATA sample:\n");
   char buff[100];
   memcpy((void*)buff,(const void*)&data,sz);
   buff[sz]='\0';
   for(int i=0;i<sz;i++){
     printf("i=%d: (%d,0x%x,%p)\n",i,buff[i],buff[i],&buff[i]);
   }
   printf("\n\n");
  }

  //  PART 2: sub-structs and initialisation
  {
   printf("hex dump of struct PERSON sample:\n");
   struct DATA dataNew={101,12,65538,36}; // 3rd argument causes overflow; COMPILER warning OR: just ignored at runtime(!)
   //struct PERSON pers = {"abc", "fgh", dataNew};     OK!
   //struct PERSON pers = {"abc", "fgh", {101,12,65538,36}};  // OK, the same result!
   struct PERSON pers = {"abc", "fgh", 101,12,65538,36};  // OK, also the same result!
   // Remark: const-initialiser does not care about structs, it is initialising element-wise by writing directly into memory (offset is internally determined using offsetof from stddef.h)!
   //pers.dob.year=39;  // OK!

   size_t sz = sizeof(struct PERSON);
   char buff[100];
   memcpy((void*)buff,(const void*)&pers,sz);
   buff[sz]='\0';
   for(int i=0;i<sz;i++){
     printf("i=%d: (%d,0x%4x,%p)\n",i,buff[i],buff[i],&buff[i]);
   }
   printf("\n\n");
  }

  // PART 3: comparison
  {
   struct PERSON bob1 = {};  // zeroes all!
   struct PERSON bob2 = {};

   size_t sz = sizeof(struct PERSON);
   char buff[100];
   memcpy((void*)buff,(const void*)&bob1,sz);
   buff[sz]='\0';
printf("BOB1:\n");
   for(int i=0;i<sz;i++){
     printf("i=%d: (%d,0x%4x,%p)\n",i,buff[i],buff[i],&buff[i]);
   }

   memcpy((void*)buff,(const void*)&bob2,sz);
   buff[sz]='\0';
printf("\nBOB2:\n");
   for(int i=0;i<sz;i++){
     printf("i=%d: (%d,0x%4x,%p)\n",i,buff[i],buff[i],&buff[i]);
   }

   printf("Bob1's address=%p, Bob2's addr=%p\n", &bob1,&bob2);
#if 0
   if (bob1==bob2){  // NOK; structs cannot directly be compared !
     puts("MATCHED!");
   } else{
     puts("NO MATCH!");
   }
#endif
   // bob1++;  // NOK, and no meaningful increment defined


   printf("\n\n");
  } 

    return 0;
}

