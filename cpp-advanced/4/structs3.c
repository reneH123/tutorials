#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct myStruct{
  int a;
  int b;
} Data1;

union myUnion{
  int a;
  int b;
} Data2 = { 0 };

int main(){
  struct myStruct s1  = { 0, 0 };
  union myUnion u1 ={0};
  s1.a++;
  u1.a++;
  printf("s1.b=%d, u1.b=%d\n", s1.b,u1.b);
  return 0;
}

