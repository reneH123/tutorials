// no compilation option needed (digraphs are supported natively by GCC)
%:include <stdio.h>

int main(int argc, char *argv<::>) {
  int a[20]; // 'problematic' characters still supported
  printf("String<:-1]='%s'\n", argv<:argc-1 :>); // replacement even inside strings
  return 0;
}
