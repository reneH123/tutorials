#include <stdio.h>

int main(void) {
#if 0
    char *ptr;
    strcpy(ptr, "you may get into trouble soon");
    puts(ptr);
#elif 0
    char *ptr;
    *ptr = 'C';  // BOOM  ptr is not initialised. Will crash here, because '*' applied to an invalid address!
    printf("%c\n",*ptr);
#endif
    //char buff[10]; // potentially dangerous
    char buff[]={1,2,3,4,5,6,7,8,9,10}; // if in stack after '10' there is no '\0' by luck, then BOOM!
    printf("'%s', strlen=%d\n",buff,strlen(buff));

    return 0;
}
