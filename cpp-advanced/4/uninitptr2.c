#include <stdio.h>
#include <stdlib.h>

#define ROWS 10
#define COLS 10

int main(void) {
    int **p=(int**)malloc(ROWS*sizeof(int*));

    int *row1=(int*)malloc(COLS*sizeof(int));
    row1[0]=55;
    row1[1]=56;
    row1[2]=57;
    row1[3]=58;

    int *row2=(int*)malloc(COLS*sizeof(int));
    row2[0]=65;
    row2[1]=66;
    row2[2]=67;
    row2[3]=68;

    int *row3=(int*)malloc(COLS*sizeof(int));
    for(int i=0;i<COLS;i++)
      row3[i]=i;

    p[0] = row1;
    p[1] = row1;    // if commented out, then SEGV at i=1, j=0 in inner loop!
    p[2] = row1;
    p[3] = row2;
    p[4] = row2;
    p[5] = row1;
    // SEGV when accessing p[6][0] (if not set) in inner loop, because not set (random noise)!
    p[6] = row3;
    p[7] = row3;
    p[8] = row3;
    p[9] = row3;

    // iterate matrix:
    for(int i=0;i<ROWS;i++){
      for (int j=0;j<COLS;j++){
        //printf("(i,j)=(%d,%d)\n",i,j);
        printf("%d ",p[i][j]); fflush(0);
      }
      printf("\n");
    }

free(row1);
free(row1);  // doubly freeing is NOT throwing a RUNTIME error! (valgrind detects)
free(row2);
free(row3);
#if 0
for (int i=0;i<ROWS;i++){
  printf("i=%d\n",i);
  free(p[i]);  // when attempting to free for i==5 row1 for the 4th time, finally a "double free or corruption" is detected and a RUNTIME error is signalled (by luck!)
}
#endif
free(p);

    return 0;
}
