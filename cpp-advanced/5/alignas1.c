#include <stdio.h>

#define ptrminus(a,b)  (unsigned)((char*)&(b)-(char*)&(a))



typedef struct { float a; float b; } s1;
typedef struct { float a; float _Alignas (double) b; } s2;

typedef struct { char a; char b; char c; } t1;
//typedef struct { char a; short _Alignas (char) b; char _Alignas (int) c; } t2; // would be compiler error, since MAY NOT reduce memory size!
typedef struct { char a; char _Alignas (s2) b; char _Alignas (int) c; } t2;


int main(void) {
{  s1 a;
  printf("floats1: %ld %d %d\n", sizeof(s1), ptrminus(a,a.a), ptrminus(a,a.b));}
{
  s2 a;
  printf("floats2: %ld %d %d\n", sizeof(s2), ptrminus(a,a.a), ptrminus(a,a.b));}
{
  t1 a;
  printf("bytes1: %ld %d %d %d\n", sizeof(t1), ptrminus(a,a.a), ptrminus(a,a.b), ptrminus(a,a.c));}
{
  t2 a;
  printf("bytes2: %ld %d %d %d\n", sizeof(t2), ptrminus(a,a.a), ptrminus(a,a.b), ptrminus(a,a.c));} // _Alignas not only may increase (but never decrease!) occupied memory, but also applies additional padding to it (as the compiler believes is best according to type provided)

  return 0;
}
