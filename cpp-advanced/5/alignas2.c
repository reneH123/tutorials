#include <stdio.h>

#define ptrminus(a,b)  (unsigned)((char*)&(b)-(char*)&(a))

struct mystruct { char s[13]; };

typedef struct { float a; float b; } s1;
typedef struct { float a; float _Alignas (double) b; } s2;

typedef struct { char a; char b; char c; } t1;
//typedef struct { char a; short _Alignas (char) b; char _Alignas (int) c; } t2; // would be compiler error, since MAY NOT reduce memory size!
typedef struct { char a; char _Alignas (s2) b; char _Alignas (int) c; } t2;

typedef struct { char a; char _Alignas (s2) b; struct mystruct m11; char _Alignas (int) c; } t3;

union myunion { int a; short b; long long c; };


int main(void) {
  printf("%ld\n", _Alignof(NULL));      // 8, because NULL==((void*)0), (pointer)!
  printf("%ld\n", _Alignof(char));      // 1 B
  printf("%ld\n", _Alignof(short));     // 2
  printf("%ld\n", _Alignof(int));       // 4
  printf("%ld\n", _Alignof(long));      // 8
  printf("%ld\n", _Alignof(long long)); // 8
  printf("base type: %ld\n", _Alignof(char[10]));  // 1Byte, because base type is considered !
  printf("unions: %ld\n", _Alignof(union myunion)); // 8 B
  // REMARK: similar to structs: the maximum alignment is taken (usually 8 Bytes on 64-bits machines for performance reasons)

  printf("s1: %ld, sz=%ld\n", _Alignof(s1),sizeof(s1));  // s1: 4, sz=8
  printf("s2: %ld, sz=%ld\n", _Alignof(s2),sizeof(s2));  // s2: 8, sz=16
  printf("t1: %ld, sz=%ld\n", _Alignof(t1),sizeof(t1));  // t1: 1, sz=3
  printf("t2: %ld, sz=%ld\n", _Alignof(t2),sizeof(t2));  // t2: 8, sz=16

  printf("sz: %ld\n", sizeof(struct mystruct));          // sz: 13Bytes
  printf("t3: %ld, sz=%ld\n", _Alignof(t3),sizeof(t3));  // t3: 8, sz=32
  // 8 Bytes-alignment: so increasing/decreasing mystruct.s will be 8-bytes aligned
  // REMARK:  _Alignof is structurally recursive over all sub-structs (a sub-struct is not assumed a fixed size, but is _Alignof of all its sub-elements!)
  return 0;
}

