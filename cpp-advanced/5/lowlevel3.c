#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#define FILE_NAME "file.tmp"

int main(void) {
  int fd=open(FILE_NAME,O_CREAT|O_WRONLY);
  if (fd>0){
    unsigned char c=0x02;
    ssize_t written_bytes = write(fd, &c, 1);
    c=0x04;
    written_bytes = write(fd, &c,1);
    int result = close(fd);
    fd=open(FILE_NAME,O_RDONLY);
    if(fd>0){
      short i;
      ssize_t read_bytes=read(fd,&i,2);
      result=close(fd);
      //printf("%d\n",i);
      char buf[10];
      sprintf(buf,"%d\n",i);
      write(STDOUT_FILENO,buf,strlen(buf)); // dumps 1026 in little endian; otherwise 516
    }
    unlink(FILE_NAME);
  }
  return 0;
}
