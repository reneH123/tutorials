#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#define FILE_NAME "file.tmp"

int main(void) {
  int fd=open(FILE_NAME,O_CREAT|O_WRONLY|O_TRUNC);
// O_TRUNC truncates the file on next open()-call; if file existed and was longer than new file, then it is truncated (non-truncation causes content from previous session is still in there!)
  if (fd>0){
    unsigned char c=0x03;
    ssize_t written_bytes = write(fd, &c, 1);
    c=0x08;
//close(fd);
for (int i=0;i<5;i++){
    //puts("AAA");
    written_bytes = write(fd, &c,1);
}
    int result = close(fd);  // if exit was before close(), then permissions would not be set at all for the written file(!), e.g. ---x--x--- instead of -r-x--x--T (owner and group do not change, however)
//exit(5);
    fd=open(FILE_NAME,O_RDONLY);
    if(fd>0){
      short i;
      ssize_t read_bytes=read(fd,&i,2);
      result=close(fd);
      //printf("%d\n",i);
      char buf[10];
      sprintf(buf,"%d\n",i);
      write(STDOUT_FILENO,buf,strlen(buf)); // dumps 1026 in little endian; otherwise 516
    }
    unlink(FILE_NAME);
  }
  return 0;
}
