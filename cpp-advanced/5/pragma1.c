#include <stdio.h>

#pragma GCC poison a

int main(){
  int b,c,d;//,a;  // COMPILER error here: a not allowed (since declared poisoned)
  c=13;
//  a=566;  // .. and COMPILER error here too
  printf("Hello world!\n");
  return 0;
}
