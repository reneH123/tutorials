#include <stdio.h>

int a; // OK!
#pragma GCC poison a  // may be used in order to localise 'problematic' situations in #define's cascades  also among different (header-)files

#if 0
#define a 124  // PRE-PROCESSOR error (since a was poisoned)
#endif
#define A 124  // OK!


int main(){

  int b,c,d;//,a;  // COMPILER error here: a not allowed (since declared poisoned)
  c=13;
  //  a=566;       // .. and obviously COMPILER error here too
  printf("Hello world!\n");
  return 0;
}
