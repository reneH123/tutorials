#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>


int main(void) {
  int fd = open("file", O_APPEND|O_WRONLY);  // O_WRONLY must be provided, otherwise nothing will be written (return code -1 instead) !
  char c1=99;
  for (int i=0;i<30;i++){
    ssize_t written_bytes = write(fd, &c1, 1);
    printf(".(%ld)",written_bytes);  fflush(0);
  }
  int ret=close(fd);  // 0 .. success
  printf("fd=%d ret=%d\n",fd, ret);
  return 0;
}

