#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>


int main(void) {
  int fd=open("file", O_CREAT|O_TRUNC|O_WRONLY);
  if (fd<=0){
    perror("open");
    return 1;
  }
  off_t off=lseek(fd,9,SEEK_SET);
  char c1=1;
  int written_bytes=write(fd,&c1,1);
  close(fd);
  printf("file offset:%ld, written bytes: %d\n",off, written_bytes);
  fd=open("file", O_RDONLY);
  if(fd<=0){
    perror("read open");
    return 1;
  }
  while(read(fd,&c1,1)>0){
    printf("%d ",c1); fflush(STDOUT_FILENO);
  }
  close(fd);  // file's content: 00 00 00 00 01
  return 0;
}

