#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(void) {
  int fd=open("newfile",O_CREAT|O_WRONLY|O_TRUNC);
  if (fd<=0){
    perror("open newfile");
    return 1;
  }
  int fd2=fcntl(fd,F_DUPFD,fd); // fd2 is a duplicate file descriptor of 'fd' (with a higher int! so, may be used e.g. inside a child-process or with threads)
  // fd2 is already open (unique resource, namely file descriptor, is handled by OS

  if (fd<=0){
    perror("fcntl");
    return 1;
  }

  for(char c1='a';c1<='z';c1++){  // .. would be the parallel execution part
    write(c1%2?fd:fd2,&c1,1);
  }

  close(fd2);
  close(fd);
  return 0;
}

