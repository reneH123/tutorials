#include <stdio.h>

#if 1
int foo(int a111, ...){  // OK!
  return a111*a111-a111;
}
#endif

#if 0
int foo(...){   // COMPILER error: at least one name MUST be provided!
  return a111*a111-a111;    // GCC needs information where varargs begin stack accomodation!
}
#endif

#if 0
int foo(...){   // COMPILER error: varargs must be distinguishable!
  return a111*a111-a111;
}
#endif

#if 0
int foo(int a111, ..., int ann){  //  COMPILER error: ellipsis must be at the end
  return a111*a111-ann;
}
#endif

#if 0
int foo(int a111, ..., int b111, ...){  // COMPILER error: because of previous issue; although could be distinguishable by preceeding different types: if types are still the same, then ambiguitiy about types; hence GCC does not allow this in general!
  return a111*a111-a111;
}
#endif

#if 1
int foo2(int a111, int a222, ...){ // OK!
  return a111*a222;
}
#endif

// Remark: ... is an internal macro (compares with what is in the macro-env of the caller-sides) !


int main(void){
  printf("%d\n", foo(111,222,333)); // OK!
  return 0;
}
