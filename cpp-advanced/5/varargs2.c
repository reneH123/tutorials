#include <stdio.h>
#include <stdarg.h>

int mysums(int n2,...){
  int sum=0;
  va_list args;
{
  int n=n2;    // COMPILER warning: if passed as 2nd argument in va_start
  va_start(args,n2);  // 2nd argument MUST always be last named in fix-part of arguments (otherwise does not know how to calculate the varargs stack windows sequence correctly!
    int arg;
    for (;n--;){
      arg=va_arg(args,int);
printf("%d: arg:%d\n",n,arg);
      sum+=arg;
    }
  va_end(args);
}

printf("\n\nSecond varargs iteration inside same function works! \n\n");
  sum=0;
{
  int n=n2;    // COMPILER warning: if passed as 2nd argument in va_start
  va_start(args,n2);  // 2nd argument MUST always be last named in fix-part of arguments (otherwise does not know how to calculate the varargs stack windows sequence correctly!
    int arg;
    for (;n--;){
      arg=va_arg(args,int);
printf("%d: arg:%d\n",n,arg);
      sum+=arg;
    }
  va_end(args);
}

  return sum;
}

int main(void){
  printf("Sum: %d\n", mysums(3,111,222,333));
  return 0;
}
