#include <stdio.h>
#include <stdarg.h>

int mysums(int n,...){
  int sum=0;
  va_list args;

  va_start(args,n);
    int arg;
    while (n--){
      sum+=va_arg(args,int);
    }
  va_end(args);
  return sum;
}

int main(void){
  printf("Sum: %d\n", mysums(3,111,222,333));
  return 0;
}
