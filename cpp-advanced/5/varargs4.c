#include <stdio.h>
#include <stdarg.h>

int max(int n, va_list l){
  int x=-1;
  for (int i=0;i<n;i++){
    int arg=va_arg(l,int);
    printf("arg%d=%d\n",i,arg);
    x=arg>x?arg:x;
  }
  return x;
}

int va_max(int n, ...)
{
  va_list args;
  va_start(args,n);
  int res=max(n,args); // varargs list from the start
  va_end(args);
  return res;
}

int main(void){
  printf("Max: %d\n", va_max(3,32,31,33));
  return 0;
}
