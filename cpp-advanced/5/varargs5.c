#include <stdio.h>
#include <stdarg.h>

#define mydef(...) printf(__VA_ARGS__)  // only available if in head ...

int main(void){
  mydef("hello world %s %d!\n", "abc",123);
  return 0;
}
