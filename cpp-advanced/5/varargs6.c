#include <stdio.h>
#include <stdarg.h>

#define mydef(...) puts(#__VA_ARGS__)

int main(void){
  mydef("hello world %s %d!\n", "abc",123); // issues: "hello world %s %d!\n", "abc",123
  return 0;
}
