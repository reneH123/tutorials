#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

void my_asprintf(char **sp, char *fmt, ...){
  char buff[100];  // will be freed on function return
  va_list ap;
  // step 1) determine required bytes
  va_start(ap,fmt);
  size_t sz = vsprintf(buff, fmt, ap);
  va_end(ap);
printf("my_asprintf:sz:%lu\n",sz);
  // step 2) create dynamic sized string:
  char *s=malloc(sz*sizeof(char));
  va_start(ap,fmt);
  vsprintf(s, fmt,ap);
  va_end(ap);
  // alternative step 2)
  // strcpy(s,buff);
  // step 3) redirect pointer to char*
  *sp=s;
}

int main(){
  char *ptr=NULL; // points to NULL, but address is not NULL (stacked)
  my_asprintf(&ptr,"%d %s %d %s",5, "hello", 3, " world!", NULL);

  printf("main: &ptr=%p\n",&ptr);
  printf("main: ptr='%s'\n",ptr);
}
