#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

void my_asprintf(char **sp, char *fmt, ...){
  char buff[100];  // will be freed on function return
  va_list ap;
  // step 1) determine required bytes
  va_start(ap,fmt);
  size_t sz = vsprintf(buff, fmt, ap);
  va_end(ap);
printf("my_asprintf:sz:%lu\n",sz);
  // step 2) create dynamic sized string:
  char *s=malloc(sz*sizeof(char));
  va_start(ap,fmt);
  vsprintf(s, fmt,ap);
  va_end(ap);
  // alternative step 2)
  // strcpy(s,buff);
  // step 3) redirect pointer to char*
  *sp=s;
}

// there is a small loop-hole in the C-spec:
//   If passed buff is NULL and size-delimiter is zero, then vsnprintf will not write to the buffer (causing a certain SEGV!), but return only the number of bytes that would have been written (sounds sick, but true!!)
void my_asprintf2(char **sp, char *fmt, ...){
  va_list ap;
  va_start(ap,fmt);
  size_t sz = vsnprintf(NULL, 0, fmt, ap); // obtains total size (without actually writing to buffer)
  va_end(ap);

printf("my_asprintf:sz:%lu\n",sz);

  // step 2) create dynamic sized string:
  //  char *s=malloc((sz+1)*sizeof(char));   // 1 extra byte for '\0'
  // OR use calloc:
  char *s=calloc(sz+1,sizeof(char));

  va_start(ap,fmt);
  vsprintf(s, fmt,ap);
  va_end(ap);
  //s[sz]='\0';   // required when malloc is used!;  BUT: calloc does not insist (since already initialised)

  *sp=s;
}


int main(){
  char *ptr=NULL; // points to NULL, but address is not NULL (stacked)
  my_asprintf2(&ptr,"%d %s %d %s",5, "hello", 3, " world!", NULL);

  printf("main: &ptr=%p\n",&ptr);
  printf("main: ptr='%s'\n",ptr);
}
