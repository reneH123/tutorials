#include <stdlib.h>
#include <stdio.h>

#define MAX_ELEMENTS 5

void print_arr(double *a){
  for(int i=0;i<MAX_ELEMENTS;i++){
    printf("%f ",a[i]);
  }
  printf("\n");
}

int my_cmp1(const void* l, const void* r){
  double d1=*((double*)l);
  double d2=*((double*)r);
//  printf("cmp %f with %f, diff=%f\n",d1,d2,d1-d2);
  return (d1>d2)?1:((d2-d1<0.1)?0:-1); // assume 0.1 is some valid epsilon here, since floating point numbers are very fragile and precision problems always occur all the time!
}


int main(){
  double arr[MAX_ELEMENTS] = {3.1, 2.7, 4.1, 2.9, 5.3};
  printf("\nBEFORE qsort:\n");
  print_arr(arr);
  qsort(arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\n\nAFTER qsort:\n");
  print_arr(arr);  // 2.7 2.9 3.1 4.1 5.3
  printf("\narr at %p (1st element is %f)\n", arr, arr[0]);

  double key=2.9;
  double *found;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr); // +1

  key=2.7;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr); // 0

  key=4.1;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr); // +3

  key=5.3;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr); // +4

  // non-existing key => returns null
  key=123.456;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr); // (nil)

  // use unsorted array with existing key
  arr[0]=2.7;
  arr[1]=3.1;
  arr[2]=2.9;
  arr[3]=4.1;
  print_arr(arr);  // 2.7 3.1 2.9 4.1 5.3

  key=2.9;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr);
  // => lucky found: it could by pure luck find 2.9 (probably because it is in the mid)

  key=3.1;
  found=bsearch(&key, arr, sizeof(arr)/sizeof(arr[0]), sizeof(arr[0]), my_cmp1);
  printf("\nfound key=%f at %p (%ld elements further)\n", key, found, found-arr); // (nil)
  // => not so lucky, missed, although it's there!

  return 0;
}
