#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CHILDREN  3
#define SECS      10

int main(int argc, char *argv[]){
  for(int c=0;c<CHILDREN;c++){
    int pid=fork();
    if (!pid){
      srand(getpid());
      int secs=rand()%SECS+1;
      char buf[10];
      sprintf(buf,"%d",secs);
      execlp("/bin/ls", "/bin/ls", buf, NULL);  // ls is in $PATH
      // NULL is implicitly required as last argument by execlp
      // "ls" is needed twice, the latter is used as argv[0] by ls
      // what comes next is only executed (and exists!) if execlp fails (-1), otherwise full control is given to the "ls" process!

      fprintf(stderr, "Fork failed: %s\n", strerror(errno)); // unreachable if execlp does not fail (returning -1) !
      return 1;
    }
  }
  for (int c=0;c<CHILDREN;c++){
    int status;
    int child=wait(&status);
    printf("Parent: child #%d exited: pid=%d, code:%d\n", c,child, WEXITSTATUS(status));
  }
  return 0;
}
