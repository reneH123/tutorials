#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[], char *env[]){
  int ret=fork();

  if (!ret){
    printf("Kid says hi!\n");
    return 0;
  } else if (ret>0){
    printf("Parent says hi!\n");
    return 0;
  } else{
    printf("fork fails, error: '%s'!\n", strerror(errno));
  }
  return 0;
}
