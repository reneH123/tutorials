#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[], char *env[]){
  char buff[]="aaabb";
  int ret=fork();

  if (!ret){
    printf("kid buff before: %s\n",buff);
    printf("Kid says hi!\n");
    strcpy(buff+1,buff+3);
sleep(2);
    printf("kid buff after: %s\n",buff);
    return 0;
  } else if (ret>0){
    printf("parent buff before: %s\n",buff);
    printf("Parent says hi!\n");
    strcpy(buff,"ccdde");
sleep(1);
    printf("parent buff after: %s\n",buff);
    return 0;
  } else{
    printf("fork fails, error: '%s'!\n", strerror(errno));
  }
  return 0;
}
