#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CHILDREN  3
#define SECS      10

int main(int argc, char *argv[]){
  for(int kid=0;kid<CHILDREN;kid++){
    int pid=fork();
    if (!pid){
      srand(getpid());
      int secs=rand()%SECS+1;
      printf("Child: PID=%d sleep time: %d [s]\n",getpid(), secs);
      sleep(secs);
      printf("Child: PID=%d woke up and quitting\n",getpid());
      return 11;
    }
    else if (pid==-1){
      printf("Fork failure: %s!\n", strerror(errno));
      return 1;
    }
  }

  printf("Parent process says hi!\n");
  for(int kid=0;kid<CHILDREN;kid++){
    int status;
    int child=wait(&status);
    printf("Parent: child with PID %d exited with %d\n", child, WEXITSTATUS(status));
  }
  return 0;
}

/*

Parent process says hi!
Child: PID=7102 sleep time: 7 [s]
Child: PID=7103 sleep time: 4 [s]
Child: PID=7104 sleep time: 2 [s]
Child: PID=7104 woke up and quitting
Parent: child with PID 7104 exited with 11
Child: PID=7103 woke up and quitting
Parent: child with PID 7103 exited with 11
Child: PID=7102 woke up and quitting
Parent: child with PID 7102 exited with 11

*/
