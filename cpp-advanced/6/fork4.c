#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CHILDREN  3
#define SECS      10

int main(int argc, char *argv[]){
  int pids[CHILDREN];
  for(int kid=0;kid<CHILDREN;kid++){
    int pid=fork();
    if (!pid){
      srand(getpid());
      int secs=rand()%SECS+1;
      printf("Child: PID=%d sleep time: %d [s]\n",getpid(), secs);
      sleep(secs);
      printf("Child: PID=%d woke up and quitting\n",getpid());  // each child-process can be ready any time really, ...
      return 11;
    }
    else if (pid==-1){
      printf("Fork failure: %s!\n", strerror(errno));
      return 1;
    }
    pids[kid]=pid;
  }

  printf("Parent process says hi!\n");
  for(int kid=0;kid<CHILDREN;kid++){
    int status;
    int child=waitpid(pids[kid],&status,0);  // ..., but they will be joined only in exactly the same order (by the parent) as they were forked!
    printf("Parent: child #%d with PID %d exited with %d\n", kid, child, WEXITSTATUS(status));
  }
  return 0;
}

/*
Parent process says hi!
Child: PID=7220 sleep time: 6 [s]
Child: PID=7221 sleep time: 8 [s]
Child: PID=7222 sleep time: 3 [s]
Child: PID=7222 woke up and quitting
Child: PID=7220 woke up and quitting
Parent: child #0 with PID 7220 exited with 11
Child: PID=7221 woke up and quitting
Parent: child #1 with PID 7221 exited with 11
Parent: child #2 with PID 7222 exited with 11
*/
