#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *env[]){
  // global environment variables are stored in env
  // argc only limits argv
  // env closes by convention with '\0', and has no apriori counter

  for(int i=0;i<argc;i++){
    printf("arg %d: '%s'\n",i,argv[i]);
  }
  int i=0;
  while (*env){
    printf("env %i: '%s'\n",i,*env);
    i++;
    env++;
  }

  char *path=getenv("PATH");
  printf("Env var $PATH='%s'\n",path);
  return 0;
}
