#include <stdio.h>
#include <stdlib.h>

void foo(char **p){
  char* s=malloc(100);
  *p=s;
  printf("foo:p=%p\n",p);
}

int main(){
  char *p1=NULL;
  foo(&p1);
  printf("p1=%p\n",p1);
  free(p1);
  return 0;
}
