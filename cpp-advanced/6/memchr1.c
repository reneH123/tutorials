#include <stdio.h>
#include <string.h>

int main(){
  char *s1="hello world!";
  char *found=memchr(s1,'o',strlen(s1));       // returns "o world!"
  //char *found=memchr(s1+5,'o',strlen(s1));     // returns "orld!"
  //char *found=memchr(s1+5,'z',strlen(s1));   // returns NULL

  printf("s1 starts at %p, found at %p\n, diff is %d\n", s1,found, found-s1); // difference is 4 bytes
}
