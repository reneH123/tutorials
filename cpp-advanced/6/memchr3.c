#include <stdio.h>
#include <string.h>

int main(){
  char *s1="hello world, oy!";
  char *p=strchr(s1,'o');
  while(p){
    printf("<%lu>\n",p-s1);
    p=strchr(p+1,'o');
  } // outputs: 4 7 13
  return 0;
}
