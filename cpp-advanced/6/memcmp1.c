#include <stdio.h>
#include <string.h>

int main(){
  char t1[10], t2[10], t3[10];
  memset(t1,'a',sizeof(t1));
  memset(t2,'a',sizeof(t1));
  memset(t3,'a',sizeof(t1));
  t3[7]='z';

  printf("%d\n", memcmp(t1,t2,sizeof(char)*10)); // 0
  printf("%d\n", memcmp(t2,t3,sizeof(char)*10)); // -25
  printf("%d\n", memcmp(t3,t2,sizeof(char)*10)); // +25

  // not reliable as univeral comparison: e.g. DO NOT apply byte-wise comparison to floats, double, structs (may have dbl, or filling gaps, etc. etc.) !
}
