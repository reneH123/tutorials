#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

typedef struct{
 short a1;
 char c2;
} str_t;

int main(){
  str_t t1[4], t2[4];
  for (int i=0;i<4;i++){
    t1[i].a1=i;
    t1[i].c2='a'+i;
  }

  memcpy(t2+2,t1, 2*sizeof(str_t));
  memcpy(t2,t1+2, 2*sizeof(str_t));

  for (int i=0;i<4;i++){
    printf("%d %c\n", t2[i].a1, t2[i].c2);
  }

  return 0;
}
