#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

void print_arr(const char * const s, int n){
  for(int i=0;i<n;i++){
    printf("%c ",s[i]);
  }
  printf("\n");
}


int main(){
  char a[4]={'a','b','c','d'}, b[4], c[4];
  
  memcpy(b,a,4);  
  memcpy(c,b,4);    // a=abcd   b=abcd   c=abcd

  memcpy(b,b+1,3);  // a=abcd   b=bcdd   c=abcd
  memcpy(c+1,c,3);  // a=abcd   b=bcdd   c=aabc <- SHOULD BE, but is not for older GCC (known "bug"!)!
  // It is actually:   c=aaac; On gcc 7.5.0 it is aabbc (OK!)
  //   for older GCC versions one should use memmove() instead!
  //   For newer GCC versions memmove and memcpy do the same (again) !
 
  print_arr(b,4);
  print_arr(c,4);
  return 0;
}
