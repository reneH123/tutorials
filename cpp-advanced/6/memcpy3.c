#include <stdio.h>
#include <string.h>

void print_arr(const char * const s, int n){
  for(int i=0;i<n;i++){
    printf("%c ",s[i]);
  }
  printf("\n");
}


int main(){
  char a1[]={'a','b','c','d'};
  char a2[]={'u','v','w','x'};
  char *p=memccpy(a2,a1,'c',4);  // a2 is abcx
  print_arr(a2,4);
  printf("*p=%c\n",*p);  // *p=='x'
  return 0;
}
