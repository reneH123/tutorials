#include <stdio.h>
#include <string.h>

// this example demonstrates the (dodgy and and often very unexpected) behaviour of memccpy !

int main(){

//OK!
#if 1
  short int a1[]={1,2,3};
  short int a2[]={0,8,7};
  //char *p=memccpy(a2,a1,0,sizeof(a1));    // 1 8 7, *p=8
printf("a2 at %p\n",&a2);
  short int *p=memccpy(a2,a1,3,sizeof(a1));    // 1 2 7, *p=0 (but as 2nd byte of a1[3] because little-endian!)
#endif

//OK!
#if 0
  short int a1[]={1,2,3};
  short int a2[]={8,0,7};
  short int *p=memccpy(a2,a1,0,sizeof(a1));    // 1 0 7, *p=0 (2nd short in a2)
printf("a2 at %p\n",&a2);
#endif

//OK!
#if 0
  short int a1[]={1111,2,3};               // 1st occuring 0 (0 as 1 byte) occurs in a1[2], the 2nd byte, due to little endian!
  short int a2[]={0,0,'a'};
printf("sizeof(a1)=%lu\n",sizeof(a1));
printf("a2 at %p\n",&a2);

  char *p=memccpy(a2,a1,0,sizeof(a1));    // 1111 2 97, *p=0 (second '0' in a2)
#endif

//OK!
#if 0
  char a1[]={'a','b','c','d'};
  char a2[]={'u','v','w','x'};
  char *p=memccpy(a2,a1,'c',sizeof(a1));  // abcx, *p='x'
#endif

printf("%d %d %d\n",a2[0],a2[1],a2[2]);
//  printf("%c %c %c %c\n",a2[0],a2[1],a2[2],a2[3]); 

  printf("*p='%d' at %p  \n", *p,p);  // *p=='x'
//  printf("p-2: %d\n", *(p-2));  // will be 111 (pointer p-2 is of char*, but a2 is short int -> hence, effectively pointering 1 element before)
  return 0;
}
