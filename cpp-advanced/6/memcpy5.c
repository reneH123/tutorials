#include <stdio.h>
#include <string.h>

int main(){
  short int t[4];
  memset(t,1,sizeof(t)); // in total 8 Bytes to be set
  printf("%d %d %d %d\n",t[0],t[1],t[2],t[3]);  // 257 257 257 257  (since 1st Byte is 256 and 2nd Byte is 1, in little-endian the short value will be 257)
  return 0;
}
