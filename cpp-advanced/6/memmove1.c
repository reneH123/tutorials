#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void) {
  printf("memmove-1\n");

  char *p1="hello world!\n";
  char* p2=malloc(100);
  size_t sz = strlen(p1);
  //memcpy(p2,p1+3,5);
  memset(p2,'a',5);
  memset(p2+5,'b',5);
  memset(p2+10,'c',5);
  p2[15]='\0';
  printf("BEFORE: p2:'%s'\n",p2); 
memmove(p2+3,p2+8,4);   // aaaaabbbb bccc cc  -> aaa bbcc bbbccccc
//memcpy(p2+3,p2+8,4); // does exactly the same! (even assembly code is identical!)
  printf("AFTER: p2:'%s'\n",p2); 

  return 0;
}
