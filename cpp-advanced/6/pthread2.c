//gcc -pthread

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define THREADS 50

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int sum=0;  // may intentionally be 'critical' wrt thread-safety
int cnt=0;  // ditto, so using mutexes...

void *thread(void *data){
  int *p=(int*)data;
  sleep(2);
  if(*p%2){
    pthread_mutex_lock(&mutex);  // entry here
    int s,c;
    s=sum;
    c=cnt;
    c++;
    s+=*p;
    sum=s;
    cnt=c;
    pthread_mutex_unlock(&mutex); // release here
  }
  return NULL;
}

int main(int argc, char *argv[]){
  pthread_t threads[THREADS];
  int data[THREADS];
  for(int i=0;i<THREADS;i++)
    data[i]=i+1;
  for(int i=0;i<THREADS;i++)
    pthread_create(threads+i,NULL,thread, data+i);
  for(int i=0;i<THREADS;i++)
    pthread_join(threads[i],NULL);
  printf("Results: cnt=%d, sum=%d\n", cnt,sum);
  return 0;
}

