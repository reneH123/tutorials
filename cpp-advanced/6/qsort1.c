#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXNAMELEN 12

typedef struct {
  char name[MAXNAMELEN];
  int age;
} t_student;

// compares two students by its age
int my_cmp1(const void* l, const void* r){
  return ((t_student*)l)->age - ((t_student*)r)->age;
}

int my_cmp2(const void* l, const void* r){
  return strncmp(((t_student*)l)->name, ((t_student*)r)->name, MAXNAMELEN);
}


void print_students(t_student *arr, int n){
  for(int i=0;i<n;i++){
    printf("Student %d: name='%s', age=%d\n",i,arr[i].name,arr[i].age);
  }
}

int main(int argc, char *argv[])
{
  t_student students[] = { {"berry", 22}, {"mary", 20}, {"larry", 23}, {"harry", 21}, {"gerry",20}, {"jerry", 21}, {"terry",20}, {"perry",22}, {"cherry",19},{"gary",22}};
  size_t n = sizeof(students)/sizeof(students[0]);

  printf("BEFORE:\n");
  print_students(students, n);

  qsort(students, n,sizeof(students[0]),my_cmp1);

  printf("\n\nAFTER sorting by age:\n");
  print_students(students, n);

  qsort(students, n,sizeof(students[0]),my_cmp2);

  printf("\n\nAFTER sorting by name:\n");
  print_students(students, n);

  return EXIT_SUCCESS;
}
