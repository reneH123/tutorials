#include <stdio.h>


void bar(){
  int a;
  printf("addr(bar::a)=%p\n",&a);
}

void foo(){
  int b;
  bar();  // must be before printf, otherwise GCC may optimise it away and suddenly 'bar::a' may have the exact same address as 'foo:b'
  printf("addr(foo:b)=%p\n",&b);
}

void g(int a, int b){
  printf("addr(a)=%p, addr(b)=%p\n",&a,&b);

}

int main(int argc, char *argv[]) {
{  /*
    Indication No.1)

     Test Sample:
      addr(a)=0x7ffdcda5e0bc
      addr(b)=0x7ffdcda5e0b8

    Since by default in GCC (Linux) we have __cdecl__:
       'a' must be in bottom of stack frame, and 'b' on top
      Since &b < &a in absolute numbers, stack grows to 0 (towards .data) !
  */

  g(4,5);
}

{
  /*
    Indication No. 2)
    Test Sample:
addr(foo:b) =0x7fffd3799284    invoker ("outer")
addr(bar::a)=0x7fffd3799264    invokee ("inner")   top
    So, since foo:b > bar::a   stack grows to 0 (up; towards .data) !

  */
  foo();
}
    
  return 0;
}
