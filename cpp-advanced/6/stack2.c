#include <stdio.h>
#include <stdlib.h>


void bar(){
  int a;
  printf("addr(bar::a)=%p\n",&a);
  void *p=malloc(100);
  printf("heap p      =%p\n",p);
  free(p);
/*  Test sample:
addr(bar::a)=0x7fff641a24cc
heap p      =0x55efaf619670

   So, heap has always a smaller address than stack, hence heap lies above stack!
*/
}

int main(int argc, char *argv[]) {
{
  bar();  // calling one instance, just to be sure we are actively on the stack
}
    
  return 0;
}
