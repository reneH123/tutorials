#include <stdio.h>
#include <string.h>

int main(){
  char *p1="hello world!";
  char *p2= strdup(p1);
  printf("addr(p1)=%p, addr(p2)=%p\n",p1,p2);
  printf("p1='%s', p2='%s', strcmp=%d (0..equals)\n",p1,p2,strcmp(p1,p2));

  printf("strcmp(\"hallo\",\"hello\")=%d  (<0 .. less than)\n",strcmp("hallo","hello"));
  printf("strcmp(\"hello\",\"heLlo\")=%d  (>0 .. greater than)\n",strcmp("hello","heLlo"));

  printf("a=%d, e=%d, L=%d, l=%d, <space>=%d\n",'a','e','L','l', ' ');

  printf("a-space=%d\n",'A'+' ');   // 'a' == 'A'+' '
  return 0;
}
