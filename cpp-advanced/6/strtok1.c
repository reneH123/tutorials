#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// actual call ./a.out 'a/bbb///cc;xxx:yyy:' ':;/'
// legacy call ./a.out 'a/bbb///cc;xxx:yyy:' ':;'  '/'
// origin: manpage of 'strtok'


// demonstrates lexing (tokenization) using separators

int main(int argc, char *argv[])
{
  if (argc != 3) {
    fprintf(stderr, "Usage: %s string delim\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int j=1;
  //char *input_string = "hello;world!";  // will cause a SEG V when calling 'strtok', because string is in RO-segment (not writable) !
  char *input_string = argv[1]; // e.g. "a/bbb///cc;xxx:yyy:"   ATTENTION: the content of input_string is really modified after strtok!!
  char *delimiters   = argv[2]; // e.g. ":;/"    if none of the separators occurs in input_string, then 'token' == NULL after 1st iteration, halt!
 
char *input_back_ptr=input_string;
  char *token;
  char *_sptr1;
  printf("Init: input='%s' at %p\n", input_string, input_string);
  
  do{
    //token = strtok_r(input_string, delimiters, &_sptr1);
    //printf("#%d: recognised token: %s, input: '%s', delimiters='%s', _sptr1='%s'\n", j, token, input_string,delimiters,_sptr1);

    token = strtok(input_string, delimiters); // input_string is required NULL after 1st strtok-call AND: delimiters may not be NULL (all the time)
    printf("#%d: recognised token: '%s', input: '%s' at %p, delimiters='%s'\n", j++, token, input_string,input_string,delimiters);
    input_string = NULL;  // must be set NULL (otherwise non-termination!)
  }while (token);

  printf("Final: input='%s' at %p\n", input_string,input_string);
  printf("input_back_ptr: '%s' at %p\n", input_back_ptr,input_back_ptr);
/*
gcc test1.c && ./a.out 'aa/bbb///cc;xxx:yyy:' ':;/'
Beginning: input='aa/bbb///cc;xxx:yyy:'
#1: recognised token: 'aa', input: 'aa', delimiters=':;/', _sptr1='bbb///cc;xxx:yyy:'
#2: recognised token: 'bbb', input: '(null)', delimiters=':;/', _sptr1='//cc;xxx:yyy:'
#3: recognised token: 'cc', input: '(null)', delimiters=':;/', _sptr1='xxx:yyy:'
#4: recognised token: 'xxx', input: '(null)', delimiters=':;/', _sptr1='yyy:'
#5: recognised token: 'yyy', input: '(null)', delimiters=':;/', _sptr1=''
#6: recognised token: '(null)', input: '(null)', delimiters=':;/', _sptr1=''
*/

  return EXIT_SUCCESS;
}
