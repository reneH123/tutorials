#include <wchar.h>
#include <stdio.h>

int main(){
  wchar_t c1=U'ж';  // = U'ж'  == u'ж'
  printf("c1='%c' encoded in %lu byte(s)\n",c1,sizeof(c1)); // '6'  (and this is 4 bytes in size)
  return 0;
}
