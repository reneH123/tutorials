#include <wchar.h>
#include <stdio.h>
#include <locale.h>
#include <wctype.h>

void is(wchar_t c){
  printf("%d %d %d\n", !!iswlower(c),!!iswupper(c), !!iswalnum(c));
}

int main(){
  setlocale(LC_ALL, "");
  is(L'Ж');  // 0 1 1
  return 0;
}
