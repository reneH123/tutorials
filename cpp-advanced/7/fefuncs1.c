#include <stdio.h>
#include <math.h>
#include <fenv.h>
#include <float.h>

// gcc -lm
#define CHECK_EXC(X)  (fetestexcept(X)?1:0)

#pragma STDC FENV_ACCESS ON   // if dropped or OFF, then compiler will use its default settings, whatever those might be!  (as always: NO Compiler errors/warnings) !

void dump_except_vec(){
  printf("(%d,%d,%d,%d,%d)\n", CHECK_EXC(FE_DIVBYZERO), CHECK_EXC(FE_INEXACT), CHECK_EXC(FE_INVALID), CHECK_EXC(FE_OVERFLOW), CHECK_EXC(FE_UNDERFLOW));
}
  
int main(void){
{
  int fe_all = FE_ALL_EXCEPT;
  int fe_all2 = FE_DIVBYZERO|FE_INEXACT|FE_INVALID|FE_OVERFLOW|FE_UNDERFLOW;  // == FE_ALL_EXCEPT
  printf("fe_all=%d, fe_all2=%d\n",fe_all,fe_all2);
  printf("fetestexcept(fe_all)=%d, fetestexcept(fe_all2)=%d\n\n\n", fetestexcept(fe_all), fetestexcept(fe_all2));  // 0,0, so, yes, at startup all exc-bits are 0!
}

{
  printf("Exception vectors have the form:\n\t (FE_DIVBYZERO, FE_INEXACT,  FE_INVALID, FE_OVERFLOW, FE_UNDERFLOW)\n\n");
  printf("Initial:\t"); dump_except_vec();

  float x=1.f, y=0.f, z1=x/y;  // raises FE_DIVBYZERO
  printf("1.0f/0.f:\t"); dump_except_vec();
  
  float z2=sqrt(-x);  // raises FE_INVALID, z2==NAN
  printf("sqrt(-1):\t"); dump_except_vec();
  
  float z3=x/1234.5678f;  // raises FE_INEXACT
  printf("1.f/1234.5678f:\t"); dump_except_vec();

  float z4=x/FLT_MAX; z4/=FLT_MAX;  // raises FE_UNDERFLOW
  printf("1.f/FLT_MAX:\t"); dump_except_vec();  // (1,1,1,0,1)


  fexcept_t flags; // initially all internal flags are 0
  fegetexceptflag(&flags, FE_ALL_EXCEPT);  // flags == (1,1,1,0,1)
  // Remark: must be called 1st at the very beginning prior to calling any fe-except function, otherwise will not work reliably with fesetexceptflag (flaky feature!!?) !
  //  However, feclearexcept and feraiseexcept will always reliably change the internal fe-state, even if  fegetexceptflag/fesetexceptflag are not properly!
  // <-- FE_ALL_EXCEPT states, that ALL exceptions will be settable (see later!)
  printf("Record flags now with:\t"); dump_except_vec();

  float z5=x/FLT_MIN; z5/=FLT_MIN;  // raises FE_OVERFLOW
  printf("~0.f/FLT_MIN:\t"); dump_except_vec(); // (1,1,1,1,1)


  feclearexcept(FE_ALL_EXCEPT);
  printf("AFTER clearexcept:\t");  dump_except_vec();

  feraiseexcept(FE_OVERFLOW);
  printf("AFTER raiseexcept-1:\t");   dump_except_vec();
  feraiseexcept(FE_UNDERFLOW);
  printf("AFTER raiseexcept-2:\t");   dump_except_vec();

  fesetexceptflag(&flags, FE_DIVBYZERO|FE_INEXACT);       // sets the flags specified according to the state (== 2nd argument ::int)
  //   flags as previously set acts as a permission mask allowing only the bits specified in 2nd argument in  fegetexceptflag(,) ! (this is an additional precaution, for setting only!)
  printf("AFTER setexceptflag:\t"); dump_except_vec();    //  Call to fegetexceptflag(&flags, FE_ALL_EXCEPT); is mandatory at the very beginning before changing any exception-setting !!

  // if above was  fegetexceptflag(&flags, FE_INEXACT); instead, then emits (0,1,0,1,1) instead of (1,1,0,1,1)

  feclearexcept(FE_ALL_EXCEPT); dump_except_vec();        // emits (0,0,0,0,0)
}
 
  return 0;
}
