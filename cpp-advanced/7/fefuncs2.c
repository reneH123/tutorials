#include <fenv.h>
#include <math.h>
#include <errno.h>
#include <stdio.h>

#pragma STDC FENV_ACCESS ON

#define flag(x) printf(#x"=%s\n", x&math_errhandling?"ON":"OFF")

int main(void){
  flag(MATH_ERRNO);
  flag(MATH_ERREXCEPT);
  errno=0;
  feclearexcept(FE_ALL_EXCEPT);
  float x=-1.f;
  printf("%f\n", sqrt(x));
  printf("math_errhandling: %d\n",math_errhandling); // e.g. 3
  if (math_errhandling&MATH_ERRNO){
    printf("errno %s EDOM\n", errno==EDOM?"==":"!=");
    printf("errno %s ERANGE\n", errno==ERANGE?"==":"!=");
  }
  if (math_errhandling&MATH_ERREXCEPT)
    printf("FE_INVALID = %s\n", fetestexcept(FE_INVALID)? "ON":"OFF");
  return 0;
}

/*
MATH_ERRNO=ON
MATH_ERREXCEPT=ON
-nan
errno == EDOM
FE_INVALID = ON
*/
