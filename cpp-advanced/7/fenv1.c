#include <fenv.h>
#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <float.h>

#define _show_exc(x)  printf(#x"=%s;", fetestexcept(x)?"ON":"OFF")

#pragma STDC FENV_ACCESS ON

void show_exc(void){
  _show_exc(FE_DIVBYZERO);
  _show_exc(FE_INEXACT);
  _show_exc(FE_INVALID);
  _show_exc(FE_OVERFLOW);
  _show_exc(FE_UNDERFLOW);
  puts("");
}

void show_round(void){
  int r=fegetround();
  char *rs;
  switch(r){
    case FE_TONEAREST: rs="FE_TONEAREST"; break;
    case FE_UPWARD: rs="FE_UPWARD"; break;
    case FE_DOWNWARD: rs="FE_DOWNWARD"; break;
    case FE_TOWARDZERO: rs="FE_TOWARDZERO"; break;
    default: rs = "unknown";
  }
  printf("Rounding=%s\n\n",rs);
}

int main(void){
  printf("1. Initialise:\n");
  show_exc(); show_round();  // = the whole fe-env

  printf("2. change rounding ; getenv:\n");
  fenv_t env;
  
  fesetround(FE_TOWARDZERO);
  float x=1.f, y=0.f;
  printf("%f\n", x/y);
  printf("%f\n",sqrtf(-x));
  fegetenv(&env);        // stores the current fe-env
  show_exc(); show_round();

  printf("3. update env (rounding only!) ; change rounding:\n");
  feupdateenv(&env);     // restores previous rounding (but not raised exceptions!)

  fesetround(FE_DOWNWARD);  // will be ignored
  printf("%f\n", (1.f/FLT_MIN)/FLT_MIN);  // causes OVERFLOW, will also be ignored
  show_exc(); show_round();

  printf("4. restore env (initialises both: rounding and exceptions):\n");
  fesetenv(&env);  // always fully restores as it was at fegetenv()!
  show_exc(); show_round();

  return 0;
}

/*
1. Initialise:
FE_DIVBYZERO=OFF;FE_INEXACT=OFF;FE_INVALID=OFF;FE_OVERFLOW=OFF;FE_UNDERFLOW=OFF;
Rounding=FE_TONEAREST

2. change rounding ; getenv:
inf
-nan
FE_DIVBYZERO=ON;FE_INEXACT=OFF;FE_INVALID=ON;FE_OVERFLOW=OFF;FE_UNDERFLOW=OFF;
Rounding=FE_TOWARDZERO

3. update env (rounding only!) ; change rounding:
340282346638528859811704183484516925440.000000
FE_DIVBYZERO=ON;FE_INEXACT=ON;FE_INVALID=ON;FE_OVERFLOW=ON;FE_UNDERFLOW=OFF;
Rounding=FE_TOWARDZERO

4. restore env (initialises both: rounding and exceptions):
FE_DIVBYZERO=ON;FE_INEXACT=OFF;FE_INVALID=ON;FE_OVERFLOW=OFF;FE_UNDERFLOW=OFF;
Rounding=FE_TOWARDZERO
*/
