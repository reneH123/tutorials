#include <stdio.h>
    
int main(void) {
  float x=1.f;
  while(x!=x+1.f)  // runs forever as with ints, right? Wrong!
    x=x*2.f;
  printf("%f\n",x); // e.g. on x86_64 will always out: 16777216.0
  return 0;
}
