#include <stdio.h>
    
int main(void) {
  double x=1.;
  while(x!=x+1.)  // still does terminate (even with doubles)
    x=x*2.;
  printf("%f\n",x); // e.g. on x86_64: just >>16777216.0 (9007199254740992.0)
  return 0;
}
