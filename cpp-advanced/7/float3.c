#include <stdio.h>
#include <math.h>

// gcc -lm
    
int main(void) {
  {
    if (16777216.f+1.f==16777216.f)
      printf("YES\n");  // surprise, surprise!
    else
      printf("NO\n");
  }
  {
    float x=16777217.f;  // ULP==1: so internally 16777216.000000 !
    printf("x=%f\n",x); // outputs: ... , so no such float actually representable! Rounding IEEE-754 internally implicitly(!) wrt 1/2 ULP
    printf("nextafter towards 0: %f\n", nextafterf(x,0.f));
    printf("nextafter towards bigger: %f\n", nextafterf(x,100000000.f));
  }
  return 0;
}

/*
x=16777216.000000
nextafter towards 0: 16777215.000000
nextafter towards bigger: 16777218.000000

So, simplified this is the distribution here:
  
  ...15, 16, 18    --> +INF
            ^-- (17) not representable internally, so plunge down to 16 (by ULP==1)! So default adjustment is always towards 0 (works in analogy for negative floats)!

Remark:
 The smaller the values of floats get, the smaller the ULP gets. IEEE-754 implies the distribution is much higher on lower values, hence ULP may get significantly smaller. As seen in this example the official IEEE recommendation of ULP at most 1/2 cannot be guaranteed, but 1 is a good barrier in practice even for big float values!
*/
