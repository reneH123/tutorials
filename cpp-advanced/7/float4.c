#include <stdio.h>
#include <math.h>
#include <fenv.h>

// gcc -lm

#pragma STDC FENV_ACCESS ON
    
int main(void) {
  float x=12.f;//2.f;
  float y=11.f; //3.f;
  fesetround(FE_TONEAREST); printf("%.7f ",x/y); printf("%.7f\n", (-x)/y);
  fesetround(FE_UPWARD); printf("%.7f ",x/y); printf("%.7f\n", (-x)/y);
  fesetround(FE_DOWNWARD); printf("%.7f ",x/y); printf("%.7f\n", (-x)/y);
  fesetround(FE_TOWARDZERO); printf("%.7f ",x/y); printf("%.7f\n", (-x)/y);
  return 0;
}

/*

2/3:

0.6666667 -0.6666667
0.6666667 -0.6666666
0.6666666 -0.6666667
0.6666666 -0.6666666

12/11:

1.0909091 -1.0909091
1.0909092 -1.0909090  ! odd
1.0909090 -1.0909092    !odd
1.0909090 -1.0909090

*/
