#if 0
// gcc -lm

#include <stdio.h>
#include <math.h>

#define N 10

int main(void){
  float val=2.f;
  for(int i=0;i<N;i++){
    val=sqrtf(val);
  }
  for(int i=0;i<N;i++){
    val*=val;
  }
  printf("%e\n", fabs(val-2.));  // 4.184246e-05
  return 0;
}
#endif


// gcc gmp2.c -lgmp -lm
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

#define N       10
#define PREC  1024

int main(void){
  mpf_t val, two, dif;

  mpf_init2(val, PREC);
  mpf_init2(two, PREC);  // initialises PREC digits
  mpf_set_d(val,2.);     // 2. (implicit) cast to double
  mpf_set(two, val);     // ordinary assignment (w/ implicit cast)
  for(int i=0;i<=N;i++){
    mpf_sqrt(val,val);   // val<-sqrt(val)
  }
  for(int i=0;i<N;i++){
    mpf_mul(val,val,val);    //  1st <- 2nd * 3rd
  }
  mpf_init2(dif, PREC);
  mpf_sub(dif, val, two);
  mpf_abs(dif, dif);
  double difd = mpf_get_d(dif);
  printf("%e\n", difd);  // 5.857864e-01

  return 0;
}

