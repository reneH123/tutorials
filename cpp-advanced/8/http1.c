#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define CALL(x)  if(!(x) && fprintf(stderr,"'%s' failed (errno=%d)\n",#x,errno))exit(1)

// demonstrate very basic http request and response via sockets

int main(int argc, char *argv[]){
  if (argc!=2){
    fprintf(stderr,"usage: %s site_addr\n",argv[0]);
    return 1;
  }
  
  //  Windows woudl require additionally here:
  //     WSADATA wsa;                                   // init Winsock library
  //     CALL ( WSAStartup(MAKEWORD(2,2), &wsa) ==0 );

  struct servent *sent;
  CALL(  (sent=getservbyname("http","tcp")) !=NULL  );   // look up port number
  int port=sent->s_port;

  struct protoent *pent;
  CALL( (pent=getprotobyname("tcp")) !=NULL );           // look up for the protocol to be used
  
  struct hostent *hent;
  CALL( (hent=gethostbyname(argv[1])) !=NULL );          // collect host information (host address will be needed)
  printf("%s -> %s\n", hent->h_name, inet_ntoa(*((struct in_addr*)hent->h_addr)));
  
  struct sockaddr_in addr;                               // assemble socket connection meta data
  addr.sin_family=AF_INET;
  addr.sin_port=port;
  addr.sin_addr=*((struct in_addr*)hent->h_addr);
  memset(addr.sin_zero,0,8);
 
  int sock;
  CALL( (sock=socket(AF_INET, SOCK_STREAM, pent->p_proto)) > 0 );                  // establish a socket

  CALL( (connect(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr))) == 0 );  // esablish a socket connection
  // cast, because we would like to have a full-detailed sockaddr_in instead!


  char buff[1024+1];
  sprintf(buff,"GET / HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n", argv[1]); // prepare HTTP-request

  CALL( (write(sock,buff,strlen(buff)) == strlen(buff) ) );  // (Windows: send instead of write)

  int readin = read(sock,buff,sizeof(buff)-1);   // catch HTTP response  (Windows: recv instead of read)
  while(readin>0){
    buff[readin]='\0';
    printf("%s",buff);                           // .. and dump it to the console
    readin=read(sock,buff,sizeof(buff)-1);
  }
  close(sock);  // (Windows: closesocket instead of close)

  return 0;
}

/*

 && ./a.out google.com
google.com -> 172.217.16.206
HTTP/1.1 301 Moved Permanently
Location: http://www.google.com/
Content-Type: text/html; charset=UTF-8
Date: Fri, 13 Nov 2020 19:56:31 GMT
Expires: Sun, 13 Dec 2020 19:56:31 GMT
Cache-Control: public, max-age=2592000
Server: gws
Content-Length: 219
X-XSS-Protection: 0
X-Frame-Options: SAMEORIGIN
Connection: close

<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>301 Moved</TITLE></HEAD><BODY>
<H1>301 Moved</H1>
The document has moved
<A HREF="http://www.google.com/">here</A>.
</BODY></HTML>

*/
