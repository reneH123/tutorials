#include <iostream>

using namespace std;

class AAA{
public:
	AAA(void){
		cout << "blub";
	}
	AAA(int n){}
	AAA a11(){
		return *this;
	}

	// unary operators:
	void operator+(void){}
	void operator-(void){}
	void operator++(void){}
	void operator--(void){}
	void operator*(void){}
	void operator ()(){}
	void operator !(){}
	void operator ~(){}

	// binary operators:
	void operator+(int a){}
	void operator-(int a){}
	void operator*(int a){}
	void operator/(int a){}

	void operator+=(int a){}
	void operator-=(int a){}
	void operator*=(int a){}
	void operator/=(int a){}

	void operator<<(int a){}
	void operator>>(int a){}

	void operator ,(int a){}
	void operator &(int a){}
	void operator ^(int a){}
	void operator &&(int a){}
	void operator ()(int a){}
	void operator [](int a){}

	void operator =(int a){}
	void operator ==(int a){}
	void operator !=(int a){}
	void operator <=(int a){}
	void operator >=(int a){}
	void operator >(int a){}
	void operator <(int a){}

	// forbidden:
	//void operator sizeof(int ab){}
	//void operator ;(int ab){}
	//void operator .(int ab){}
	//void operator ::(int ab){}
};


int main() {
	cout << "Bye" << endl;
	return 0;
}

