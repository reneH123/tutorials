#include <stdio.h>
#include <arpa/inet.h>
#include <netdb.h>

int main(){
#if 0
  printf("%s\n", inet_ntoa(  *((struct in_addr*)gethostbyname("www.google.com")->h_addr )));
#endif

  printf("%d (as it comes in on little-endian arch)\n", getservbyname("http","tcp")->s_port);
  // due to Big-endian is the default network order:
  // returns 20480 (on little-endian machines, since networking is always in Big-Endian!); 20480 >> 8 == 80 should return the right byte 1st

  printf("%d (corrected for x86_64)\n", getservbyname("http","tcp")->s_port >> 8);

  // or more elegantly: use standard network-to-host converter
  printf("%d (corrected for x86_64)\n", ntohs(getservbyname("http","tcp")->s_port));

  return 0;
}

/*
Protocol no. 6!
Protocol name='tcp'
 Alias: 'TCP'
*/
