#include <stdio.h>
#include <netdb.h>

int main(){
  struct protoent *proto = getprotobynumber(6); //getprotoent();
  printf("Protocol no. %d!\n", proto->p_proto);
  printf("Protocol name='%s'\n", proto->p_name);
  char **aliases = proto->p_aliases;
  while (*aliases){
    printf(" Alias: '%s'\n", *aliases++);
  }
  return 0;
}

/*
Protocol no. 6!
Protocol name='tcp'
 Alias: 'TCP'
*/
