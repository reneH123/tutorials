// Rene Haberland,  September 2000, this is a simulation of the mysterious and virtual Wusel

#include "gl.hpp"
//#include "gl
 //opengl32.lib glu32.lib in Linker
#include <conio.h>
#include <memory.h>

#include <iostream>
#include <vector>
#include <stdio.h>

#define xscr 1024
#define yscr 768
#define bpp 32


using namespace std;

struct plot
{
	int x,y,z;
	bool operator<( const plot &p ) const { return true; }
	bool operator==( const plot &p ) const { return true; }
};

typedef vector<plot> plotlist;

plotlist mylist;		// global list
plotlist::iterator theIterator;	// iterate vector

void filetest(char *pszFileName)
{
	FILE *pfile;
	float x,y,z;
	// open and read the file   
	pfile = fopen(pszFileName, "r"); 
	if (!pfile)     
	{     
		cout << "Bad file name";
		exit(0);
	}
	else cout << "Successfully opened file" << endl;

	// read coordinates from file
	plot t;
	char s[256];
    while (!feof(pfile))
	{
		fscanf( pfile, "%s\n", s );
		sscanf( s, "{%d,%d,%d}", &t.x, &t.y, &t.z );
		mylist.push_back(t);
	}
	fclose(pfile); 
}

// seek for 1 offset point (1st point is where body starts); returns index of dynamic array
int GiveSettingPosition(int x, plot *p)
{
	theIterator = mylist.begin();
	int cnt = 0;
	
	while (!((theIterator->x == 0) || (theIterator == mylist.end())))
	{
		theIterator++;
		cnt++;
	}
	*p = mylist[cnt];
	return cnt;
}

int MaximumX( void ) // max x in mylist
{
	int t = mylist.begin()->x;

	for (theIterator = mylist.begin()+1; theIterator != mylist.end(); theIterator++)    
	{        
		if (t < theIterator->x) t = theIterator->x;
	}

	return t;
}

// return value !=0 indicates falling
int falling( void )
{
	typedef vector<int> intlist;
	intlist alist, blist;		        // list for front/rear and left/right
	intlist::iterator theintIterator;

	int a=0, b=0;
	
	// trace a branch
	//	recurse in an arbitrary direction

	// find 1st column and add all linked cubes to list
	const int maxX = MaximumX();           // nothing changes in comparison to highest
	int high=0, highest=MaximumX();
	
	plot oldplot , newplot;
	for (int x=0; ((x <= maxX) || (high == highest)); x++)	// max w.r.t. the whole wusel
	{
		high = GiveSettingPosition(x, &oldplot);
		/* for each wusel-cube
			{*/
			highest = GiveSettingPosition(x+1,&newplot);
		/*}*/
	}

	// trace direction (recurse in width)

	return (a || b);
}

void main() 
{
  filetest("wusel.txt");     // collect data

  plot *first = mylist.begin();
  plot *last = mylist.end(); // 1 behind the last

	cout << "List Size: " << mylist.size() << endl;

	plotlist mylist2;

	mylist2 = mylist;

	cout << "1st element:  " << "[" << first->x << ";"<< first->y << ";" << first->z << "]" << endl;
	cout << "2nd element: " << "[" << mylist.back().x << ";" << mylist.back().y << ";" << mylist.back().z << "]" << endl;
	
	cout << "[";
	for (theIterator = mylist.begin(); theIterator != mylist.end(); theIterator++)    
	{        
		cout << "[" << theIterator->x << ";" << theIterator->y << ";" << theIterator->z << "]";
		if (theIterator != mylist.end()-1) 
			cout << ", ";
	} 
	cout << "]" << endl;

	cout << "Press Key.";

  gl::init( xscr, yscr );

  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  //build a texture
  int s = xscr*yscr*(bpp>>3);
  char *bmp = new char[s+4];

  char *p = bmp;
  for( int j = 0; j<xscr; j++ ) 
  for( int i = 0; i<yscr; i++ ) 
  {
     int col = xscr-(j>>1) + ((i>>1)<<8);
     *(int*)p = col;
     p+= (bpp>>3);
  }

  //note: glu will scale your textures automatically
  gluBuild2DMipmaps( GL_TEXTURE_2D, 4, xscr, yscr, GL_RGBA, GL_UNSIGNED_BYTE, bmp );

  //init perspective projection
  float d = 1;
  float project[16] = {
    (float)xscr, 0, 0, 0  // one column
      ,0, (float)xscr, 0, 0
      ,0, 0, 1, d*(float)xscr
      ,0, 0, 0, 0
  };
  glMatrixMode( GL_PROJECTION );
  glLoadMatrixf( project );
  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity( );
  //enable some features  
  glEnable( GL_TEXTURE_2D );
  //deactivate the mipmaps - we dont have 
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR );
  glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

  glEnable( GL_TEXTURE_GEN_S );
  glTexGeni( GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR );
  float sg[] = { 0.5f, 0, 0, -0.5f };
  glTexGenfv( GL_S, GL_OBJECT_PLANE, sg );
  
  glEnable( GL_TEXTURE_GEN_T );
  glTexGeni( GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR );
  float tg[] = { 0, -0.5f, 0, 0.5f };
  glTexGenfv( GL_T, GL_OBJECT_PLANE, tg );

  gl::flush();

  while( kbhit() ) getch();

  glMatrixMode( GL_MODELVIEW );
  glTranslatef( 0,0,200.2 );

  glEnable( GL_BLEND );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glDisable( GL_TEXTURE_2D ); 
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

static GLfloat  wAngleY = 0.01f;
static GLfloat  wAngleX = 0.02f;
static GLfloat  wAngleZ = 0.01f;

  while( !kbhit() && !gl::keypressed ) 
  {
    gl::messages();
	glRotatef( 0, 0,0,0 );  // parameters: (angle, distance to axis: X,Y,Z)

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glPushMatrix();

		glTranslatef(0.0f, 0.0f, -20);
		glRotatef(wAngleX, 1.0f, 0.0f, 0.0f);
		glRotatef(wAngleY, 0.0f, 1.0f, 0.0f);
		glRotatef(wAngleZ, 0.0f, 0.0f, 1.0f);

		wAngleX += 0.3f;
		wAngleY += 2.0f;
		wAngleZ += 0.9f;

		
/*		glEnable(GL_CULL_FACE);		// will not have an effect due to clock-wise direction
		glCullFace(GL_FRONT);*/
		
		plot t;
		for (theIterator = mylist.begin(); theIterator != mylist.end(); theIterator++)    
		{        
			t.x = theIterator->x;
			t.y = theIterator->y;
			t.z = theIterator->z;


		glBegin(GL_QUAD_STRIP);
			glColor3f(1.0f, 0.0f, 1.0f);
			glVertex3f(-5.0f+t.x, 5.0f+t.y, 5.0f+t.z);

			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-5.0f+t.x, -5.0f+t.y, 5.0f+t.z);

			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(5.0f+t.x, 5.0f+t.y, 5.0f+t.z);

			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(5.0f+t.x, -5.0f+t.y, 5.0f+t.z);


			glColor3f(0.0f, 1.0f, 1.0f);
			glVertex3f(5.0f+t.x, 5.0f+t.y, -5.0f+t.z);

			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(5.0f+t.x, -5.0f+t.y, -5.0f+t.z);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-5.0f+t.x, 5.0f+t.y, -5.0f+t.z);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(-5.0f+t.x, -5.0f+t.y,  -5.0f+t.z);

			glColor3f(1.0f, 0.0f, 1.0f);
			glVertex3f(-5.0f+t.x, 5.0f+t.y, 5.0f+t.z);

			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-5.0f+t.x, -5.0f+t.y, 5.0f+t.z);

		glEnd();

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.0f, 1.0f);
			glVertex3f(-5.0f, 5.0f, 5.0f);

			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(5.0f, 5.0f, 5.0f);

			glColor3f(0.0f, 1.0f, 1.0f);
			glVertex3f(5.0f, 5.0f, -5.0f);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-5.0f, 5.0f, -5.0f);
		glEnd();

		glBegin(GL_QUADS);
			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-5.0f, -5.0f, 5.0f);

			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(5.0f, -5.0f, 5.0f);

			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(5.0f, -5.0f, -5.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(-5.0f, -5.0f,  -5.0f);
		glEnd();

		} // end of loop

	glPopMatrix();

	glFinish();

    gl::flush();
  }

  gl::done();
  while( kbhit() ) getch();
  delete bmp;
}

/*
Explanation on chosen data structure for the 'Wusel':

	1. find all columns directed vertically down
	2. count branching cubes:
	(sketch:)
			|=====|
			|	  |
			|  +a |
			|     | 
	   |===============|
	   |	|	  |    |
	   | +b |  X  | -b |
	   |	|	  |	   |
	   |===============|
			|	  |
			|  -a |
			|	  |
			|=====|
	3. add all a[i] and b[i]
	4. if ((norm(a) > 0)  or (norm(b) > 0)) then make the Wusel fall
*/
