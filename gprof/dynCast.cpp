#include <iostream>
 
class abc   // base class
{
public:
  virtual ~abc() { } 
  virtual void hello() 
  {
    std::cout << "in abc";
  }
};
 
class xyz : public abc
{
  public:
  void hello() 
  {
    std::cout << "in xyz";
  }
};
 
int main()
{
  abc *abc_pointer = new xyz();
  xyz *xyz_pointer;
 
  // to find whether abc is pointing to xyz type of object
  xyz_pointer = dynamic_cast<xyz*>(abc_pointer);
 
  if (xyz_pointer != NULL)
    std::cout << "abc pointer is pointing to a xyz class object";   // identified
  else
    std::cout << "abc pointer is NOT pointing to a xyz class object";
 
  // needs virtual destructor 
  delete abc_pointer;
 
  return 0;
}
