#include <iostream>
#include <typeinfo>  //for 'typeid' to work
 
class Class{
};

class NonVirtClass {
 		public:
 		int abc() {
 			 return 0; 
 		}
};

class NonVirtClass_Sub {
 		public:
 		int cde() {
 			 return 0; 
 		}
};
 
class Person {
public:
   // ... Person members ...
   virtual ~Person() {}
};
 
class Employee : public Person {
   // ... Employee members ...
};
 
int main () {
   Person person;
   Employee employee;
   Person *ptr = &employee;
   NonVirtClass nvc_obj;
   NonVirtClass_Sub nvcs_obj;
   Class class_obj;
 
   std::cout << typeid(person).name() << std::endl;   // Person (statically known at compile-time)
   std::cout << typeid(employee).name() << std::endl; // Employee (statically known at compile-time)
   std::cout << typeid(ptr).name() << std::endl;      // Person * (statically known at compile-time)
   std::cout << typeid(*ptr).name() << std::endl;     // Employee (looked up dynamically at run-time
                                            // because it is the dereference of a pointer to a polymorphic class)
	// typeid() funktioniert mit verschiedenen Klassen (nicht nur virtuellen)
	std::cout << typeid(nvc_obj).name() << std::endl;   
	std::cout << typeid(nvcs_obj).name() << std::endl;
	std::cout << typeid(class_obj).name() << std::endl;
}
