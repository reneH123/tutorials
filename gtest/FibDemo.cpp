/*
 * FibDemo.cpp
 *
 *  Created on: Apr 6, 2009
 *      Author: rhaberla
 */
#include <iostream>
#include <string>
using namespace std;

int fib(int n){
	if (n==0) return 1;
	else{
		if (n==1) return 1;
		else return fib(n-1)+fib(n-2);
	}
}

int main(){
	cout << "Hallo world!" << endl;
	cout << "Fib(5)=" << fib(5) << endl;
	return 0;
}
