/*
 * FibGTest.cpp
 *
 *  Created on: Apr 6, 2009
 *      Author: Rene Haberland (rhaberla)
 */
#include <iostream>
#include <string>
#include <limits.h>
#include "fibs.hpp"
#include <gtest/gtest.h>


/*
#include "gtest.cpp"
#include "gtest-death-test.cpp"
#include "gtest-filepath.cpp"
#include "gtest-internal-inl.h"
#include "gtest-port.cpp"
#include "gtest-test-part.cpp"
#include "gtest-typed-test.cpp"*/

using namespace std;

TEST(FacTest,Zero){
	EXPECT_EQ(1,fac(0));
	EXPECT_EQ(1,fac(1));
}

int main(int argc, char** argv){
	testing::InitGoogleTest(&argc,argv);
	return RUN_ALL_TESTS();
}
