/*
 * fibs.cpp
 *
 *  Created on: Apr 7, 2009
 *      Author: rhaberla
 */

#include "fibs.hpp"

int fac(int n){
	if (n==0) return 1;
	return n*fac(n-1);
}

int fib(int n){
	if (n==0) return 1;
	if (n==1) return 1;
	return fib(n-1)+fib(n-2);
}
