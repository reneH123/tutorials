/*
 * fibs.hpp
 *
 *  Created on: Apr 7, 2009
 *      Author: rhaberla
 */

#ifndef FIBS_HPP_
#define FIBS_HPP_

int fib(int n);
int fac(int n);

#endif /* FIBS_HPP_ */
