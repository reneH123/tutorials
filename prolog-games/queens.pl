% dump chess field on screen
board([]).
board([Row|T]):-print(Row), print('\n'), board(T).

% 'Col' is 'X'-th column number in 'Board'
col(_,[],[]):-!.
col(X,Col,[Row|Rows]):-nth(X,Row,C),
		       col(X,Col2,Rows),
		       append([C],Col2,Col).

row(Y,Row,Board):-nth(Y,Board,Row).

% check if position('X','Y') in 'Board' is occupied
occupied(X,Y,Board):-nth(Y,Board,RowY), nth(X,RowY,0),!,fail.
occupied(_,_,_).

free(X,Y,Board):-occupied(X,Y,Board),!,fail.
free(_,_,_).

% check if for position (X,Y) one of its diagonals threats
diagonal(X,Y,_,Board):-occupied(X,Y,Board),!.

diagonal(1,_,nw,_):-!,fail.
diagonal(_,1,nw,_):-!,fail.
diagonal(X,Y,nw,Board):-XM is X-1,YM is Y-1,diagonal(XM,YM,nw,Board),!.

diagonal(X,_,no,Board):-length(Board,X),!,fail.
diagonal(_,1,no,_):-!,fail.
diagonal(X,Y,no,Board):-XP is X+1,YM is Y-1,diagonal(XP,YM,no,Board),!.

diagonal(1,_,sw,_):-!,fail.
diagonal(_,Y,sw,Board):-length(Board,Y),!,fail.
diagonal(X,Y,sw,Board):-XM is X-1,YP is Y+1,diagonal(XM,YP,sw,Board),!.

diagonal(X,Y,so,Board):-length(Board,X),!,fail;length(Board,Y),!,fail.
diagonal(X,Y,so,Board):-XP is X+1,YP is Y+1,diagonal(XP,YP,so,Board),!.

diag(X,Y,Board):-diagonal(X,Y,nw,Board),!.
diag(X,Y,Board):-diagonal(X,Y,no,Board),!.
diag(X,Y,Board):-diagonal(X,Y,sw,Board),!.
diag(X,Y,Board):-diagonal(X,Y,so,Board),!.

% take the first 'N' elements of a list
take(0,_,[]):-!.
take(N,[H|T],[H|T2]):-N1 is N-1, take(N1,T,T2).

drop(0,L,L):-!.
drop(N,[_|T],T2):-N1 is N-1, drop(N1,T,T2).

% occupy(X,Y,Board,BoardNew)
occupy(X,Y,Board,_):-occupied(X,Y,Board),!,fail.
occupy(X,Y,Board,BoardNew):-Y1 is Y-1, X1 is X-1,
			    take(Y1,Board,PreY),
			    drop(Y1,Board,[RowY|PostY]),
			    take(X1,RowY,PreX),
			    drop(X1,RowY,[_|PostX]),
			    %% consturct Result: 'BoardNew'
			    append(PreX,[1],PreXX),
			    append(PreXX,PostX,NewRowY),
			    append(PreY,[NewRowY],PreYY),
			    append(PreYY,PostY,BoardNew).

%queens(StartX,StartY,Board,NewBoard):-


%threaten(X,Y,Board):-


go1([[1,0,1,1],[0,1,2]]).
go2([[1,0,0,0],[0,0,1,0],[0,5,0,3],[7,0,3,0]]).
