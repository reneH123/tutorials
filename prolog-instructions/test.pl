% Template for an arithmetic instructions simplifier
% author: Rene Haberland
% year: 2012/2020
% license: CC BY-ND  https://creativecommons.org/licenses/by-nd/4.0/

fac(0,1).
fac(N,Res):-N>0,N1 is N-1,fac(N1,Res2),Res is Res2*N.

%% TODO: later: operator notation -> instruction definition

%:-op(5,yfx,+).  % add
%:-op(5,yfx,-).  % sub
%:-op(5,yfx,-*). % rsb
%:-op(3,fx,-).   %  'neg'
% :-op(1,xfy,^).  % lsl

%%% 

%% An instruction is defined as \Sum (-1)^{j,i}*r_{j,i}*2^(e_{j,i})  
%%  \forall i,j, where e_{j,0}=0i, Exponents e_{j,i} are symbolicly evaluated
%%  Sign={p,n}, Register={r_j | forall j \in Z,j>0}, Exponent=Z
%%
%%  Any instruction is encoded as list with elements from: Sign x Register x Exponent
%%      [(s1,r1,e1), ... , (sn,rn,en)]
% Examples:
%    r1 == ([r1],[])
%    -r1 + r2^e  == ([neg(r1)],[[r2]])
%   r1 -r2^e + r3^e^f + (r1+r2^e)^f == ([r1],[[neg(r2),]])

%% 1. Translate from term built-up from functors defined above into instruction representation. (maybe: back again)
%%   [ add(rt,r0,r1,3), ... ] => [(p,r0,0),(p,r1,3), ...]
%%% Example: parse([add(r1,r0,r0,5), sub(r2,r1,r1)],[],Env),last(Env,(_,IR)),buildIF(IR,IF),simplify(IF,IFn).

arm_ins(add(_,_,_)).
arm_ins(add(_,_,_,_)).
arm_ins(sub(_,_,_)).
arm_ins(sub(_,_,_,_)).
arm_ins(mov(_,_)).
arm_ins(mov(_,_,_)).
arm_ins(rsb(_,_,_)).
arm_ins(rsb(_,_,_,_)).

%substitute (recursively) all occurences of 'Xpr' in 'Tree' expression by term 'Term'
%  is defined over ARM-instructions only
% e.g. subst(r0,add(r0,r1),xyz,Res) => Res=add(xyz,r1)
subst(Xpr,Xpr,T,T):-!.
subst(Xpr,Term,Xpr_Subs,Res):-
  Term=..[Fn|Ops],
  substOps(Xpr,Ops,Xpr_Subs,Ops_Res),
  Res=..[Fn|Ops_Res],!.  
subst(_,N,_,N):-!.

% apply 'subst' to each operand
%  e.g.substOps(r0,[r1,r0],xyz,Res) => Res=[r1,xyz]
substOps(_,[],_,[]):-!.
substOps(Xpr,[Op|T],Subs,[Op2|T2]):-
 subst(Xpr,Op,Subs,Op2),
 substOps(Xpr,T,Subs,T2).


% substitute all expressions from 'Xprs' with corresponding 'Val' from the environment in 'Ins'
%    substAll(Xprs,[(Xpr,Val)],Ins,Res)
% e.g. substAll([r0,r1], [(r0,xyz)], add(r0,r1), Res) => Res=add(xyz,r1)
substAll([],_,Ins,Ins).
substAll([H|T],Env,Ins,InsRes):-
 member((H,Val),Env),!,
 subst(H,Ins,Val,InsRes1),
 substAll(T,Env,InsRes1,InsRes).
substAll([_|T],Env,Ins,InsRes):-
 substAll(T,Env,Ins,InsRes).

nmember(E,L):-member(E,L),!,fail.
nmember(_,_).

% check for cycle: whether T is contained in any expression of Vals
checkCycle(T,Vals):-
 member(Val,Vals),
 flatten(Val,AL,_),
 member(T,AL),!.

% a directed acyclic graph
checkDAG(T,Vals):-
 checkCycle(T,Vals),!,fail.
checkDAG(_,_).


selVals([O|T],Env,Tmp,Res):-
 member((O,Val),Env),!,
 append(Tmp,[Val],Res1),
 selVals(T,Env,Res1,Res).
selVals(_,_,Res,Res).

%% e.g. parse([add(Rt,R0,R1)],Env,Env2).
% the final tree is in 'last(Env2,(_,IR))', can then be passed to 'buildIF'
parse([],Env,Env):-!.
parse([Ins|T],Env,Env2):-
  arm_ins(Ins),
  Ins=..[Fn|[Rt|Ops]],
  nmember((Rt,_),Env), Ops\=[],
  RedIns=..[Fn|Ops],
  selVals(Ops,Env,[],Vals),
  checkDAG(Rt,Vals),
  substAll(Ops,Env,RedIns,InsRes),
  append(Env,[(Rt,InsRes)],Env1),
  parse(T,Env1,Env2), Env2\=[].

%% e.g. add(add(r0,r2),sub(r2,r1),3) => r0 +r2 -r2 lsl 3 +r1 lsl 3
%%      = [ (p,r0,0), (p,r2,0), (n,r2,3), (p,r1,3) ]
% buildIF(Term,IFTerm)


% shiftIF([(p,r0,0),(n,r1,e)],1,Res) => Res= [(p,r0,1),(n,r1,e+1)]
%  not invertible!
shiftIF([],_,[]).
shiftIF([(S,R,Exp)|T],SH,Res):-
 shiftIF(T,SH,Res1),
 append([(S,R,Exp+SH)],Res1,Res).

% negateIF([(p,r0,0)],[])
%  not invertible!
negateIF([],[]).
negateIF([(S,R,Exp)|T],Res):-
  negateIF(T,Res1),
  converse(S,CS),
  append([(CS,R,Exp)],Res1,Res).

buildIF(X,[(p,X,0)]):-atom(X).
buildIF(add(R0,R1),Res):-
  buildIF(R0,L_R0), buildIF(R1,L_R1), append(L_R0,L_R1,Res).
buildIF(add(R0,R1,SH),Res):-
  buildIF(R0,L_R0), buildIF(R1,L_R1),
  shiftIF(L_R1,SH,L_R2),
  append(L_R0,L_R2,Res).
buildIF(sub(R0,R1),Res):-
  buildIF(R0,L_R0), buildIF(R1,L_R1),
  negateIF(L_R1,L_R2),
  append(L_R0,L_R2,Res).
buildIF(sub(R0,R1,SH),Res):-
  buildIF(R0,L_R0), buildIF(R1,L_R1),
  shiftIF(L_R1,SH,L_R2),
  negateIF(L_R2,L_R3),
  append(L_R0,L_R3,Res).
buildIF(mov(R0),Res):-
  buildIF(R0,Res).
buildIF(mov(R0,SH),Res):-
  buildIF(R0,L_R0),
  shiftIF(L_R0,SH,Res).
buildIF(rsb(R1,R0),Res):-
  buildIF(R0,L_R0), buildIF(R1,L_R1),
  negateIF(L_R1,L_R2),
  append(L_R0,L_R2,Res).
buildIF(rsb(R0,R1,SH),Res):-
  buildIF(R0,L_R0), buildIF(R1,L_R1),
  shiftIF(L_R1,SH,L_R2),
  negateIF(L_R0,L_R3),
  append(L_R2,L_R3,Res).

%buildIR(IF,IR)
% it can be assumed, that IF is given in normalised (simplified) notation
%  to be applied with all subsets of a given IF!
buildIR([],_):-!,fail.

buildIR([(p,R,Exp)],mov(R,Exp)):-Exp\=0,!.
 %%CONTINUE!
buildIR([(p,R,0),(p,S,0)],add(R,S)).
buildIR([(p,R,0),(p,S,Exp)],add(R,S,Exp)):-Exp\=0.

buildIR([(p,R,0),(n,S,0)],sub(R,S)).
buildIR([(p,R,0),(n,S,Exp)],sub(R,S,Exp)).

buildIR([(n,R,0),(p,S,0)],sub(S,R)).
buildIR([(n,R,0),(p,S,Exp)],rsb(R,S,Exp)).

% M + r1*2^Exp1 + r2*2^(Exp1+F)  <=> M + (r1+r2*2^F)*2^(Exp1)
buildIR([(p,M,0),(p,R1,Exp1),(p,R2,Exp2)], add(M,add(R1,R2,F),Exp1)):- Exp1\=0, Exp2\=0, atom(M),
  eqExp(Exp1+F,Exp2).
buildIR([(p,M,0),(p,R1,Exp1),(p,R2,Exp2)], add(M,add(R1,R2,F),Exp1)):- Exp1\=0, Exp2\=0, atom(M),
  eqExp(F+Exp1,Exp2).

% M - r1*2^Exp1 - r2*2^(Exp1+F)  <=> M - (r1+r2*2^F)*2^(Exp1)
buildIR([(p,M,0),(n,R1,Exp1),(n,R2,Exp2)], add(M,sub(R1,R2,F),Exp1)):- Exp1\=0, Exp2\=0, atom(M),
  eqExp(Exp1+F,Exp2).
buildIR([(p,M,0),(n,R1,Exp1),(n,R2,Exp2)], add(M,sub(R1,R2,F),Exp1)):- Exp1\=0, Exp2\=0, atom(M),
  eqExp(F+Exp1,Exp2).
  
buildIR(_):-!,fail.

%% build:-  buildIR  buildIF

%subset([X|R],S) :- member(X,S), subset(R,S).
%subset([],_).
%%subset :: [a] -> [[a]]
%%subset ls = concat [comb ls k | k <- [0..(length l)]]


%% substitute and check there are no dependencies later on!
% guarantee all entries are unique!
% transform to normalised instruction form!

%% 2. Simplification rules over instruction representation (combine neg(x) and x, e_i = E+F, e_j = --E - -F)

converse(p,n):-!.
converse(n,p):-!.
converse(_,_):-fail.

%% flatten an expresion (with occurs-check)
%  flatten((XTree,X,Y),AtomList,IntegerList), where var(X),var(Y) may not occur free in XTree
flatten0((A,_,_),[A],[]):-atom(A),!.
flatten0((I,_,_),[],[I]):-integer(I),!.
flatten0((X,Y,_),_,_):-var(X),var(Y),X==Y,!,fail.
flatten0((X,_,Y),_,_):-var(X),var(Y),X==Y,!,fail.
flatten0((X,_,_),[X],[]):-var(X),!.
flatten0((T,AL,IL),AL2,IL2):-    % e.g. T=A1+A2 or T=add(X,y,z,2)
  compound(T),!, T=..[_|Ops],
  flatten1((Ops,AL,IL),AL2,IL2).
flatten0(_,_):-!,fail.

% flatten a list of expressions
flatten1(([],_,_),[],[]):-!.
flatten1(([Opnd|T],AL,IL),AL3,IL3):-!,
  flatten0((Opnd,AL,IL),AL1,IL1),
  flatten1((T,AL,IL),AL2,IL2),
  append(AL1,AL2,AL3),
  append(IL1,IL2,IL3).

% flatten an expression
% e.g. flatten(r1+r2-Y+r4+5**a,AL,IL) => AL=[r1,r2,Y,r4,a], IL=[5]
%  where flatten(r1+X-5,X,IL) is not valid
flatten(XTree,AL,IL):-flatten0((XTree,AL,IL),AL,IL).


buildAdd([],_):-!,fail.
buildAdd([H],H):-!.
buildAdd([H|T],H+T2):-buildAdd(T,T2),!.

sum([],S,S).
sum([H|T],S,S2):-S1 is S+H, sum(T,S1,S2).

normalise(A,A):-atom(A),!.
normalise(I,I):-integer(I),!.
normalise(I + I2, Res):-integer(I),integer(I2),!,Res is I+I2.
normalise(Xpr,Sum + Node):-!,compound(Xpr),
  flatten(Xpr,AVs,Ints),
  sum(Ints,0,Sum),
  permutation(AVs,AVs2),
  buildAdd(AVs2,Node).

normalise(Sum + Node,Xpr):-!,normalise(Xpr,Sum + Node).

eqExp(Xpr1,Xpr2):-
 normalise(Xpr1,Xpr_NF),
 normalise(Xpr2,Xpr_NF),!.

%% remove two triples +r*2^e and -r*2^e from Ins
simp(Ins,ResIns):-append(X,Y,Ins),
		  append(Xpre,[(S1,R,E1)|TX],X),
		  append(Ypre,[(S2,R,E2)|TY],Y),
		  converse(S1,S2),
		  eqExp(E1,E2),
		  append(Xpre,TX,H),
		  append(Ypre,TY,H2),
		  append(H,H2,ResIns), ResIns\=Ins,!.
simp(X,X):-!.

simplify(X,Z):-simp(X,Y),X\=Y,!,simplify(Y,Z).
simplify(X,X):-!.

%% 3. Decide whether a valid ARM-instruction, and build 1 or 2 ARM-instructions from normalised instruction form
%%   = from normalised instruction form -> ARM instruction

%% 4. Model-Checker: generate and test A-instructions (2 and 3 instructions) for opt-rules (with functor-constructors!)
