/*
 * block.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef BLOCK_HPP_
#define BLOCK_HPP_

#include "stone.hpp"
#include "playground.hpp"

/*
 * superclass of all blocks
 */

class block {
public:
	virtual int getWidth();
	virtual int getHeigth();
	virtual stone getStoneAt(int x, int y);
	int getBottom();
	int getRight();
	void turn(bool toleft, playground *field);
	// TODO: ...
private:
	int x;
	int y;
	int col;
};

#endif /* BLOCK_HPP_ */
