/*
 * blockFactory.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef BLOCKFACTORY_HPP_
#define BLOCKFACTORY_HPP_

/**
 * is a (concrete) creator (abstract is dropped due to fixed block elements being used)
 * in accordance to the FACTORY-METHOD Design Pattern, here:
 *    - blockFactory <<concrete creator, creator>>
 *    - block <<product>>
 */
#include "block.hpp"

class blockFactory {
public:
	static blockFactory getInstance();
	block createSquareBlock(int col);
	block createLZBlock(int col);
	// ..
	// TODO ...
private:
	static blockFactory bf;
	// TODO ...
};

#endif /* BLOCKFACTORY_HPP_ */
