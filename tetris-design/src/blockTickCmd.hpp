/*
 * blockTickCmd.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef BLOCKTICKCMD_HPP_
#define BLOCKTICKCMD_HPP_

#include "playGroundCmd.hpp"

/*
 * tetris also requires a time trigger in order to control block movement
 * and checking for solid line(s)
 *
 */

#include "playground.hpp"
#include "block.hpp"

class blockTickCmd: public playGroundCmd {
public:
	void execute(playground &pg, block b){ /* TODO */ }
	// TODO: ...
private:
	// TODO: ...
};
#endif /* BLOCKTICKCMD_HPP_ */
