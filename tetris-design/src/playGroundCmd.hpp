/*
 * playGroundCmd.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef PLAYGROUNDCMD_HPP_
#define PLAYGROUNDCMD_HPP_

/*
 * is the abstract superclass for all kinds of commands in the tetris game,
 * e.g. turn down, move aside, turn block around
 */
#include "playground.hpp"
#include "block.hpp"

class playGroundCmd {
public:
	virtual void execute(playground &pg, block b);
	// TODO ...
private:
	// TODO ...
};

#endif /* PLAYGROUNDCMD_HPP_ */
