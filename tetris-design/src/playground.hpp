/*
 * playground.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef PLAYGROUND_HPP_
#define PLAYGROUND_HPP_

/*
 * a playground is the central instance managing the game:
 *  - it contains a zero-or-more blocks (e.g. T or square blocks dropping down)
 *  - .. and processes commands
 *  - 'physical' fixed blocks are called stones. Stones can be cleared if in a line
 */

#include <vector>
#include "stone.hpp"
#include "block.hpp"

class playground {
public:
	playground(int width, int heigth);
	void start();
	bool canMoveDown(block b);
	void paint();
	void setStoneAt(int x, int y, stone s);
	void deleteBlock();
	void removeline();
	block createfreshblock();
	// TODO ...
private:
	//... vector<stone> field();
	int height;
	int width;
	block current_block;
	bool gameOver;
	// TODO ...
};

#endif /* PLAYGROUND_HPP_ */
