/*
 * stone.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef STONE_HPP_
#define STONE_HPP_

/*
 * A stone is a fixed physical unit on the playground (a 'cell') which
 * is either filled or not.
 */

class stone {
public:
	bool isFree(){return false;}
	void paint(int x, int y, int width, int height){}
	// TODO ...
private:
	int col;
	//TODO ...
};

#endif /* STONE_HPP_ */
