/*
 * stoneFactory.hpp
 *
 *  Created on: 26 Nov 2010
 *      Author: Rene Haberland
 */

#ifndef STONEFACTORY_HPP_
#define STONEFACTORY_HPP_

/**
 * is similar to 'blockFactory'. Difference is that a stone is a physical
 * dropped unit which is fixed. If all stones in a 'playground' build up a
 * solid line it needs to be removed from the playground. For extensibility and
 * also to abstract from a concrete stone (which might be different by colour and/or
 * textures, etc.) a factory was introduced.
 *
 * the roles are in accordance to the FACTORY-METHOD design pattern:
 *    - stoneFactory <<concrete creator, creator>>
 *    - stone <<product>>
 */

#include <map>
#include "stone.hpp"

class stoneFactory {
public:
	static stoneFactory getInstance(); // use SINGLETON pattern to restrict back ref instance !
	stone getEmptyStone();
	stone getColStone(int col);
	// TODO ...
private:
	map<stone> colStones;
	static stoneFactory theFactory;
	// TODO ...
};

#endif /* STONEFACTORY_HPP_ */
