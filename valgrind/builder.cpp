//============================================================================
// Name        : builder
// Author      : René Haberland  (haberland1@mail.com)
// Date        : 14.08.2015, 2016, 2019, 2020
// Copyright   : 2020: CC BY-NC 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0/legalcode)
//   version: ad0d68b2b4de5bb434fb53b88b4434c1300c3aeb  from 9th October 2020
// Description : direct build script
//============================================================================

#ifndef BRANCH
 #define BRANCH "unknown branch"
#endif
#ifndef COMMIT
 #define COMMIT "unknown commit"
#endif

#include <algorithm>
#include <unistd.h>
#include <string>
#include <string.h>
#include <time.h>
#include <vector>
#include <sys/stat.h>

using namespace std;

#define BUILD_DIR 	"__BUILDER"
#define BUILD_CONFIG 	"builder.conf"
#define SHORT_STRING    256
#define LONG_STRING     5120
#define LLONG_STRING    524288

#ifdef THREADING
  #include <pthread.h>
  const unsigned NUM_THREADS = 4;
  const unsigned MAX_BUILDER_CORES = 8;
  unsigned BUILDER_CORES=4;
#endif

#ifdef MYMD5
    #include <stdio.h>
    #include <stdlib.h>
    #include <openssl/md5.h>
    #define BUILD_MD5SUMS   "MD5SUMS"
    #define MD5_HASH_CODE_LENGTH 33
    char fileMD5[MD5_HASH_CODE_LENGTH];
    vector<char*> _loaded_md5sums;
    vector<char*> _calculated_md5sums;
    vector<int> _diff_list_md5sums;

    void calc_MD5sums();
    int load_MD5sums();
    void compare_MD5sums();
    char* calculate_md5(const char*);
    void update_md5sums_file();
#endif

bool BUILDER_SHOW_FILES=false;
bool BUILDER_SHOW_STATS =false;

time_t start_time, end_time;

FILE *f;
unsigned _curr_line_no=0;

char _proj[SHORT_STRING];
char _cc[SHORT_STRING], _flags[SHORT_STRING];
char _includes[LONG_STRING], _libs[LONG_STRING];
char proj_var[SHORT_STRING];
vector<char*> _filelist;
char cwd[LONG_STRING];
bool verbose=false;

void load_configuration();
void load_switches_env();
void* compile(void *);
void link();

void usage() {
	printf("Usage: builder [ --help | -h | CONFIGFILE [ -v | --verbose ]]\n\n");
	printf(" René Haberland (haberland1@mail.ru), %s (%s,%s)", __DATE__, BRANCH, COMMIT); 
	printf("\n\tBuilds up a project with a configuration file.\n\n");

	printf("\t --help,-h \t\t Displays this message.\n\n");

	printf("If no CONFIGFILE is specified 'builder.config' is searched by default.");
	printf(" Builder will attempt to create a '%s' directory where it will dump all intermediate and final files into.", BUILD_DIR);
	printf(" A configuration file has to have the following parts:\n\n");

	printf("\t CC=       \t specifies the compiler to be used\n");
	printf("\t INCLUDES= \t Include directories \n");
	printf("\t LIBS=     \t Libraries and pathes (e.g. -lopenssl or -LPATH)\n");
	printf("\t FLAGS=    \t Additional compilation flags (if non, keep empty)\n");
	printf("\t FILES=    \t Newline-separted source file list to be compiled\n\n");

	printf("Comments may be introduced to the configuration file with starting '#'.\n");
	printf("Compilation tweaks available via runtime shell variables:\n\n");

	printf("\t BUILDER_CORES      \t availabe cpu cores to be used\n");
	printf("\t BUILDER_SHOW_STATS \t displays runtime per PU (ticks) / total (s)\n");
	printf("\t BUILDER_SHOW_FILES \t display files processed during build\n");

	printf("\n");
}

int init_switches(int argc, char **argv){
	char *_p=getcwd(cwd,sizeof(cwd));
	if (_p == NULL){
		printf("ERROR: Could not refer to current working directory ! Use '-h' for help.\n\n");
		exit(12);
	}

	if (argc == 1){
		f = fopen(BUILD_CONFIG,"r");
		if (f == NULL){
			printf("ERROR: Exiting because did not find '%s' in current directory ! Use '-h' for help.\n\n", BUILD_CONFIG);
			return 2;
		}
		strcpy(_proj, cwd);
	}
	else if ((argc == 2) || (argc == 3 && (!strcmp(argv[2],"-v") || !strcmp(argv[2],"--verbose")))){
        if ((!strcmp(argv[1],"--help") || !strcmp(argv[1],"-h"))){
        	usage();
            exit(0);
        }
        else{
        	if (argc == 3){
        		verbose = true;
        		printf("INFO: verbose build from config file '%s'\n", argv[1]);
        	}
        	f = fopen(argv[1], "r");
    		if (f == NULL){
    			printf("ERROR: Could not find configuration file '%s' !\n\n", argv[1]);
    			return 3;
    		}
    		int i=0;
    		while ((_proj[i]=cwd[i])) i++;
    		int j = strlen(argv[1]);
    		while (j>0 && argv[1][j--] != '/') ;
    		int n = j;
    		if (n > 0){
    			j=0;
    			_proj[i++]='/';
    			while (j<=n && (_proj[i]=argv[1][j])){
    			  i++;
    		  	  j++;
    			}
    		}
        }
    }
    else{
    	usage();
    	return 1;
    }

    return 0;
}

int main(int argc, char **argv) {
	int exit_code = init_switches(argc, argv);
	if (exit_code){
		exit(exit_code);
	}
	load_configuration();
	load_switches_env();
#ifdef MYMD5
	calc_MD5sums();
	if (load_MD5sums()){
		_diff_list_md5sums.clear();
		for (int i=0;i<_filelist.size();i++){
			_diff_list_md5sums.push_back(i);
		}
	}
	compare_MD5sums();
#endif

	if (BUILDER_SHOW_STATS){
		start_time = time(NULL);
	}

#ifndef THREADING
	compile(NULL);
#else
	printf("In total %lu file(s) and %u core(s) available\n", _filelist.size(), BUILDER_CORES);
#ifdef MYMD5
	printf("%lu files to compile.\n", _diff_list_md5sums.size());
#endif

	pthread_t threads[NUM_THREADS];
	int tmp;
	int args[NUM_THREADS] = {0,1,2,3};
	pthread_attr_t attr;

	tmp = pthread_attr_init(&attr);
	if (tmp){
		printf("Init attributes of thread failed with error code %d!\n", tmp);
		exit(EXIT_FAILURE);		
	}
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_attr_setscope(&attr,PTHREAD_SCOPE_SYSTEM);

	for(unsigned i=0;i<NUM_THREADS;i++){
		tmp=pthread_create(&threads[i],&attr,compile,(void*)&args[i]);
		if (tmp){
			printf("Creating thread %u failed with error %d!\n", i, tmp);
			exit(EXIT_FAILURE);
		}
	}

	tmp = pthread_attr_destroy(&attr);
	if (tmp){
		printf("Destroying attributes of thread failed with error code %d!\n", tmp);
		exit(EXIT_FAILURE);
	}
	
	for(unsigned i=0;i<NUM_THREADS;i++){
		tmp=pthread_join(threads[i],NULL);
		if (tmp){
			printf("Joining thread %u failed !\n", i);
			exit(EXIT_FAILURE);
		}
	}
#endif

	link();
#ifdef MYMD5
	update_md5sums_file();
#endif

	if (BUILDER_SHOW_STATS){
		end_time = time(NULL);
		printf("Time elapsed in total %ld second(s)\n", end_time-start_time);
	}
	return 0;
}

#ifdef MYMD5
void update_md5sums_file(){
	char md5_file[LONG_STRING];
	sprintf(md5_file,"%s/%s/%s",_proj,BUILD_DIR,BUILD_MD5SUMS);

	f = fopen(md5_file,"w");
	if (f == NULL){
		printf(" ERROR: could not write to MD5sum-file %s\n", md5_file);
		return;
	}
	for (auto it=_calculated_md5sums.begin();it!=_calculated_md5sums.end();++it){
		fprintf(f,"%s\n", *it);
	}
	fclose(f);
	printf("Written %lu md5sums to %s\n", _calculated_md5sums.size(), BUILD_MD5SUMS);
}
#endif

void read_variable(const char *var_name, char *var_value){
	char *_line = NULL;
	size_t _len = 0;
	ssize_t nread;
	++_curr_line_no;
	do{
		nread = getline(&_line, &_len, f);
		if (_line == NULL){
			printf("ERROR: Could not find variable '%s' down to line %d\n\n", var_name, _curr_line_no);
			exit(77);
		}
	}while (_line[0] == '#');
	if (nread <= 0 || var_name == NULL || _line == NULL){
		printf("ERROR: Corrupt or invalid configuration file at line %d\n\n", _curr_line_no);
		exit(7);
	}
	int j=0;
	while (var_name[j] == _line[j]){
		j++;
	}
	if (_line[j++] != '=' || j == 0){
		printf("ERROR: EQUAL expected at line %d\n\n", _curr_line_no);
		exit(8);
	}
	int size = nread-j-1;
	for (int dj=0;dj<size;dj++){
		var_value[dj] = _line[j++];
	}
}

void read_filelist(){
	char *_line = NULL;
	size_t _len = 0;
	ssize_t nread;

	do{
		nread = getline(&_line, &_len,f);
	}while(nread>0 && _line[0] == '#');
	if (nread <= 0 || _line == NULL || strcmp(_line, "FILES=\n")){
		printf("ERROR: Missing 'FILES=' at line %d\n\n", _curr_line_no);
		exit(7);
	}
	long l_pos = ftell(f);
	unsigned int files_count=0;
	while ((nread=getline(&_line,&_len,f))!=-1){
		if (nread>0 && _line[0] != '#'){
			++files_count;
		}
	}
	fseek(f,l_pos,SEEK_SET);
	_filelist.resize(files_count);
	for (unsigned int j=0, jj=0;jj<files_count;j++){
		do{
			nread=getline(&_line,&_len,f);
		}while(nread>0 && _line[0] == '#');
		char *_line2=(char*)malloc(nread+1);
		strncpy(_line2,_line,nread+1);
		_line2[nread-1]='\0';
		_filelist[jj++] = _line2;
	}
	fclose(f);
}

void load_configuration(){
	read_variable("CC", _cc);
	read_variable("INCLUDES", _includes);
	read_variable("LIBS", _libs);
	read_variable("FLAGS", _flags);

	read_filelist();
}

void load_switches_env(){
#ifdef THREADING
	const char* s;
	s = getenv("BUILDER_CORES");
	if (s!=NULL){
		int i=atoi(s);
		if (i==0 || i>MAX_BUILDER_CORES){
			printf(" Warning: BUILDER_CORES=%d ignored, resetting to default: %d\n",i,BUILDER_CORES);
		}
		else{
			BUILDER_CORES=i;
			printf(" BUILDER_CORES set to %d\n", BUILDER_CORES);
		}
	}
	s = getenv("BUILDER_SHOW_FILES");
	if (s!=NULL){
		printf(" BUILDER_SHOW_FILES set\n");
		BUILDER_SHOW_FILES=true;
	}
	s = getenv("BUILDER_SHOW_STATS");
	if (s!=NULL){
		printf(" BUILDER_SHOW_STATS set\n");
		BUILDER_SHOW_STATS=true;
	}
#endif
}

#ifdef MYMD5
void calc_MD5sums(){
	char full_src_file[LONG_STRING];

	_calculated_md5sums.clear();
	for (auto it=_filelist.begin(); it!=_filelist.end();++it){
		sprintf(full_src_file, "%s/%s", _proj, *it);
		char* md5_hash = calculate_md5(full_src_file);
		if (md5_hash == NULL || md5_hash[0]=='\0'){
			printf(" ERROR: in calc_MD5sums an invalid MD5hash was found for source file: %s\n. Check this suspicious file !", full_src_file);
			exit(4);
		}

		char *md5_hash_cpy = (char*)malloc(MD5_HASH_CODE_LENGTH-1);
		strncpy(md5_hash_cpy,md5_hash,MD5_HASH_CODE_LENGTH-1);
		md5_hash_cpy[MD5_HASH_CODE_LENGTH-1]='\0';

		_calculated_md5sums.push_back(md5_hash_cpy);
	}
}

int load_MD5sums(){
	_loaded_md5sums.clear();
	char *_p=getcwd(cwd,sizeof(cwd));
	if (_p == NULL){
		printf("ERROR: Could not cwd in load_MD5sums !\n\n");
		exit(12);
	}
	char md5_file[LONG_STRING];
	sprintf(md5_file, "%s/%s/%s", _proj, BUILD_DIR, BUILD_MD5SUMS);

	f = fopen(md5_file,"r");
	if (f == NULL){
		printf(" DEBUG: full rebuild required (e.g. no MD5sums)\n");
		return 1;
	}

	char *_line = NULL;
	size_t _len = 0;
	ssize_t _nread;
	while ((_nread = getline(&_line,&_len,f)) != -1){
		char *_line_cpy = (char*)malloc(MD5_HASH_CODE_LENGTH-1);
		strncpy(_line_cpy,_line,MD5_HASH_CODE_LENGTH-1);
		_line_cpy[MD5_HASH_CODE_LENGTH-1]='\0';
		_loaded_md5sums.push_back(_line_cpy);
	}

	if (_loaded_md5sums.size() != _filelist.size()){
		printf(" DEBUG: md5sum(s) indicate(s) source repository has changed since last build\n");
		return 1;
	}
	fclose(f);

	return 0;
}

char* calculate_md5(const char* fname)
{
	const unsigned BUFLEN = 1024;
	unsigned char md[MD5_DIGEST_LENGTH];
	MD5_CTX md5Ctx;
	unsigned char buf[BUFLEN];
	int bytes_read;
	
	memset(fileMD5, '\0', MD5_HASH_CODE_LENGTH);

	FILE* fin = fopen(fname, "rb");
	if (!fin) {
		perror(fname);
		return NULL;
	}

	MD5_Init(&md5Ctx);
	while (bytes_read = fread(buf, 1, BUFLEN, fin))
		MD5_Update(&md5Ctx, buf, bytes_read);
	MD5_Final(md,&md5Ctx);

	for (int i=0; i<MD5_DIGEST_LENGTH; i++)
		snprintf(&fileMD5[i*2], MD5_HASH_CODE_LENGTH, "%02x", (unsigned)md[i]);

	fclose(fin);
	return fileMD5;
}

void compare_MD5sums(){
	if (_calculated_md5sums.size() != _filelist.size()){
		return;
	}
	if (_calculated_md5sums.size() != _loaded_md5sums.size()){
		return;
	}

	int sz = _calculated_md5sums.size();
	_diff_list_md5sums.clear();
	for (int i=0;i<sz;i++){
		if (strncmp(_calculated_md5sums[i], _loaded_md5sums[i], MD5_HASH_CODE_LENGTH)){
			_diff_list_md5sums.push_back(i);
		}
		else{
			char object_file[LONG_STRING];
			sprintf(object_file, "%s/%s/%d.o", _proj, BUILD_DIR,i);
			if (access(object_file,F_OK)){
				_diff_list_md5sums.push_back(i);
			}
		}
	}
}
#endif

void* compile(void *arg){
	char source_file[LONG_STRING];
	sprintf(source_file, "%s/%s", _proj, BUILD_DIR);

	if (access(source_file,F_OK)){
		if (mkdir((const char*)source_file,0777)){
			printf("ERROR: Could not create temporary directory '%s'!\n\n", BUILD_DIR);
			exit(11);
		}
	}
	char compile_cmd[LONG_STRING];

#ifdef THREADING
	int FILES = _filelist.size();

	int thread_id;
	thread_id = *((int*)arg);
	int is_last = 0;
	int old_width=-1;

	int width=FILES/BUILDER_CORES;
	if (FILES%BUILDER_CORES){
		++width;
		if (thread_id == BUILDER_CORES-1){
			is_last = 1;
			old_width = width;
			width=FILES-(old_width*(BUILDER_CORES-1));
		}
	}
	int start_idx = (is_last)?thread_id*old_width:thread_id*width;
	int stop_idx=start_idx+width-1;

	if (start_idx>FILES-1){
		start_idx=-1;
		stop_idx=-2;
	}
#else
	int start_idx=0;
	int stop_idx=_filelist.size()-1;
#endif

	for(unsigned i=start_idx;i<=stop_idx;i++){

#ifdef MYMD5
		if (std::find(_diff_list_md5sums.begin(), _diff_list_md5sums.end(), i) == _diff_list_md5sums.end()){
			continue;
		}
#endif
		sprintf(compile_cmd,"%s -c %s %s %s/%s -o %s/%s/%d.o",_cc,_flags,_includes,_proj,_filelist[i],_proj,BUILD_DIR,i);

		clock_t finish;
		int ret = system(compile_cmd);
		if (WEXITSTATUS(ret)){
			printf("ERROR: Cannot compile '%s'!\n\n", _filelist[i]);
			exit(13);
		}
		finish = clock();

		if (verbose == true){
#ifndef THREADING
			printf("COMPILED: %s\n", compile_cmd);
#else
			printf("COMPILED(#%d): %s\n", thread_id, compile_cmd);
#endif
		}
		else{
			if (BUILDER_SHOW_FILES){
				printf("%s",_filelist[i]);
				if (!BUILDER_SHOW_STATS){
					printf("\n"); fflush(0);
				}
			}
			else{
				printf("."); fflush(0);
			}
		}

		if (BUILDER_SHOW_STATS){
			printf (" %ld ticks\n", (long)finish); fflush(0);
		}
	}
	printf("\n");
}

void link(){
	char link_cmd[LLONG_STRING];
	sprintf(link_cmd, "%s -o %s/%s/main", _cc, _proj, BUILD_DIR);

	for (unsigned i=0;i<_filelist.size();i++){
		sprintf(link_cmd, "%s %s/%s/%d.o", link_cmd, _proj, BUILD_DIR, i);
	}
	sprintf(link_cmd, "%s %s", link_cmd, _libs);

#ifdef MYMD5
	if (_diff_list_md5sums.size()>0){
#endif
		if (verbose == true){
			printf("LINK: %s\n", link_cmd);
		}
		int ret = system(link_cmd);
		if (WEXITSTATUS(ret)){
			printf("ERROR: Linking error with command:\n %s\n\n", link_cmd);
			exit(13);
		}
#ifdef MYMD5
	}
	else{
		if (verbose == true){
			printf("LINKING skipped, since no file changed! \n");
			printf(" LINK command not executed: %s\n", link_cmd);
		}
	}
#endif
}
