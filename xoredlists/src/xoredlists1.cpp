/*
 * Simple double-linked list operations: print list, front insert into list
 *  (no xor linkage)
 *
 *  18/11/2014 René Haberland
 * */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
using namespace std;

const int ages[] = {27,24};
const char* names[] = {"alice","bob"};

struct person {
	struct person *prev;
	struct person *next;
	char name[10];
	int age;
};

person first;

// print the list content as tuple colon
void print(person* l){
	cout << "List dump: ";
	while (l!=NULL){
		cout << "(" << l->name << "," << l->age << ")";
		l=l->next;
	}
	cout << endl;
}

// insert a new person at the front
void insert(person* &l, person p) {
	person* n = (person*) malloc(sizeof(person));
	n->age = p.age;
	memcpy(n->name, p.name, sizeof(p.name));

	if (l == NULL) {
		l = n;
	} else {
		l->prev = n;
		n->next = l;
		l = n;
	}
}

int main(int argc, char** args) {
	person p;
	person*l=NULL;
	print(l);
	for (int i=0;i<2;i++){
		p.age=ages[i];
		memcpy(p.name,names[i],sizeof(p.name));
		insert(l,p);
		print(l);
	}
	return 0;
}
