/*
 * Simple double-linked list operations: print list, front insert into list
 *  (with xor linkage) ...
 *
 *  18/11/2014 René Haberland
 * */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
using namespace std;

const int ages[] = {27,24};
const char* names[] = {"alice","bob"};

struct person {
	struct person *prev;
	struct person *next;
	char name[10];
	int age;
};

person first;

// perform some point manipulations test
void test(){
	void *p=0x0;
	char *p1,*p2,*p3;

	p1=(char*)0x000000ff; // 256
	p2=(char*)((int)p1>>4);
	p1=(char*)p;
	int ad_p2=(int)p2;
	p1=(char*)0x000000ff;
	p3=(char*)(p1-(char*)p);
	int ad_p3=(int)p3;
	ad_p3=(p1-(char*)p);
	ad_p2=0x000000ff;
	p1=(char*)ad_p2;

	p=(void*)malloc(10);
	p1=(char*)p-17;
	p2=p1+5;

	person* n = (person*)malloc(sizeof(person));

	p=NULL;
	p=0;
	n=(person*)p;
}


void print(person* l){
	cout << "List dump: ";
	while (l!=NULL){
		cout << "(" << l->name << "," << l->age << ")";
		l=l->next;
	}
	cout << endl;
}

void insert(person* &l, person p) {
	person* n = (person*) malloc(sizeof(person));
	n->age = p.age;
	memcpy(n->name, p.name, sizeof(p.name));

	if (l == NULL) {
		l = n;
	} else {
		l->prev = n;
		n->next = l;
		l = n;
	}
}

// equivalent (doubly linked) pointer structure as person
struct xperson {
	struct xperson *div;
	char id[4]; // e.g. al27
};

int main(int argc, char** args) {
	test();

	person p;
	person*l=NULL;
	print(l);
	for (int i=0;i<2;i++){
		p.age=ages[i];
		memcpy(p.name,names[i],sizeof(p.name));
		insert(l,p);
		print(l);
	}
	return 0;
}
