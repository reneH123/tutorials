/*
 * 18/10/2014 René Haberland
 *
 * XOR-doubly linked lists
 *
 * Parts have been taken from:
 *    http://www.linuxjournal.com/article/6828
 *
 * Code corrected (now error free) and modified
 *
 * */
#include <iostream>
#include <stdlib.h>
#include <assert.h>

using namespace std;

struct listNode{
	int key;
	struct listNode * ptrdiff;
};

listNode* next(listNode* pNode, listNode* pPrevNode) {
	return ((listNode*) ((int) pNode->ptrdiff ^ (int) pPrevNode));
}

// start node of the XOR-linked list
listNode* pStart;

// place node 'pNew' after node with entry 'key'
void insertAfter(listNode* pNew, int key) {
	listNode* pPrev;
	listNode* pCurrent;
	listNode* pNext;
	pPrev = 0;
	pCurrent = pStart;

	while (pCurrent) {
		pNext = next(pCurrent, pPrev);
		if (pCurrent->key == key) {
			// traversal is done
			if (pNext) {
				// fix the existing next node
				pNext->ptrdiff = (listNode*) (((int) pNext->ptrdiff
						^ (int) pCurrent) ^ (int) pNew);
				// fix the current node
				pCurrent->ptrdiff = (listNode*) ((int) pNew ^ ((int) pNext
						^ (int) pCurrent->ptrdiff));
				// fix the new node
				pNew->ptrdiff = (listNode*) ((int) pCurrent ^ (int) pNext);
			}
			else{
				pCurrent->ptrdiff = (listNode*) (((int)pCurrent->ptrdiff ^ (int)0) ^ (int)pNew);
				pNew->ptrdiff = (listNode*) ((int)pCurrent ^ (int)0);
			}
			break;
		}
		pPrev = pCurrent;
		pCurrent = pNext;
	}
}

int main() {
	cout << "Construct the XOR-linked list: (nil)<->17<->11<->29<->37<->(nil)" << endl;

	pStart = (listNode*) malloc(sizeof(listNode));
	pStart->key = 17;

	listNode* p11;
	p11 = (listNode*) malloc(sizeof(listNode));
	p11->key = 11;
	listNode* p29;
	p29 = (listNode*) malloc(sizeof(listNode));
	p29->key = 29;
	listNode* p37;
	p37 = (listNode*) malloc(sizeof(listNode));
	p37->key = 37;

	insertAfter(p11, 17);
	insertAfter(p29, 11);
	insertAfter(p37, 29);

	// (nil)<->17<->11<->29<->37<->(nil)

	// forward traversal
	listNode *tmp;
	tmp=next(pStart,(listNode*)0);
	assert(tmp==p11);
	tmp=next(p11,pStart);
	assert(tmp==p29);
	tmp=next(p29,p11);
	assert(tmp==p37);
	tmp=next(p37,p29);
	assert(tmp==(listNode*)0);

	// backward traversal
	tmp=next(p37,(listNode*)0);
	assert(tmp==p29);
	tmp=next(p29,p37);
	assert(tmp==p11);
	tmp=next(p11,p29);
	assert(tmp==pStart);
	tmp=next(pStart,p11);
	assert(tmp==(listNode*)0);

	return 0;
}
